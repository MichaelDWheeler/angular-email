export interface EnjoyLinks {
    show: string;
    link: string;
    title: string;
    date: string;
    addInterest: string;
}
