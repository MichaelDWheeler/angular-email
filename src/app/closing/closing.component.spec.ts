import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosingComponent } from './closing.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { FormsModule } from '@angular/forms';

describe('ClosingComponent', () => {
  let component: ClosingComponent;
  let fixture: ComponentFixture<ClosingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClosingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
