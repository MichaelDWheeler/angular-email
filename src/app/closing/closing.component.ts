import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-closing',
  templateUrl: './closing.component.html',
  styleUrls: ['./closing.component.css']
})
export class ClosingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'closing';
  closingValediction: string;
  closingName: string;
  closingTitle: string;
  closingImageSrc: string;
  closingText: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent
    this.email.closingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.closingDisplay;
    this.closingValediction = this.email.closingValediction;
    this.closingName = this.email.closingName;
    this.closingTitle = this.email.closingTitle;
    this.closingImageSrc = this.email.closingImageSrc;
    this.closingText = this.email.closingText;
  }
}
