import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from './create-template.service';
import { CheckEmailFieldsService } from './check-email-fields.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  email: any;
  hasCache = false;
  showControls: boolean;

  private _isEquivalent(a, b) {
    const aProps = Object.getOwnPropertyNames(a);
    const bProps = Object.getOwnPropertyNames(b);

    if (aProps.length !== bProps.length) {
      return false;
    }

    for (const key in a) {
      if (a.hasOwnProperty(key)) {
        if (a[key] !== b[key] && key !== 'showControls') {
          return false;
        }
      }
    }
    return true;
  }

  public toggleShow(): void {
    const defaultTemplate = this.createTemplate.templateDefault,
      variableTemplate = this.createTemplate.templateVariables;
    this.email.showControls = !this.email.showControls;

    if (!this._isEquivalent(defaultTemplate, variableTemplate) || variableTemplate.dataEnjoyLinks.length !== 0 || variableTemplate.adProviders.length !== 0) {
      this.createTemplate.setStorage();
      this.hasCache = true;
    }
  }

  public clearStorage(): void {
    this.createTemplate.clearStorage();
    this.hasCache = false;
  }

  toggleControls(): void {
    this.toggleShow();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public check: CheckEmailFieldsService
  ) { }

  ngOnInit(): void {
    this.email = this.createTemplate.getVariables();
    this.showControls = this.email.showControls;
    this.email.showControls = true;
    if (this.email.localStorage) {
      this.hasCache = true;
    }
  }
}
