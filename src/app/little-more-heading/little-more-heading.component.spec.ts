import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { LittleMoreHeadingComponent } from './little-more-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { mockLocalStorageData } from '../mock-local-storage-data';
import { TEMPLATEVARIABLES } from '../data-create-template';



describe('LittleMoreHeadingComponent', () => {
  let component: LittleMoreHeadingComponent;
  let fixture: ComponentFixture<LittleMoreHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LittleMoreHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LittleMoreHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();    
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set showComponent to false and then to true when toggleControl method is called twice', ()=>{
     component.showComponent = true;
     component.toggleControls();
     expect(component.showComponent).toBe(false);
     component.toggleControls();
     expect(component.showComponent).toBe(true);
  });
});
