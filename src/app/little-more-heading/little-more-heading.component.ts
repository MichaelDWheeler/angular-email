import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-little-more-heading',
  templateUrl: './little-more-heading.component.html',
  styleUrls: ['./little-more-heading.component.css'],
})
export class LittleMoreHeadingComponent implements OnInit {
  email: any = {};
  component = 'heading';
  showComponent: boolean;
  littleMoreHeading: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.littleMoreHeadingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.littleMoreHeadingDisplay;
    this.littleMoreHeading = this.email.littleMoreHeading;
  }

}
