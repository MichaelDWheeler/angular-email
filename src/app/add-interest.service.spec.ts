import { TestBed, inject } from '@angular/core/testing';

import { AddInterestService } from './add-interest.service';
import { CreateTemplateService } from './create-template.service';

describe('AddInterestService', () => {
  let service: AddInterestService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddInterestService]
    });
    service = new AddInterestService(new CreateTemplateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an array with 22 objects', () => {
    const array: any = [] = service.getAddInterests();
    expect(array.length).toBe(22);
  });

  it('should set component.email.comingSoonBottomRightAddInterestConverted to 0', () => { 
    service.setAddInterests(0, 'comingSoonBottomRightAddInterest');
    expect(service.email.comingSoonBottomRightAddInterestConverted).toBe('');
  });

  it('should set component.email.comingSoonBottomRightAddInterestConverted to AddInterest=1206&', () => { 
    service.setAddInterests(1206, 'comingSoonBottomRightAddInterest');
    expect(service.email.comingSoonBottomRightAddInterestConverted).toBe('AddInterest=1206&');
  });
});
