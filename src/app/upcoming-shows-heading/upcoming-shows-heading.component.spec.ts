import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingShowsHeadingComponent } from './upcoming-shows-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { FormsModule } from '@angular/forms';

describe('UpcomingShowsHeadingComponent', () => {
  let component: UpcomingShowsHeadingComponent;
  let fixture: ComponentFixture<UpcomingShowsHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        UpcomingShowsHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingShowsHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });
});
