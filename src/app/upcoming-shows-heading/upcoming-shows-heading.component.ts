import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-upcoming-shows-heading',
  templateUrl: './upcoming-shows-heading.component.html',
  styleUrls: ['./upcoming-shows-heading.component.css'],
})
export class UpcomingShowsHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'heading';
  upcomingShowsHeading: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.upcomingShowsHeadingDisplay = this.showComponent;
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.upcomingShowsHeadingDisplay;
    this.upcomingShowsHeading = this.email.upcomingShowsHeading;
  }

}
