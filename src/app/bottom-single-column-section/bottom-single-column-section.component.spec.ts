import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { BottomSingleColumnSectionComponent } from './bottom-single-column-section.component';

describe('BottomSingleColumnSectionComponent', () => {
  let component: BottomSingleColumnSectionComponent;
  let fixture: ComponentFixture<BottomSingleColumnSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomSingleColumnSectionComponent ],
      imports: [ FormsModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSingleColumnSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the properties to specific values', ()=>{
    component.email.bottomSingleColumnTitle = "Component Test";
    component.addVariables();
    expect(component.email.bottomSingleColumnAlt).toEqual("Component Test");
    expect(component.email.bottomSingleColumnUTM).toEqual("Component%20Test");
  });

});
