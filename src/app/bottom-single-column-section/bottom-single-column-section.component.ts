import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-bottom-single-column-section',
  templateUrl: './bottom-single-column-section.component.html',
  styleUrls: ['./bottom-single-column-section.component.css']
})
export class BottomSingleColumnSectionComponent implements OnInit {
  @Input() bottomSingleColumnAlt: string;
  @Input() bottomSingleColumnUTM: string;
  @Input() bottomSingleColumnButton: string;

  showComponent: boolean;
  email: any = {};
  bottomSingleColumnLink: string;
  bottomSingleColumnImage: string;
  bottomSingleColumnTitle: string;
  bottomSingleColumnSubTitle: string;
  bottomSingleColumnAdditionalInfo: string;
  bottomSingleColumnContent: string;
  bottomSingleColumnDescription: string;

  addVariables(): void {
    this.createTemplate.setVariables('bottomSingleColumnAlt', this.bottomSingleColumnTitle);
    this.createTemplate.setVariables('bottomSingleColumnUTM', this.bottomSingleColumnTitle);
    this.createTemplate.changeVariables('bottomSingleColumnTitle', this.bottomSingleColumnTitle);
    this.bottomSingleColumnAlt = this.email.bottomSingleColumnAlt;
    this.bottomSingleColumnUTM = this.email.bottomSingleColumnUTM;
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = true;
    this.bottomSingleColumnAlt = this.email.bottomSingleColumnAlt;
    this.bottomSingleColumnUTM = this.email.bottomSingleColumnUTM;
    this.bottomSingleColumnButton = this.email.bottomSingleColumnButton;
    this.bottomSingleColumnLink = this.email.bottomSingleColumnLink;
    this.bottomSingleColumnImage = this.email.bottomSingleColumnImage;
    this.bottomSingleColumnDescription = this.email.bottomSingleColumnDescription;
    this.bottomSingleColumnAdditionalInfo = this.email.bottomSingleColumnAdditionalInfo;
    this.bottomSingleColumnTitle = this.email.bottomSingleColumnTitle;
    this.bottomSingleColumnSubTitle = this.email.bottomSingleColumnSubTitle;
    this.bottomSingleColumnSubTitle = this.email.bottomSingleColumnSubTitle;
  }

}
