import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-content-image-wrap-heading',
  templateUrl: './content-image-wrap-heading.component.html',
  styleUrls: ['./content-image-wrap-heading.component.css'],
})
export class ContentImageWrapHeadingComponent implements OnInit {
  email: any;
  component = 'heading';
  showComponent: boolean;
  contentImageWrapHeading;

  toggleControls() {
    this.showComponent = !this.showComponent;
    this.email.contentImageWrapHeadingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.contentImageWrapHeadingDisplay;
    this.contentImageWrapHeading = this.email.contentImageWrapHeading;
  }

}
