import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ContentImageWrapHeadingComponent } from './content-image-wrap-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('ContentImageWrapHeadingComponent', () => {
  let component: ContentImageWrapHeadingComponent;
  let fixture: ComponentFixture<ContentImageWrapHeadingComponent>;
  const templateVariables = TEMPLATEVARIABLES;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ContentImageWrapHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [ FormsModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentImageWrapHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "showComponent" to change between true and false', () => {
    component.toggleControls();
    expect(component.showComponent).toBeFalsy;
    component.toggleControls();
    expect(component.showComponent).toBeTruthy;
  });

  it('the email object should equal the default properties and values in TEMPLATEVARIABLES', ()=>{
    component.email = component.createTemplate.getVariables();
    expect(component.email).toEqual(templateVariables);
  });

});
