import { TestBed } from '@angular/core/testing';

import { CreateTemplateService } from './create-template.service';
import { emailProps } from './email-properties';
import { mockLocalStorageData } from './mock-local-storage-data';
import { TEMPLATEVARIABLES } from './data-create-template';

describe('CreateTemplateService', () => {
  let service: CreateTemplateService;
  let templateVariables: any;
  let convertedData: any;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateTemplateService]
    });
    service = new CreateTemplateService;
    templateVariables = service.getVariables();
    convertedData = service.restoreDataType(mockLocalStorageData);
  });

  afterEach(() => {
    templateVariables = {};
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return email object containing properties', () => {
    const data = service.getVariables();
    const actual = Object.keys(data).sort();
    const expected = emailProps.sort();
    expect(actual).toEqual(expected);
  });

  it('should convert comingSoonBottomLeftAddInterest to number type', () => {
    const comingSoonBottomLeftAddInterest = typeof (convertedData.comingSoonBottomLeftAddInterest);
    expect(comingSoonBottomLeftAddInterest).toEqual('number');
  });

  it('should convert comingSoonBottomRightAddInterest to number type', () => {
    const comingSoonBottomRightAddInterest = typeof (convertedData.comingSoonBottomRightAddInterest);
    expect(comingSoonBottomRightAddInterest).toEqual('number');
  });

  it('should convert comingSoonTopLeftAddInterest to number type', () => {
    const comingSoonTopLeftAddInterest = typeof (convertedData.comingSoonTopLeftAddInterest);
    expect(comingSoonTopLeftAddInterest).toEqual('number');
  });

  it('should convert comingSoonTopRightAddInterest to number type', () => {
    const comingSoonTopRightAddInterest = typeof (convertedData.comingSoonTopRightAddInterest);
    expect(comingSoonTopRightAddInterest).toEqual('number');
  });

  it('should convert episodeFontSize to number type', () => {
    const episodeFontSize = typeof (convertedData.episodeFontSize);
    expect(episodeFontSize).toEqual('number');
  });

  it('should convert extraSingleColumnHeadingDisplay to boolean type', () => {
    const extraSingleColumnHeadingDisplay = typeof (convertedData.extraSingleColumnHeadingDisplay);
    expect(extraSingleColumnHeadingDisplay).toEqual('boolean');
  });

  it('should convert littleMoreAddInterest to number type', () => {
    const littleMoreAddInterest = typeof (convertedData.littleMoreAddInterest);
    expect(littleMoreAddInterest).toEqual('number');
  });

  it('should convert localStorage to boolean type', () => {
    const localStorage = typeof (convertedData.localStorage);
    expect(localStorage).toEqual('boolean');
  });

  it('should convert partnerSpotlightAddInterest to number type', () => {
    const partnerSpotlightAddInterest = typeof (convertedData.partnerSpotlightAddInterest);
    expect(partnerSpotlightAddInterest).toEqual('number');
  });

  it('should convert partnerSpotlightTwoAddInterest to number type', () => {
    const partnerSpotlightTwoAddInterest = typeof (convertedData.partnerSpotlightTwoAddInterest);
    expect(partnerSpotlightTwoAddInterest).toEqual('number');
  });

  it('should convert passportLearnMoreAddInterest to number type', () => {
    const passportLearnMoreAddInterest = typeof (convertedData.passportLearnMoreAddInterest);
    expect(passportLearnMoreAddInterest).toEqual('number');
  });

  it('should convert programFontSize to number type', () => {
    const programFontSize = typeof (convertedData.programFontSize);
    expect(programFontSize).toEqual('number');
  });

  it('should convert showControls to boolean type', () => {
    const showControls = typeof (convertedData.showControls);
    expect(showControls).toEqual('boolean');
  });

  it('should convert singleColumnAddInterest to number type', () => {
    const singleColumnAddInterest = typeof (convertedData.singleColumnAddInterest);
    expect(singleColumnAddInterest).toEqual('number');
  });

  it('should convert singleImageAddInterest to number type', () => {
    const singleImageAddInterest = typeof (convertedData.singleImageAddInterest);
    expect(singleImageAddInterest).toEqual('number');
  });

  it('should convert templateSelected to number type', () => {
    const templateSelected = typeof (convertedData.templateSelected);
    expect(templateSelected).toEqual('undefined');
  });

  it('should return url string', () => {
    const subDomain = service.checkSubDomain();
    expect(subDomain).toEqual('context.html');
  });

  it('should return an empty object', () => {
    const allStorageObj = typeof (service.allStorage());
    expect(allStorageObj).toEqual('object');
  });

  it('should return undefined', () => {
    const templateSelected = service.checkTemplateSelected();
    expect(templateSelected).toEqual(undefined);
  });

  it('variable should equal \'Hi%20This%20Is%20A%20Test\'', () => {
    service.checkVariableFormat('bottomSingleColumnUTM', 'Hi This Is A Test');
    expect(templateVariables['bottomSingleColumnUTM']).toEqual('Hi%20This%20Is%20A%20Test');
  });

  it('should return a date', () => {
    const date = service.getDate();
    expect(templateVariables.utmSource).toMatch(/\d\d\d\d-\d\d-\d\d/);
  });

  it('should return a future date', () => {
    const monthsObj = service.month;
    const headingWithDate = service.getFutureDate();
    const month = headingWithDate.split(' ')[0];
    const hasMonth = () => {
      for (const key in monthsObj) {
        if (monthsObj.hasOwnProperty(key) && monthsObj[key] === month) {
          return true;
        }
      }
    };
    expect(hasMonth()).toEqual(true);
  });

  it('should return obj with getLinks Interface', () => {
    const component = 'test';
    templateVariables.dataEnjoyLinks.push({ 'show': 'Show', 'link': 'https://www.example.com', 'title': 'Example', 'date': '10-10-2018', 'addInterest': 'none' });
    const links = service.getLinks(component);
    expect(links).toMatch('[{ "show": "Show", "link": "https://www.example.com", "title": "Example", "date": "10-10-2018", "addInterest": "none" }]');
  });

  it('should return the default template variables', () => {
    const storage = service.getStorage();
    expect(storage).toEqual(TEMPLATEVARIABLES);
  });

  it('should return array of ad supporters', () => {
    templateVariables.adProviders.push({ 'image': 'test', 'link': 'test', 'alt': 'test' }, { 'image': 'test', 'link': 'test', 'alt': 'test' }, { 'image': 'test', 'link': 'test', 'alt': 'test' });
    const supporters = service.getSupporters();
    expect(supporters).toMatch('[{ "image": "test", "link": "test", "alt": "test" },{ "image": "test", "link": "test", "alt": "test" },{ "image": "test", "link": "test", "alt": "test" }]');
  });

  it('should return 01', () => {
    const numString = '1';
    const rtnString = service.makeTwoDigit(numString);
    expect(rtnString).toEqual('01');
  });

  it('should return a date that is on the next Saturday', () => {
    const dateString = service.nextSaturday();
    const day = dateString.toString().split(' ')[0];
    expect(day).toEqual('Sat');
  });

  it('should set an item in localStorage', () => {
    templateVariables.bottomSingleColumnSubTitle = 'This is a test';
    service.setStorage();
    const item = localStorage.getItem('bottomSingleColumnSubTitle');
    expect(item).toEqual('This is a test');
  });

  it('should change the variables in templateVariables', () => {
    service.setVariables('bottomSingleColumnUTM', 'TESTING');
    expect(templateVariables.bottomSingleColumnUTM).toEqual('TESTING');
  });
});
