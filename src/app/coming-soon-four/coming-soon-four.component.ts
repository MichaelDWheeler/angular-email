import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { CreateTemplateService } from '../create-template.service';
import { AddInterestService } from '../add-interest.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-coming-soon-four',
  templateUrl: './coming-soon-four.component.html',
  styleUrls: ['./coming-soon-four.component.css']
})
export class ComingSoonFourComponent implements OnInit, OnDestroy {
  email: any;
  showComponent: boolean;
  component = 'two programs';
  addInterest: any = [];
  topLeft = 'comingSoonFourLeftAddInterest';
  topRight = 'comingSoonFourRightAddInterest';
  comingSoonFourLeftProgram: string;
  comingSoonFourLeftAlt: string;
  comingSoonFourLeftUTM: string;
  comingSoonFourLeftTitle: string;
  comingSoonFourLeftAirDate: string;
  comingSoonFourLeftButton: string;
  comingSoonFourLeftDescription: string;
  comingSoonFourLeftImage: string;
  comingSoonFourLeftLink: string;
  comingSoonFourRightProgram: string;
  comingSoonFourRightAlt: string;
  comingSoonFourRightUTM: string;
  comingSoonFourRightTitle: string;
  comingSoonFourRightAirDate: string;
  comingSoonFourRightButton: string;
  comingSoonFourRightDescription: string;
  comingSoonFourRightImage: string;
  comingSoonFourRightLink: string;
  subscription: Subscription;

  addVariables(): void {
    this.createTemplate.setVariables('comingSoonFourLeftAlt', this.comingSoonFourLeftProgram);
    this.createTemplate.setVariables('comingSoonFourLeftUTM', this.comingSoonFourLeftProgram);
    this.createTemplate.changeVariables('comingSoonFourLeftProgram', this.comingSoonFourLeftProgram);
    this.comingSoonFourLeftAlt = this.email.comingSoonFourLeftAlt;
    this.comingSoonFourLeftUTM = this.email.comingSoonFourLeftUTM;
    this.createTemplate.setVariables('comingSoonFourRightAlt', this.comingSoonFourRightProgram);
    this.createTemplate.setVariables('comingSoonFourRightUTM', this.comingSoonFourRightProgram);
    this.createTemplate.changeVariables('comingSoonFourRightProgram', this.comingSoonFourRightProgram);
    this.comingSoonFourRightAlt = this.email.comingSoonFourRightAlt;
    this.comingSoonFourRightUTM = this.email.comingSoonFourRightUTM;
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.comingSoonFourDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void {
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if (message) {
        this.comingSoonFourLeftUTM = this.email.comingSoonFourLeftUTM;
        this.comingSoonFourRightUTM = this.email.comingSoonFourRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public interest: AddInterestService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.comingSoonFourDisplay;
    this.comingSoonFourLeftProgram = this.email.comingSoonFourLeftProgram;
    this.comingSoonFourLeftAlt = this.email.comingSoonFourLeftAlt;
    this.comingSoonFourLeftUTM = this.email.comingSoonFourLeftUTM;
    this.comingSoonFourLeftTitle = this.email.comingSoonFourLeftTitle;
    this.comingSoonFourLeftAirDate = this.email.comingSoonFourLeftAirDate;
    this.comingSoonFourLeftButton = this.email.comingSoonFourLeftButton;
    this.comingSoonFourLeftDescription = this.email.comingSoonFourLeftDescription;
    this.comingSoonFourLeftImage = this.email.comingSoonFourLeftImage;
    this.comingSoonFourRightProgram = this.email.comingSoonFourRightProgram;
    this.comingSoonFourRightAlt = this.email.comingSoonFourRightAlt;
    this.comingSoonFourRightUTM = this.email.comingSoonFourRightUTM;
    this.comingSoonFourRightTitle = this.email.comingSoonFourRightTitle;
    this.comingSoonFourRightAirDate = this.email.comingSoonFourRightAirDate;
    this.comingSoonFourRightButton = this.email.comingSoonFourRightButton;
    this.comingSoonFourRightDescription = this.email.comingSoonFourRightDescription;
    this.comingSoonFourRightImage = this.email.comingSoonFourRightImage;
    this.comingSoonFourRightLink = this.email.comingSoonFourRightLink;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
