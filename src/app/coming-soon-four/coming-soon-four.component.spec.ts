import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComingSoonFourComponent } from './coming-soon-four.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { FormsModule } from '@angular/forms';
import { AddInterestComponent } from '../add-interest/add-interest.component';

describe('ComingSoonFourComponent', () => {
  let component: ComingSoonFourComponent;
  let fixture: ComponentFixture<ComingSoonFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ComingSoonFourComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingSoonFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
