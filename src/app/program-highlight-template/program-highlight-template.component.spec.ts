import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ProgramHighlightTemplateComponent } from './program-highlight-template.component';
import { ContentComponent } from '../content/content.component';
import { FooterComponent } from '../footer/footer.component';
import { EmailOutputComponent } from '../email-output/email-output.component';
import { WhatsOnComponent } from '../whats-on/whats-on.component';
import { TopSingleColumnComponent } from '../top-single-column/top-single-column.component';
import { UpcomingShowsColumnsTopComponent } from '../upcoming-shows-columns-top/upcoming-shows-columns-top.component';
import { UpcomingShowsColumnsBottomComponent } from '../upcoming-shows-columns-bottom/upcoming-shows-columns-bottom.component';
import { AlsoEnjoyComponent } from '../also-enjoy/also-enjoy.component';
import { AlsoEnjoyLinksComponent } from '../also-enjoy-links/also-enjoy-links.component';
import { StreamingNowHeaderComponent } from '../streaming-now-header/streaming-now-header.component';
import { StreamingNowTopComponent } from '../streaming-now-top/streaming-now-top.component';
import { StreamingNowBottomComponent } from '../streaming-now-bottom/streaming-now-bottom.component';
import { PassportLogoHeaderComponent } from '../passport-logo-header/passport-logo-header.component';
import { PassportAdComponent } from '../passport-ad/passport-ad.component';
import { BottomSingleColumnHeadingComponent } from '../bottom-single-column-heading/bottom-single-column-heading.component';
import { BottomSingleColumnSectionComponent } from '../bottom-single-column-section/bottom-single-column-section.component';
import { ExtraSingleColumnHeadingComponent } from '../extra-single-column-heading/extra-single-column-heading.component';
import { ExtraSingleColumnComponent } from '../extra-single-column/extra-single-column.component';
import { MonthlyFunHeadingComponent } from '../monthly-fun-heading/monthly-fun-heading.component';
import { MonthlyFunSectionComponent } from '../monthly-fun-section/monthly-fun-section.component';
import { ProvidingSupportHeadingComponent } from '../providing-support-heading/providing-support-heading.component';
import { ProvidingSupportAdsComponent } from '../providing-support-ads/providing-support-ads.component';
import { CodeOutComponent } from '../code-out/code-out.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { GlobalUtmComponent } from '../global-utm/global-utm.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ProgramHighlightTemplateComponent', () => {
  let component: ProgramHighlightTemplateComponent;
  let fixture: ComponentFixture<ProgramHighlightTemplateComponent>;

  beforeEach(async((done) => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        ProgramHighlightTemplateComponent,
        ContentComponent,
        FooterComponent,
        EmailOutputComponent,
        WhatsOnComponent,
        TopSingleColumnComponent,
        UpcomingShowsColumnsTopComponent,
        UpcomingShowsColumnsBottomComponent,
        AlsoEnjoyComponent,
        AlsoEnjoyLinksComponent,
        StreamingNowHeaderComponent,
        StreamingNowTopComponent,
        StreamingNowBottomComponent,
        PassportLogoHeaderComponent,
        PassportAdComponent,
        BottomSingleColumnHeadingComponent,
        BottomSingleColumnSectionComponent,
        ExtraSingleColumnHeadingComponent,
        ExtraSingleColumnComponent,
        MonthlyFunHeadingComponent,
        MonthlyFunSectionComponent,
        ProvidingSupportHeadingComponent,
        ProvidingSupportAdsComponent,
        CodeOutComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        GlobalUtmComponent
      ],
      imports: [ FormsModule ]
    })
      .compileComponents();
  }));

  beforeEach(async((done) => {
    fixture = TestBed.createComponent(ProgramHighlightTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
