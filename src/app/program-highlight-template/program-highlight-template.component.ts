import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-program-highlight-template',
  templateUrl: './program-highlight-template.component.html',
  styleUrls: ['./program-highlight-template.component.css']
})
export class ProgramHighlightTemplateComponent implements OnInit {
  email: any = {};

  constructor(
    public createTemplate: CreateTemplateService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
  }
}
