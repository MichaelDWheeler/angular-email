import { TestBed, inject } from '@angular/core/testing';

import { FontService } from './font.service';
import { CreateTemplateService } from './create-template.service';


describe('FontService', () => {
  let service: FontService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FontService]
    });
    service = new FontService(new CreateTemplateService);
    service.email.programFontSize = 24;
    service.email.episodeFontSize = 24;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should increment the email.programFontSize by 1', () => {
    service.programIncrease();
    expect(service.email.programFontSize).toBe(25);
  });

  it('should decrease the email.programFontSize by 1', () => { 
    service.programDecrease();
    expect(service.email.programFontSize).toBe(23);
  });

  it('should increment the email.episodeFontSize by 1', () => {
    service.episodeIncrease();
    expect(service.email.episodeFontSize).toBe(25);
  });

  it('should decrease the email.episodeFontSize by 1', () => { 
    service.episodeDecrease();
    expect(service.email.episodeFontSize).toBe(23);
  });
});
