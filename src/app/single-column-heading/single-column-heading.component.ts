import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-single-column-heading',
  templateUrl: './single-column-heading.component.html',
  styleUrls: ['./single-column-heading.component.css'],
})
export class SingleColumnHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'single column section';
  singleColumnHeading: string;
  singleColumnImageAlt: string;
  singleColumnContent: string;

  addVariables(): void {
    this.createTemplate.setVariables('singleColumnImageAlt',this.singleColumnHeading);
    this.createTemplate.setVariables('singleColumnContent',this.singleColumnHeading);
    this.createTemplate.changeVariables('singleColumnHeading',this.singleColumnHeading);
    this.singleColumnImageAlt = this.email.singleColumnImageAlt;
    this.singleColumnContent = this.email.singleColumnContent;
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.singleColumnDisplay = this.showComponent;
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.singleColumnDisplay;
    this.singleColumnHeading = this.email.singleColumnHeading;
  }

}
