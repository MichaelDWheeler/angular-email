import { PreHeaderText } from './preheader-text';

export const PREHEADERTEXT: PreHeaderText = { subject: 'Super Subject Line' };
