import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { LittleMoreComponent } from './little-more.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestComponent } from '../add-interest/add-interest.component';

describe('LittleMoreComponent', () => {
  let component: LittleMoreComponent;
  let fixture: ComponentFixture<LittleMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LittleMoreComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LittleMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', (() => {
    expect(component).toBeTruthy();
  }));

  it('should return an object with "MyTest" as the value for the "show" key', () => {
    component.links = [];
    component.programTitle = 'MyTest';
    component.programLink = 'TestLink';
    component.addNewLink();
    expect(component.links[0]['show']).toBe('MyTest');
  });

  it('should have the value of "top-ten" when running method checkUTM', () => {
    spyOn(component, 'checkUTM');
    component.checkUTM();
    expect(component.email.littleMoreUTM).toBe('top-ten');
  });

  it('should return an empty object when calling method "removeLink"', () => {
    component.links = [{show: "MyTest", link: "TestLink", addInterest: ""}];
    component.removeLink({show: "MyTest", link: "TestLink", addInterest: ""});
    expect(component.links).not.toContain({show: "MyTest", link: "TestLink", addInterest: ""});
  });

  it('should return false, and then true when calling "toggleControls" method', ()=>{
    component.showComponent = true;
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });
});
