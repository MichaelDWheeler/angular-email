import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-little-more',
  templateUrl: './little-more.component.html',
  styleUrls: ['./little-more.component.css'],
})
export class LittleMoreComponent implements OnInit {
  email: any = {};
  component = 'little more';
  showComponent: boolean;
  programLink: string;
  programTitle: string;
  programUTM: string;
  littleMore = 'littleMoreAddInterest';
  littleMoreUTM: string;
  littleMoreTitleRight: string;
  littleMoreDescription: string;
  links: any = [];

  addNewLink(): void {
    if (this.programLink === '' || this.programLink === 'undefined' || this.programLink == null) {
      const intentional = confirm('There is no link associated with this title. If this was intentional click "OK" otherwise select "Cancel"');
      if (intentional) {
        this.programLink = 'javascript:void(0)';
      } else {
        return;
      }
    }
    this.links.push({ show: this.programTitle, link: this.programLink, addInterest: this.email.littleMoreAddInterestConverted });
    this.programLink = '';
    this.programTitle = '';
    this.createTemplate.setVariables('littleMoreLinks', this.links);
  }

  checkUTM(): void {
    if (this.email.littleMoreUTM === '') {
      this.email.littleMoreUTM = 'top-ten';
    }
  }

  removeLink(link: any): void {
    const position = this.links.indexOf(link);
    this.links.splice(position, 1);
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.littleMoreDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public sanitizer: DomSanitizer,
    public createTemplate: CreateTemplateService,
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.checkUTM();
    this.links = this.createTemplate.getLinks(this.component);
    this.showComponent = this.email.littleMoreDisplay;
    this.littleMoreUTM = this.email.littleMoreUTM;
    this.littleMoreTitleRight = this.email.littleMoreTitleRight;
    this.littleMoreDescription = this.email.littleMoreDescription;
  }

}
