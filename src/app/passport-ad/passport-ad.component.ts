import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-passport-ad',
  templateUrl: './passport-ad.component.html',
  styleUrls: ['./passport-ad.component.css']
})

export class PassportAdComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  passportAdLink: string;
  passportAdUTM: string;
  passportAdAlt: string;
  passportAdImage: string;
  passportAdDescription: string;
  passportAdButton: string;

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public sanitizer: DomSanitizer
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = true;
    this.passportAdLink = this.email.passportAdLink;
    this.passportAdUTM = this.email.passportAdUTM;
    this.passportAdAlt = this.email.passportAdAlt;
    this.passportAdImage = this.email.passportAdImage;
    this.passportAdDescription = this.email.passportAdDescription;
    this.passportAdButton = this.email.passportAdButton;
  }

}
