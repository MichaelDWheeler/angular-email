import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassportAdComponent } from './passport-ad.component';
import { FormsModule } from '@angular/forms';

describe('PassportAdComponent', () => {
  let component: PassportAdComponent;
  let fixture: ComponentFixture<PassportAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassportAdComponent ],
      imports: [ FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassportAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
