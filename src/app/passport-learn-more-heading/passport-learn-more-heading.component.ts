import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-passport-learn-more-heading',
  templateUrl: './passport-learn-more-heading.component.html',
  styleUrls: ['./passport-learn-more-heading.component.css'],
})
export class PassportLearnMoreHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'passport heading';
  passportLearnMoreImage: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.passportLearnMoreHeadingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.passportLearnMoreHeadingDisplay;
    this.passportLearnMoreImage = this.email.passportLearnMoreImage;
  }

}
