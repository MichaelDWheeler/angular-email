import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { SingleImageSectionComponent } from './single-image-section.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestFullWidthComponent } from '../add-interest-full-width/add-interest-full-width.component';

describe('SingleImageSectionComponent', () => {
  let component: SingleImageSectionComponent;
  let fixture: ComponentFixture<SingleImageSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        SingleImageSectionComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestFullWidthComponent
      ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleImageSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });
});
