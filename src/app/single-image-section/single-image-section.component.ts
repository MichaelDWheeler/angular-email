import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-single-image-section',
  templateUrl: './single-image-section.component.html',
  styleUrls: ['./single-image-section.component.css'],
})
export class SingleImageSectionComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'single image content';
  singleImageSection = 'singleImageAddInterest';
  singleImageSource: string;
  singleImageLink: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.singleImageDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.singleImageDisplay;
    this.singleImageSource = this.email.singleImageSource;
    this.singleImageLink = this.email.singleImageLink;
  }

}
