import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  email: any = {};
  templateSelected: Number;

  constructor(
    public createTemplate: CreateTemplateService,
  ) { }

  ngOnInit() {
    this.createTemplate.getDate();
    this.email = this.createTemplate.getVariables();
    this.createTemplate.checkTemplateSelected();
  }

}
