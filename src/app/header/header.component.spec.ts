import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { TEMPLATEVARIABLES } from '../data-create-template';
import { CreateTemplateService } from '../create-template.service';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.email = Object.assign({}, TEMPLATEVARIABLES);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return a date', () => {
    component.createTemplate.getDate();
    expect(component.email.utmSource).toMatch(/\d\d\d\d-(\d)|(\d\d)-(\d)|(\d\d)/);
  });

  it('should create a reference to TEMPLATE VARIABLES', () => {
    component.email = component.createTemplate.getVariables();
    expect(component.email).toBe(TEMPLATEVARIABLES);
  });

  it('should return undefined', () => {
    component.createTemplate.checkTemplateSelected();
    expect(component.email.templateSelected).toBe(undefined);
  });

  it('should return the value as 1 when changing utmCampaign to "membership"', inject([CreateTemplateService], (createTemplate: CreateTemplateService) => {
    createTemplate.templateVariables.emailTemplate = 'Membership';
    createTemplate.checkTemplateSelected();
    expect(createTemplate.templateVariables.templateSelected).toBe(1);
  }));
});
