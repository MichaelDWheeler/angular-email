import { Injectable } from '@angular/core';
import { CreateTemplateService } from './create-template.service';

@Injectable({
  providedIn: 'root'
})
export class PagenameService {
  email: any = {} = this.createTemplate.getVariables();

  checkPage(): string {
    if (this.email.pagename !== '') {
      return 'pagename=' + this.email.pagename + '&';
    } else {
      return '';
    }
  }

  constructor(public createTemplate: CreateTemplateService) { }
}
