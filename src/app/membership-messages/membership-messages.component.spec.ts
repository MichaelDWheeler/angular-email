import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MembershipMessagesComponent } from './membership-messages.component';
import { EmailHeadingComponent } from '../email-heading/email-heading.component';
import { TextDescriptionComponent } from '../text-description/text-description.component';
import { DividerComponent } from '../divider/divider.component';
import { UpcomingShowsHeadingComponent } from '../upcoming-shows-heading/upcoming-shows-heading.component';
import { ComingSoonTopComponent } from '../coming-soon-top/coming-soon-top.component';
import { ComingSoonBottomComponent } from '../coming-soon-bottom/coming-soon-bottom.component';
import { ContentImageWrapComponent } from '../content-image-wrap/content-image-wrap.component';
import { ContentImageWrapHeadingComponent } from '../content-image-wrap-heading/content-image-wrap-heading.component';
import { LittleMoreHeadingComponent } from '../little-more-heading/little-more-heading.component';
import { LittleMoreComponent } from '../little-more/little-more.component';
import { SingleColumnHeadingComponent } from '../single-column-heading/single-column-heading.component';
import { SingleColumnContentComponent } from '../single-column-content/single-column-content.component';
import { PartnerSpotlightHeadingComponent } from '../partner-spotlight-heading/partner-spotlight-heading.component';
import { PartnerSpotlightComponent } from '../partner-spotlight/partner-spotlight.component';
import { partnerSpotlightTwoHeadingComponent } from '../partner-spotlight-heading-two/partner-spotlight-heading-two.component';
import { PassportLearnMoreHeadingComponent } from '../passport-learn-more-heading/passport-learn-more-heading.component';
import { PartnerSpotlightTwoComponent } from '../partner-spotlight-two/partner-spotlight-two.component';
import { PassportLearnMoreComponent } from '../passport-learn-more/passport-learn-more.component';
import { SingleImageSectionComponent } from '../single-image-section/single-image-section.component';
import { FooterComponent } from '../footer/footer.component';
import { AppComponent } from '../app.component';
import { TopSingleColumnComponent } from '../top-single-column/top-single-column.component';
import { HeaderComponent } from '../header/header.component';
import { UpcomingShowsColumnsTopComponent } from '../upcoming-shows-columns-top/upcoming-shows-columns-top.component';
import { ContentComponent } from '../content/content.component';
import { CodeOutComponent } from '../code-out/code-out.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('MembershipMessagesComponent', () => {
  let component: MembershipMessagesComponent;
  let fixture: ComponentFixture<MembershipMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        MembershipMessagesComponent,
        EmailHeadingComponent,
        TextDescriptionComponent,
        DividerComponent,
        UpcomingShowsHeadingComponent,
        ComingSoonTopComponent,
        ComingSoonBottomComponent,
        ContentImageWrapComponent,
        ContentImageWrapHeadingComponent,
        LittleMoreHeadingComponent,
        LittleMoreComponent,
        SingleColumnHeadingComponent,
        SingleColumnContentComponent,
        PartnerSpotlightHeadingComponent,
        partnerSpotlightTwoHeadingComponent,
        PassportLearnMoreHeadingComponent,
        PartnerSpotlightTwoComponent,
        PartnerSpotlightComponent,
        PassportLearnMoreComponent,
        SingleImageSectionComponent,
        FooterComponent,
        AppComponent,
        TopSingleColumnComponent,
        HeaderComponent,
        UpcomingShowsColumnsTopComponent,
        ContentComponent,
        CodeOutComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [ FormsModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembershipMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
