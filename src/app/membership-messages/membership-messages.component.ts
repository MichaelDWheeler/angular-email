import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-membership-messages',
  templateUrl: './membership-messages.component.html',
  styleUrls: ['./membership-messages.component.css']
})
export class MembershipMessagesComponent implements OnInit {
  email: any = {};
  showComponent = true;

  constructor(
    public createTemplate: CreateTemplateService,
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
  }

}
