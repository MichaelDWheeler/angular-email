import { Injectable } from '@angular/core';
import { SUPPORTERS } from './data-supporters';

@Injectable({
  providedIn: 'root'
})
export class AddSupportersService {
  supporters = SUPPORTERS;
  getSupporters() {
    return this.supporters;
  }
  constructor() { }
}
