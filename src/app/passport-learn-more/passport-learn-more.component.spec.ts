import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassportLearnMoreComponent } from './passport-learn-more.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { FormsModule } from '@angular/forms';
import { AddInterestFullWidthComponent } from '../add-interest-full-width/add-interest-full-width.component';

describe('PassportLearnMoreComponent', () => {
  let component: PassportLearnMoreComponent;
  let fixture: ComponentFixture<PassportLearnMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        PassportLearnMoreComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestFullWidthComponent
       ],
       imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassportLearnMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });
});
