import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-passport-learn-more',
  templateUrl: './passport-learn-more.component.html',
  styleUrls: ['./passport-learn-more.component.css'],
})
export class PassportLearnMoreComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'passport content';
  passportLearnMore = 'passportLearnMoreAddInterest';
  passportLearnMoreTitle: string;
  passportLearnMoreDescription: string;
  passportLearnMoreButton: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.passportLearnMoreDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public sanitizer: DomSanitizer,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.passportLearnMoreDisplay;
    this.passportLearnMoreTitle = this.email.passportLearnMoreTitle;
    this.passportLearnMoreDescription = this.email.passportLearnMoreDescription;
    this.passportLearnMoreButton = this.email.passportLearnMoreButton;
  }

}
