import { Injectable } from '@angular/core';
import { PREHEADERTEXT } from './data-preheader-text';

@Injectable({
  providedIn: 'root'
})
export class PreheaderTextService {
  preheaderText = PREHEADERTEXT;

  getPreheader(): any {
    return this.preheaderText;
  }

  changePreheaderText(val: string): any {
    return this.preheaderText.subject = val;
  }

  constructor() { }
}
