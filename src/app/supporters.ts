export interface Supporters {
  image: string;
  link: string;
  alt: string;
}
