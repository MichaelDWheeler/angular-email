import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-streaming-now-header',
  templateUrl: './streaming-now-header.component.html',
  styleUrls: ['./streaming-now-header.component.css']
})
export class StreamingNowHeaderComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  streamingNow: string;

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = true;
    this.streamingNow = this.email.streamingNow;
  }

}
