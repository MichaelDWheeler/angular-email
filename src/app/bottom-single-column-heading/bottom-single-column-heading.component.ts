import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-bottom-single-column-heading',
  templateUrl: './bottom-single-column-heading.component.html',
  styleUrls: ['./bottom-single-column-heading.component.css']
})
export class BottomSingleColumnHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'Heading';
  bottomSingleColumnHeading: string;
  bottomSingleColumnAlt: string;
  bottomSingleColumnUTM: string;
  bottomSingleColumnButton: string;

  addAll(): void {
    this.bottomSingleColumnAlt = this.bottomSingleColumnHeading;
    this.bottomSingleColumnUTM = this.createTemplate.formatUTMSource(this.bottomSingleColumnHeading);
    this.bottomSingleColumnButton = 'LEARN MORE';
    this.createTemplate.setVariables('bottomSingleColumnHeading', this.bottomSingleColumnHeading);
    this.createTemplate.setVariables('bottomSingleColumnAlt', this.bottomSingleColumnHeading);
    this.createTemplate.setVariables('bottomSingleColumnUTM', this.bottomSingleColumnHeading);
    this.createTemplate.setVariables('bottomSingleColumnButton', 'LEARN MORE');
    this.createTemplate.setStorage();
  }

  setVariables(): void {
    if (this.email.bottomSingleColumnAlt === '') {
      this.createTemplate.setVariables('bottomSingleColumnAlt', this.bottomSingleColumnHeading);
    }
    if (this.email.bottomSingleColumnUTM === '') {
      this.createTemplate.setVariables('bottomSingleColumnUTM', this.bottomSingleColumnHeading);
    }
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.bottomSingleHeadingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService, 
    public select: SelectTextService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.bottomSingleHeadingDisplay;
    this.bottomSingleColumnHeading = this.email.bottomSingleColumnHeading;
    this.setVariables();
  }

}
