import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { BottomSingleColumnHeadingComponent } from './bottom-single-column-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('BottomSingleColumnHeadingComponent', () => {
  let component: BottomSingleColumnHeadingComponent;
  let fixture: ComponentFixture<BottomSingleColumnHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BottomSingleColumnHeadingComponent,
        RestoreButtonComponent,
        RemoveButtonComponent
      ],
      imports: [FormsModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSingleColumnHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return an object with specified properties', () => {
    component.email.bottomSingleColumnHeading = "Testing Multiple Components";
    component.addAll();
    expect(component.email.bottomSingleColumnHeading).toEqual("Testing Multiple Components");
    expect(component.email.bottomSingleColumnAlt).toEqual("Testing Multiple Components");
    expect(component.email.bottomSingleColumnUTM).toEqual("Testing%20Multiple%20Components");
    expect(component.email.bottomSingleColumnButton).toEqual('LEARN MORE');
  });

  it('should change bottomSingleColumnAlt and bottomSingleColumnUTM', () => {
    component.email = component.createTemplate.getVariables();
    component.email.bottomSingleColumnAlt = '';
    component.email.bottomSingleColumnUTM = '';
    component.email.bottomSingleColumnHeading = "This is a test";
    component.setVariables();
    expect(component.email.bottomSingleColumnAlt).toEqual("This is a test");
    expect(component.email.bottomSingleColumnUTM).toEqual("This%20is%20a%20test");
  });

});
