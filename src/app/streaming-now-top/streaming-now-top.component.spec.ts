import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamingNowTopComponent } from './streaming-now-top.component';
import { FormsModule } from '@angular/forms';

describe('StreamingNowTopComponent', () => {
  let component: StreamingNowTopComponent;
  let fixture: ComponentFixture<StreamingNowTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StreamingNowTopComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamingNowTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the values of email.streamingNowTopRightAlt and email.streamingNowTopRightUTM', () => { 
    component.email.streamingNowTopRightProgram = 'This is a test';
    component.addVariables();
    expect(component.email.streamingNowTopRightAlt).toBe('This is a test');
    expect(component.email.streamingNowTopRightUTM).toBe('This%20is%20a%20test');
  });
});
