import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-streaming-now-top',
  templateUrl: './streaming-now-top.component.html',
  styleUrls: ['./streaming-now-top.component.css']
})
export class StreamingNowTopComponent implements OnInit, OnDestroy {
  email: any = {};
  streamingNowTopLeftLink: string;
  streamingNowTopLeftUTM: string;
  streamingNowTopLeftAlt: string;
  streamingNowTopLeftImage: string;
  streamingNowTopLeftProgram: string;
  streamingNowTopLeftTitle: string;
  streamingNowTopLeftDescription: string;
  streamingNowTopRightLink: string;
  streamingNowTopRightUTM: string;
  streamingNowTopRightAlt: string;
  streamingNowTopRightImage: string;
  streamingNowTopRightProgram: string;
  streamingNowTopRightTitle: string;
  streamingNowTopRightDescription: string;
  subscription: Subscription;

  addVariables(): void {
    this.createTemplate.setVariables('streamingNowTopLeftAlt', this.streamingNowTopLeftProgram);
    this.createTemplate.setVariables('streamingNowTopLeftUTM', this.streamingNowTopLeftProgram);
    this.streamingNowTopLeftAlt = this.email.streamingNowTopLeftAlt;
    this.streamingNowTopLeftUTM = this.email.streamingNowTopLeftUTM;
    this.createTemplate.setVariables('streamingNowTopRightAlt', this.streamingNowTopRightProgram);
    this.createTemplate.setVariables('streamingNowTopRightUTM', this.streamingNowTopRightProgram);
    this.streamingNowTopRightAlt = this.email.streamingNowTopRightAlt;
    this.streamingNowTopRightUTM = this.email.streamingNowTopRightUTM;
    this.createTemplate.setStorage();
  }

  selectAllText($event: any): void {
    this.select.selectAllText($event);
  }

  utmSubscription(): void{
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.streamingNowTopLeftUTM = this.email.streamingNowTopLeftUTM;
        this.streamingNowTopRightUTM = this.email.streamingNowTopRightUTM;
      }
    });
  }
  
  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.streamingNowTopLeftLink = this.email.streamingNowTopLeftLink;
    this.streamingNowTopLeftUTM = this.email.streamingNowTopLeftUTM;
    this.streamingNowTopLeftAlt = this.email.streamingNowTopLeftAlt;
    this.streamingNowTopLeftImage = this.email.streamingNowTopLeftImage;
    this.streamingNowTopLeftProgram = this.email.streamingNowTopLeftProgram;
    this.streamingNowTopLeftTitle = this.email.streamingNowTopLeftTitle;
    this.streamingNowTopLeftDescription = this.email.streamingNowTopLeftDescription;
    this.streamingNowTopRightLink = this.email.streamingNowTopRightLink;
    this.streamingNowTopRightUTM = this.email.streamingNowTopRightUTM;
    this.streamingNowTopRightAlt = this.email.streamingNowTopRightAlt;
    this.streamingNowTopRightImage = this.email.streamingNowTopRightImage;
    this.streamingNowTopRightProgram = this.email.streamingNowTopRightProgram;
    this.streamingNowTopRightTitle = this.email.streamingNowTopRightTitle;
    this.streamingNowTopRightDescription = this.email.streamingNowTopRightDescription;
    this.utmSubscription();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
