import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CheckEmailFieldsService } from '../check-email-fields.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-top-single-column',
  templateUrl: './top-single-column.component.html',
  styleUrls: ['./top-single-column.component.css']
})
export class TopSingleColumnComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  topSingleColumnAirDate: string;
  topSingleColumnDescription: string;
  topSingleColumnImageAlt: string;
  topSingleColumnLink: string;
  topSingleColumnUTM: string;
  topSingleColumnImageSource: string;
  topSingleColumnProgram: string;
  topSingleColumnTitle: string
  subscription: Subscription;
  message: any;

  addVariables(): void {
    this.createTemplate.setVariables('topSingleColumnImageAlt', this.topSingleColumnProgram);
    this.createTemplate.setVariables('topSingleColumnUTM', this.topSingleColumnProgram);
    this.createTemplate.changeVariables('topSingleColumnProgram', this.topSingleColumnProgram);
    this.topSingleColumnImageAlt = this.email.topSingleColumnImageAlt;
    this.topSingleColumnUTM = this.email.topSingleColumnUTM;
    console.log(this.email.topSingleColumnUTM);
    this.createTemplate.setStorage();
  }

  utmSubscription(): void{
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.topSingleColumnUTM = this.email.topSingleColumnUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer,
    public check: CheckEmailFieldsService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = true;
    this.topSingleColumnAirDate = this.email.topSingleColumnAirDate;
    this.topSingleColumnDescription = this.email.topSingleColumnDescription;
    this.topSingleColumnImageAlt = this.email.topSingleColumnImageAlt;
    this.topSingleColumnLink = this.email.topSingleColumnLink;
    this.topSingleColumnUTM = this.email.topSingleColumnUTM;
    this.topSingleColumnImageSource = this.email.topSingleColumnImageSource;
    this.topSingleColumnProgram = this.email.topSingleColumnProgram;
    this.topSingleColumnTitle = this.email.topSingleColumnTitle;
    this.utmSubscription();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
