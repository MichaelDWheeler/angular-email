import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopSingleColumnComponent } from './top-single-column.component';
import { FormsModule } from '@angular/forms';

describe('TopSingleColumnComponent', () => {
  let component: TopSingleColumnComponent;
  let fixture: ComponentFixture<TopSingleColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopSingleColumnComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopSingleColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the values of email.topSingleColumnImageAlt and email.topSingleColumnUTM', () => { 
    component.email.topSingleColumnProgram = 'This is a test';
    component.addVariables();
    expect(component.email.topSingleColumnImageAlt).toBe('This is a test');
    expect(component.email.topSingleColumnUTM).toBe('This%20is%20a%20test');
  });
});
