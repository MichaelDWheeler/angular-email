    ;
    (function() {
        var cards = document.getElementsByClassName('socal-card');
        var cardHeight = 0;
        var findLargestCard = function() {
            for (var i = 0, length = cards.length; i < length; i++) {
                var height = cards[i].clientHeight;
                if (height > cardHeight) {
                    cardHeight = height;
                }
            }
        };
        var setCardHeights = function() {
            for (var i = 0, length = cards.length; i < length; i++) {
                cards[i].style.height = cardHeight + "px";
            }
        };
        var checkForLength = function() {
            if (cards.length === 0) {
                return true
            } else {
                clearInterval(whenCardsLoad);
                return false
            }
        };
        var whenCardsLoad = setInterval(function() {
            if (checkForLength()) {
                return
            } else {
                window.setTimeout(function() {
                    findLargestCard();
                    setCardHeights();
                    window.clearInterval(whenCardsLoad);
                }, 300);
            }
        }, 100);
        var resetCards = function() {
            cardHeight = 0;
            for (var i = 0, length = cards.length; i < length; i++) {
                cards[i].style.height = "initial";
            }
        };
        var addEvent = function(evnt, elem, func) {
            if (elem.addEventListener) // W3C DOM
                elem.addEventListener(evnt, func, false);
            else if (elem.attachEvent) { // IE DOM
                elem.attachEvent("on" + evnt, func);
            } else { // No much to do
                elem[evnt] = func;
            }
        }
        var removeEvent = function(evnt, elem, func) {
            if (elem.removeEventListener) // W3C DOM
                elem.removeEventListener(evnt, func, false);
            else if (elem.attachEvent) { // IE DOM
                elem.detachEvent("on" + evnt, func);
            } else { // No much to do
                elem[evnt] = func;
            }
        }
        var scrollBy;
        var mouseDownID = -1;
        var booksContainer = document.getElementById('telescope-widget');
        var buttonDown = document.getElementById('arrow-down');
        var buttonUp = document.getElementById('arrow-up');
        var whileMouseDown = function() {
            if (mouseDownID != -1) {
                booksContainer.scrollTop += scrollBy;
                scrollBy += 0.25;
            }
        };
        var whileMouseUp = function() {
            if (mouseDownID != -1) {
                booksContainer.scrollTop -= scrollBy;
                scrollBy += 0.25;
            }
        };
        var mouseDown = function(event) {
            scrollBy = 0.5;
            if (mouseDownID == -1) {
                mouseDownID = setInterval(whileMouseDown, 10);
            }
        };
        var mouseUp = function(event) {
            if (mouseDownID != -1) {
                mouseDownID = -1;
            } else {
                scrollBy = 0.5;
                mouseDownID = setInterval(whileMouseUp, 10);
            }
        }
        var endMouseDown = function() {
            window.clearInterval(mouseDownID);
            mouseDownID = -1;
        };
        var getMouseDownEvent = function() {
            removeEvent('mouseup', buttonDown, endMouseDown);
            removeEvent('mouseup', buttonUp, endMouseDown);
            var pageWidth = document.body.clientWidth;
            if (pageWidth <= 767) {
                return "touchstart"
            } else {
                addEvent('mouseup', buttonDown, endMouseDown);
                addEvent('mouseup', buttonUp, endMouseDown);
                return "mousedown"
            }
        };
        var setScrollButtons = function() {
            var mouseDownEvent = getMouseDownEvent();
            removeEvent('touchstart', buttonDown, mouseDown);
            removeEvent('mousedown', buttonDown, mouseDown);
            removeEvent('mouseout', buttonDown, endMouseDown);
            removeEvent('touchstart', buttonUp, mouseUp);
            removeEvent('mousedown', buttonUp, mouseUp);
            removeEvent('mouseout', buttonUp, endMouseDown);
            addEvent(mouseDownEvent, buttonDown, mouseDown);
            addEvent('touchend', buttonDown, endMouseDown);
            addEvent(mouseDownEvent, buttonUp, mouseUp);
            addEvent('touchend', buttonUp, endMouseDown);
        };
        window.onresize = function() {
            resetCards();
            findLargestCard();
            setCardHeights();
            setScrollButtons();
        };
        var loadVoteNow = function(){
            document.addEventListener('DOMContentLoaded', function() {
            Telescope.loadApplication({
                parentId: 'telescope-widget',
                url: 'https://tgar.votenow.tv',
                allowStoredPosition: true
                });
            });
        };
        var getContainerCoordinates = function(){
            var containerPos = document.getElementsByClassName('entry-content')[0];
            var elPos = containerPos.offsetLeft;
            return elPos;
        };
        var onPageAnchors = document.getElementsByClassName('on-page-nav');
        var changeAnchorColor = function(color){
            for(var i = 0, length = onPageAnchors.length; i < length; i++){
                onPageAnchors[i].style.color = color;
            }
        };
        var stickyNavSet = false;
        var voteLink = document.getElementById('vote-link');
        var unsetStickyNav = function(onPageNav, elPos){
            if(elPos.top > 0 && stickyNavSet === true){
                onPageNav.className = "onpage-menu animated";
                onPageNav.className += " slideOutUp"; 
                var timer = window.setTimeout(function(){
                    onPageNav.style.visibility = "visible";
                    onPageNav.style.position = "static";
                    onPageNav.style.height = "50px";
                    onPageNav.style.paddingTop = "0";
                    onPageNav.style.backgroundColor = "#fff";
                    onPageNav.style.color = "#000";
                    voteLink.style.marginLeft = "0";
                    changeAnchorColor("#000");
                    stickyNavSet = false;
                    onPageNav.className = "onpage-menu animated";
            },50);
            }                
        };
        var checkIfSet = function(onPageNav){
            if(stickyNavSet === false){
                stickyNavSet = true;
                onPageNav.className = "onpage-menu animated";
                onPageNav.className += " slideInDown";                    
            }     
        };
        var setStickyNav = function(onPageNav){
            var left = getContainerCoordinates();
            onPageNav.style.position = "fixed";
            onPageNav.style.height = "40px";
            onPageNav.style.paddingTop = "5px";
            onPageNav.style.top = "0";
            onPageNav.style.left = "0";
            onPageNav.style.backgroundColor = "#000";
            voteLink.style.marginLeft = left + "px";
            checkIfSet(onPageNav);
            changeAnchorColor("#fff");
        };   
        var stickyNav = function(){
            var marker = document.getElementById('marker');
            var onPageNav = document.getElementById('onpage-menu');
            document.onscroll = function(){
                var elPos = marker.getBoundingClientRect();
                if(elPos.top <= 10){
                    setStickyNav(onPageNav);
                } else {
                    unsetStickyNav(onPageNav, elPos);
                }
            };

        };
var addPlayButton = function(){
    var videoContainer = document.getElementsByClassName('video-thumb');
    for(var i = 0, count = videoContainer.length; i < count; i++){
        if(!videoContainer[i].querySelector("div [class='overlay']")){
            var anchor = videoContainer[i].getElementsByTagName('figure')[0];
            var docFrag = document.createDocumentFragment();
            var div = document.createElement("div");
            div.setAttribute("class", "overlay");
            var span = document.createElement("span");
            span.setAttribute("class", "video-icon");
            var fontAwesome = document.createElement("i");
            fontAwesome.setAttribute("class", "fa fa-play-circle");
            span.appendChild(fontAwesome);
            div.appendChild(span);
            docFrag.appendChild(div);
            anchor.appendChild(docFrag);
        }
    }
}
        setScrollButtons();
        loadVoteNow();
        stickyNav();
        addPlayButton();
    })();