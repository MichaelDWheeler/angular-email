import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { GlobalUtmComponent } from './global-utm.component';
import { TEMPLATEVARIABLES } from '../data-create-template';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('GlobalUtmComponent', () => {
  let component: GlobalUtmComponent;
  let fixture: ComponentFixture<GlobalUtmComponent>;
  let title: Title;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [GlobalUtmComponent],
      imports: [FormsModule],
      providers: [Title]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalUtmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should spy on changePath', () => {
    const optionName = 'Program Highlights';
    const spy = spyOn(component, 'changePath')
    component.changePath(optionName);
    expect(spy).toHaveBeenCalled();
  });

  it('should change the window.location.href of the spy', () => {
    const optionName = 'Program Highlights';
    const spy = spyOn(component, 'changePath').and.callFake(() => {
      let location: string;
      if (optionName === 'Program Highlights') {
        location = '/program-highlights';
      } else if (optionName === 'Membership Messages') {
        location = '/membership-messages';
      }
      return location;
    });
    expect(spy()).toBe('/program-highlights');
  });

  it('should spy on onTemplateSelect', () => {
    const spy = spyOn(component, 'onTemplateSelect');
    component.onTemplateSelect('test');
    expect(spy).toHaveBeenCalled();
  });

  it('page title should be "THIS IS A TEST"', () => {
    component.setTitle('THIS IS A TEST');
    title = TestBed.get(Title);
    expect(title.getTitle()).toBe('THIS IS A TEST');
  });

  it('should change the subject line to "THIS IS A SUBJECT LINE TEST"', () => {
    component.email.luminateSubjectLine = 'THIS IS A SUBJECT LINE TEST';
    component.changeSubjectLine();
    title = TestBed.get(Title);
    expect(title.getTitle()).toEqual('THIS IS A SUBJECT LINE TEST');
  });

  it('should change the pagename to "convio_snippets', () => {
    component.templateSelected = 1;
    component.email.pagename = 'noPageName';
    component.pageName();
    expect(component.email.pagename).toBe('convio_snippets');
  });

  it('should create an email object that is equal to TEMPLATE VARIABLES', () => {
    expect(component.email).toBe(TEMPLATEVARIABLES);
  });

  it('should contain an object with paths to both program-highlights and membership-services', () => {
    expect(component.option.length).toBe(4);
    expect(component.option).toEqual([
      { id: 0, name: 'Dedicated', path: '/dedicated' },
      { id: 1, name: 'Membership', path: '/membership' },
      { id: 2, name: 'Passport', path: '/passport' },
      { id: 3, name: 'Program Highlights', path: '/program-highlights' }
    ]);
  });

  it('should return "undefined" on load when template has not been selected', () => {
    expect(component.templateSelected).toBeUndefined();
  });

  it('should return "3" when Program Highlights is selected as the email template', () => {
    component.email.emailTemplate = 'Program Highlights';
    component.templateSelected = component.createTemplate.checkTemplateSelected();
    expect(component.templateSelected).toBe(3);
  });

  it('should return an empty string when checking pagename', () => {
    expect(component.email.pagenameConverted).toBe('');
  });

  it('should return a custom value for pagenameconverted when changing the pagename', () => {
    component.email.pagename = 'Testing';
    component.email.pagenameConverted = component.page.checkPage();
    expect(component.email.pagenameConverted).toBe('pagename=Testing&');
  });

});
