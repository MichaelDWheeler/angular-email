import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { TemplateSelectService } from '../template-select.service';
import { PagenameService } from '../pagename.service';

@Component({
  selector: 'app-global-utm',
  templateUrl: './global-utm.component.html',
  styleUrls: ['./global-utm.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class GlobalUtmComponent implements OnInit {
  @Input() show: boolean;
  date: Date;
  email: any = {};
  luminateSubjectLine: string;
  membershipSelected: Number;
  membershipUTMSource: any;
  option: any;
  pagename: string;
  superSubjectLink: string;
  templateSelected: Number;
  utmCampaign: string;
  utmSource: string;

  changePath(optionName: string): void {
    if (optionName === 'Program Highlights') {
      window.location.href = '/program-highlights';
    } else if (optionName === 'Membership') {
      window.location.href = '/membership';
    } else if (optionName === 'Passport') {
      window.location.href = '/passport';
    } else if (optionName === 'Dedicated') {
      window.location.href = '/dedicated';
    }
  }

  changeSubjectLine(): void {
    const newTitle = this.email.luminateSubjectLine;
    this.setTitle(newTitle);
  }

  determineUTMSource(): void {
    if (this.templateSelected === 3) {
      if(this.utmSource){
        let dateString = this.email.utmSource.split('-');
        this.date = new Date(dateString);      
      } else {
        this.date = this.createTemplate.nextSaturday();
      }   
    } else if (this.templateSelected === 2) {
      this.utmSource = `Passport Newsletter ${this.options.yearWithMonth()}`;
    } else if (this.templateSelected === 0) {
      this.utmSource = this.createTemplate.formatUTMSource(this.email.superSubjectLink);
    } else if (this.templateSelected === 1) {
      this.membershipUTMSource = this.options.getMembershipOptions();
    }
  }

  formatDate(event: any): void {
    this.utmSource = this.createTemplate.formatDate(event);
  }

  onMembershipSelect(val: any): void {
    const membershipName = this.membershipUTMSource[val].source;
    this.email.utmSource = membershipName;
  }

  onTemplateSelect(val: any): void {
    const clearStorage = this.createTemplate.clearLocalStorageWarning();
    if (clearStorage) {
      localStorage.clear();
      const optionName = this.option[val].name;
      this.email.emailTemplate = optionName;
      this.createTemplate.setVariables('utmCampaign', optionName.toLowerCase());
      this.changePath(optionName);
    } else {
      this.createTemplate.setStorage();
      location.reload();
    }
  }

  pageName(): void {
    if ((this.templateSelected === 1 && this.email.pagename === 'noPageName') || (this.templateSelected === 2 && this.email.pagename === 'noPageName')  || this.email.pagename === 'convio_snippets') {
      this.pagename = 'convio_snippets';
      this.email.pagename = 'convio_snippets';
    } else {
      this.pagename = '';
      this.email.pagename = '';
    }
  }

  setTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle);
  }

  resumeTemplateSelected(paths: any, templateSelected: any): void {
    if(templateSelected === null){
      return;
    }
    const location = window.location.href;
    const pathArray = location.split('/');
    const path = '/' + pathArray[pathArray.length - 1];
    if(paths[templateSelected]['path'] !== path) {
      window.location.href = paths[templateSelected]['path'];
    }
  }

  constructor(
    public createTemplate: CreateTemplateService,
    private titleService: Title,
    public select: SelectTextService,
    private options: TemplateSelectService,
    public page: PagenameService,
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.luminateSubjectLine = this.email.luminateSubjectLine;
    this.membershipSelected = this.email.membershipSelected;
    this.superSubjectLink = this.email.superSubjectLink;
    this.templateSelected = this.email.templateSelected;
    this.utmCampaign = this.email.utmCampaign;
    this.utmSource = this.email.utmSource;
    this.option = this.options.getOptions();
    this.pagename = this.email.pagename;
    this.templateSelected = this.createTemplate.checkTemplateSelected();
    this.resumeTemplateSelected(this.option, this.templateSelected);
    this.pageName();
    this.email.pagenameConverted = this.page.checkPage();
    this.determineUTMSource();
  }
}
