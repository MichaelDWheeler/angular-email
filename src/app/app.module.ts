import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';

import { AppComponent } from './app.component';
import { TopSingleColumnComponent } from './top-single-column/top-single-column.component';
import { HeaderComponent } from './header/header.component';
import { UpcomingShowsColumnsTopComponent } from './upcoming-shows-columns-top/upcoming-shows-columns-top.component';
import { FooterComponent } from './footer/footer.component';
import { WhatsOnComponent } from './whats-on/whats-on.component';
import { GlobalUtmComponent } from './global-utm/global-utm.component';
import { UpcomingShowsColumnsBottomComponent } from './upcoming-shows-columns-bottom/upcoming-shows-columns-bottom.component';
import { AlsoEnjoyComponent } from './also-enjoy/also-enjoy.component';
import { AlsoEnjoyLinksComponent } from './also-enjoy-links/also-enjoy-links.component';
import { StreamingNowHeaderComponent } from './streaming-now-header/streaming-now-header.component';
import { StreamingNowTopComponent } from './streaming-now-top/streaming-now-top.component';
import { StreamingNowBottomComponent } from './streaming-now-bottom/streaming-now-bottom.component';
import { PassportLogoHeaderComponent } from './passport-logo-header/passport-logo-header.component';
import { PassportAdComponent } from './passport-ad/passport-ad.component';
import { MonthlyFunHeadingComponent } from './monthly-fun-heading/monthly-fun-heading.component';
import { MonthlyFunSectionComponent } from './monthly-fun-section/monthly-fun-section.component';
import { BottomSingleColumnHeadingComponent } from './bottom-single-column-heading/bottom-single-column-heading.component';
import { BottomSingleColumnSectionComponent } from './bottom-single-column-section/bottom-single-column-section.component';
import { ProvidingSupportHeadingComponent } from './providing-support-heading/providing-support-heading.component';
import { ProvidingSupportAdsComponent } from './providing-support-ads/providing-support-ads.component';
import { CodeOutComponent } from './code-out/code-out.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { ExtraSingleColumnComponent } from './extra-single-column/extra-single-column.component';
import { ExtraSingleColumnHeadingComponent } from './extra-single-column-heading/extra-single-column-heading.component';
import { EmailHeadingComponent } from './email-heading/email-heading.component';
import { TextDescriptionComponent } from './text-description/text-description.component';
import { UpcomingShowsHeadingComponent } from './upcoming-shows-heading/upcoming-shows-heading.component';
import { SingleColumnHeadingComponent } from './single-column-heading/single-column-heading.component';
import { SingleColumnContentComponent } from './single-column-content/single-column-content.component';
import { ComingSoonTopComponent } from './coming-soon-top/coming-soon-top.component';
import { ComingSoonBottomComponent } from './coming-soon-bottom/coming-soon-bottom.component';
import { PassportLearnMoreComponent } from './passport-learn-more/passport-learn-more.component';
import { SingleImageSectionComponent } from './single-image-section/single-image-section.component';
import { PartnerSpotlightComponent } from './partner-spotlight/partner-spotlight.component';
import { PassportLearnMoreHeadingComponent } from './passport-learn-more-heading/passport-learn-more-heading.component';
import { PartnerSpotlightHeadingComponent } from './partner-spotlight-heading/partner-spotlight-heading.component';
import { partnerSpotlightTwoHeadingComponent } from './partner-spotlight-heading-two/partner-spotlight-heading-two.component';
import { PartnerSpotlightTwoComponent } from './partner-spotlight-two/partner-spotlight-two.component';
import { RemoveButtonComponent } from './remove-button/remove-button.component';
import { RestoreButtonComponent } from './restore-button/restore-button.component';
import { DividerComponent } from './divider/divider.component';
import { ContentImageWrapComponent } from './content-image-wrap/content-image-wrap.component';
import { ContentImageWrapHeadingComponent } from './content-image-wrap-heading/content-image-wrap-heading.component';
import { LittleMoreHeadingComponent } from './little-more-heading/little-more-heading.component';
import { LittleMoreComponent } from './little-more/little-more.component';
import { AddInterestComponent } from './add-interest/add-interest.component';
import { AddInterestFullWidthComponent } from './add-interest-full-width/add-interest-full-width.component';
import { PassportTemplateComponent } from './passport-template/passport-template.component';
import { BannerAdComponent } from './banner-ad/banner-ad.component';
import { TitleAndTextComponent } from './title-and-text/title-and-text.component';
import { ComingSoonTwoComponent } from './coming-soon-two/coming-soon-two.component';
import { ComingSoonThreeComponent } from './coming-soon-three/coming-soon-three.component';
import { ComingSoonFourComponent } from './coming-soon-four/coming-soon-four.component';
import { ClosingComponent } from './closing/closing.component';
import { DedicatedTemplateComponent } from './dedicated-template/dedicated-template.component';
import { UpcomingShowsColumnsTwoComponent } from './upcoming-shows-columns-two/upcoming-shows-columns-two.component';
import { UpcomingShowsColumnsThreeComponent } from './upcoming-shows-columns-three/upcoming-shows-columns-three.component';
import { StreamingNowTwoComponent } from './streaming-now-two/streaming-now-two.component';


@NgModule({
  schemas: [NO_ERRORS_SCHEMA],
  declarations: [
    AppComponent,
    TopSingleColumnComponent,
    HeaderComponent,
    UpcomingShowsColumnsTopComponent,
    FooterComponent,
    WhatsOnComponent,
    GlobalUtmComponent,
    UpcomingShowsColumnsBottomComponent,
    AlsoEnjoyComponent,
    AlsoEnjoyLinksComponent,
    StreamingNowHeaderComponent,
    StreamingNowTopComponent,
    StreamingNowBottomComponent,
    PassportLogoHeaderComponent,
    PassportAdComponent,
    MonthlyFunHeadingComponent,
    MonthlyFunSectionComponent,
    BottomSingleColumnHeadingComponent,
    BottomSingleColumnSectionComponent,
    ProvidingSupportHeadingComponent,
    ProvidingSupportAdsComponent,
    CodeOutComponent,
    routingComponents,
    ExtraSingleColumnComponent,
    ExtraSingleColumnHeadingComponent,
    EmailHeadingComponent,
    TextDescriptionComponent,
    UpcomingShowsHeadingComponent,
    SingleColumnHeadingComponent,
    SingleColumnContentComponent,
    ComingSoonTopComponent,
    ComingSoonBottomComponent,
    PassportLearnMoreComponent,
    SingleImageSectionComponent,
    PartnerSpotlightComponent,
    PassportLearnMoreHeadingComponent,
    PartnerSpotlightHeadingComponent,
    partnerSpotlightTwoHeadingComponent,
    PartnerSpotlightTwoComponent,
    RemoveButtonComponent,
    RestoreButtonComponent,
    DividerComponent,
    ContentImageWrapComponent,
    ContentImageWrapHeadingComponent,
    LittleMoreHeadingComponent,
    LittleMoreComponent,
    AddInterestComponent,
    AddInterestFullWidthComponent,
    PassportTemplateComponent,
    BannerAdComponent,
    TitleAndTextComponent,
    ComingSoonTwoComponent,
    ComingSoonThreeComponent,
    ComingSoonFourComponent,
    ClosingComponent,
    DedicatedTemplateComponent,
    UpcomingShowsColumnsTwoComponent,
    UpcomingShowsColumnsThreeComponent,
    StreamingNowTwoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
