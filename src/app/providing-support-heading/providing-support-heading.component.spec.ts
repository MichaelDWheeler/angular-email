import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidingSupportHeadingComponent } from './providing-support-heading.component';
import { FormsModule } from '@angular/forms';

describe('ProvidingSupportHeadingComponent', () => {
  let component: ProvidingSupportHeadingComponent;
  let fixture: ComponentFixture<ProvidingSupportHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidingSupportHeadingComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidingSupportHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
