import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-providing-support-heading',
  templateUrl: './providing-support-heading.component.html',
  styleUrls: ['./providing-support-heading.component.css']
})
export class ProvidingSupportHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  constructor(
    public createTemplate: CreateTemplateService,
    public selectText: SelectTextService,
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = true;
  }

}
