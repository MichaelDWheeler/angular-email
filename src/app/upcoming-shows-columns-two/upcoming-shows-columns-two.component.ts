import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-upcoming-shows-columns-two',
  templateUrl: './upcoming-shows-columns-two.component.html',
  styleUrls: ['./upcoming-shows-columns-two.component.css']
})
export class UpcomingShowsColumnsTwoComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = '2nd Watch Now';
  upcomingShowsColumnTwoLeftLink: string;
  upcomingShowsColumnTwoLeftUTM: string;
  upcomingShowsColumnTwoLeftAlt: string;
  upcomingShowsColumnTwoLeftImage: string;
  upcomingShowsColumnTwoLeftProgram: string;
  upcomingShowsColumnTwoLeftTitle: string;
  upcomingShowsColumnTwoLeftAirDate: string;
  upcomingShowsColumnTwoLeftDescription: string;
  upcomingShowsColumnTwoRightLink: string;
  upcomingShowsColumnTwoRightUTM: string;
  upcomingShowsColumnTwoRightAlt: string;
  upcomingShowsColumnTwoRightImage: string;
  upcomingShowsColumnTwoRightProgram: string;
  upcomingShowsColumnTwoRightTitle: string;
  upcomingShowsColumnTwoRightAirDate: string;
  upcomingShowsColumnTwoRightDescription: string;
  subscription: Subscription;

  utmSubscription(): void {
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if (message) {
        this.upcomingShowsColumnTwoLeftUTM = this.email.upcomingShowsColumnTwoLeftUTM;
        this.upcomingShowsColumnTwoRightUTM = this.email.upcomingShowsColumnTwoRightUTM;
      }
    });
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.upcomingShowsColumnTwoDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  addVariables(): void {
    this.createTemplate.setVariables('upcomingShowsColumnTwoLeftAlt', this.upcomingShowsColumnTwoLeftProgram);
    this.createTemplate.setVariables('upcomingShowsColumnTwoLeftUTM', this.upcomingShowsColumnTwoLeftProgram);
    this.upcomingShowsColumnTwoLeftAlt = this.email.upcomingShowsColumnTwoLeftAlt;
    this.upcomingShowsColumnTwoLeftUTM = this.email.upcomingShowsColumnTwoLeftUTM;
    this.createTemplate.setVariables('upcomingShowsColumnTwoRightAlt', this.upcomingShowsColumnTwoRightProgram);
    this.createTemplate.setVariables('upcomingShowsColumnTwoRightUTM', this.upcomingShowsColumnTwoRightProgram);
    this.upcomingShowsColumnTwoRightAlt = this.email.upcomingShowsColumnTwoRightAlt;
    this.upcomingShowsColumnTwoRightUTM = this.email.upcomingShowsColumnTwoRightUTM;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.upcomingShowsColumnTwoDisplay;
    this.upcomingShowsColumnTwoLeftLink = this.email.upcomingShowsColumnTwoLeftLink;
    this.upcomingShowsColumnTwoLeftUTM = this.email.upcomingShowsColumnTwoLeftUTM;
    this.upcomingShowsColumnTwoLeftAlt = this.email.upcomingShowsColumnTwoLeftAlt;
    this.upcomingShowsColumnTwoLeftImage = this.email.upcomingShowsColumnTwoLeftImage;
    this.upcomingShowsColumnTwoLeftProgram = this.email.upcomingShowsColumnTwoLeftProgram;
    this.upcomingShowsColumnTwoLeftTitle = this.email.upcomingShowsColumnTwoLeftTitle;
    this.upcomingShowsColumnTwoLeftAirDate = this.email.upcomingShowsColumnTwoLeftAirDate;
    this.upcomingShowsColumnTwoLeftDescription = this.email.upcomingShowsColumnTwoLeftDescription;
    this.upcomingShowsColumnTwoRightLink = this.email.upcomingShowsColumnTwoRightLink;
    this.upcomingShowsColumnTwoRightUTM = this.email.upcomingShowsColumnTwoRightUTM;
    this.upcomingShowsColumnTwoRightAlt = this.email.upcomingShowsColumnTwoRightAlt;
    this.upcomingShowsColumnTwoRightImage = this.email.upcomingShowsColumnTwoRightImage;
    this.upcomingShowsColumnTwoRightProgram = this.email.upcomingShowsColumnTwoRightProgram;
    this.upcomingShowsColumnTwoRightTitle = this.email.upcomingShowsColumnTwoRightTitle;
    this.upcomingShowsColumnTwoRightAirDate = this.email.upcomingShowsColumnTwoRightAirDate;
    this.upcomingShowsColumnTwoRightDescription = this.email.upcomingShowsColumnTwoRightDescription;
    this.utmSubscription();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
