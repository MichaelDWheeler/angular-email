import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingShowsColumnsTwoComponent } from './upcoming-shows-columns-two.component';

describe('UpcomingShowsColumnsTwoComponent', () => {
  let component: UpcomingShowsColumnsTwoComponent;
  let fixture: ComponentFixture<UpcomingShowsColumnsTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpcomingShowsColumnsTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingShowsColumnsTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
