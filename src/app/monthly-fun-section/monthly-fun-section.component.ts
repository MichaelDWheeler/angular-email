import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-monthly-fun-section',
  templateUrl: './monthly-fun-section.component.html',
  styleUrls: ['./monthly-fun-section.component.css']
})
export class MonthlyFunSectionComponent implements OnInit {
  email: any = {};
  @Input() monthUTM: string;
  @Input() monthAlt: string;
  monthLink: string;
  monthImage: string;
  monthDescription: string;
  monthButton: string;

  constructor(
    public createTemplate: CreateTemplateService, 
    public select: SelectTextService,
    public sanitizer: DomSanitizer
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.monthUTM = this.email.monthUTM;
    this.monthAlt = this.email.monthAlt;
    this.monthLink = this.email.monthLink;
    this.monthImage = this.email.monthImage;
    this.monthDescription = this.email.monthDescription;
    this.monthButton = this.email.monthButton;
  }

}
