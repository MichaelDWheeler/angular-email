import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-restore-button',
  templateUrl: './restore-button.component.html',
  styleUrls: ['./restore-button.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class RestoreButtonComponent implements OnInit {
  @Input() component: string;
  @Input() showComponent: string;
  color: string;
  
  checkComponent(): void {
    if (this.component === 'divider') {
      this.color = 'divider';
    } else {
      this.color = 'restore-btn';
    }
  }
  
  constructor() { }

  ngOnInit() {
    this.checkComponent();
  }

}
