import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-upcoming-shows-columns-three',
  templateUrl: './upcoming-shows-columns-three.component.html',
  styleUrls: ['./upcoming-shows-columns-three.component.css']
})
export class UpcomingShowsColumnsThreeComponent implements OnInit, OnDestroy {
  email: any;
  showComponent: boolean;
  component = '3rd Watch Now';
  upcomingShowsColumnThreeLeftLink: string;
  upcomingShowsColumnThreeLeftUTM: string;
  upcomingShowsColumnThreeLeftAlt: string;
  upcomingShowsColumnThreeLeftImage: string;
  upcomingShowsColumnThreeLeftProgram: string;
  upcomingShowsColumnThreeLeftTitle: string;
  upcomingShowsColumnThreeLeftAirDate: string;
  upcomingShowsColumnThreeLeftDescription: string;
  upcomingShowsColumnThreeRightLink: string;
  upcomingShowsColumnThreeRightUTM: string;
  upcomingShowsColumnThreeRightAlt: string;
  upcomingShowsColumnThreeRightImage: string;
  upcomingShowsColumnThreeRightProgram: string;
  upcomingShowsColumnThreeRightTitle: string;
  upcomingShowsColumnThreeRightAirDate: string;
  upcomingShowsColumnThreeRightDescription: string;
  subscription: Subscription;

  toggleControls() {
    this.showComponent = !this.showComponent;
    this.email.upcomingShowsColumnThreeDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  addVariables(): void {
    this.createTemplate.setVariables('upcomingShowsColumnThreeLeftAlt', this.upcomingShowsColumnThreeLeftProgram);
    this.createTemplate.setVariables('upcomingShowsColumnThreeLeftUTM', this.upcomingShowsColumnThreeLeftProgram);
    this.upcomingShowsColumnThreeLeftAlt = this.email.upcomingShowsColumnThreeLeftAlt;
    this.upcomingShowsColumnThreeLeftUTM = this.email.upcomingShowsColumnThreeLeftUTM;
    this.createTemplate.setVariables('upcomingShowsColumnThreeRightAlt', this.upcomingShowsColumnThreeRightProgram);
    this.createTemplate.setVariables('upcomingShowsColumnThreeRightUTM', this.upcomingShowsColumnThreeRightProgram);
    this.upcomingShowsColumnThreeRightAlt = this.email.upcomingShowsColumnThreeRightAlt;
    this.upcomingShowsColumnThreeRightUTM = this.email.upcomingShowsColumnThreeRightUTM;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void{
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.upcomingShowsColumnThreeLeftUTM = this.email.upcomingShowsColumnThreeLeftUTM;
        this.upcomingShowsColumnThreeRightUTM = this.email.upcomingShowsColumnThreeRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.upcomingShowsColumnThreeDisplay;
    this.upcomingShowsColumnThreeLeftLink = this.email.upcomingShowsColumnThreeLeftLink;
    this.upcomingShowsColumnThreeLeftUTM = this.email.upcomingShowsColumnThreeLeftUTM;
    this.upcomingShowsColumnThreeLeftAlt = this.email.upcomingShowsColumnThreeLeftAlt;
    this.upcomingShowsColumnThreeLeftImage = this.email.upcomingShowsColumnThreeLeftImage;
    this.upcomingShowsColumnThreeLeftProgram = this.email.upcomingShowsColumnThreeLeftProgram;
    this.upcomingShowsColumnThreeLeftTitle = this.email.upcomingShowsColumnThreeLeftTitle;
    this.upcomingShowsColumnThreeLeftAirDate = this.email.upcomingShowsColumnThreeLeftAirDate;
    this.upcomingShowsColumnThreeLeftDescription = this.email.upcomingShowsColumnThreeLeftDescription;
    this.upcomingShowsColumnThreeRightLink = this.email.upcomingShowsColumnThreeRightLink;
    this.upcomingShowsColumnThreeRightUTM  = this.email.upcomingShowsColumnThreeRightUTM;
    this.upcomingShowsColumnThreeRightAlt = this.email.upcomingShowsColumnThreeRightAlt;
    this.upcomingShowsColumnThreeRightImage = this.email.upcomingShowsColumnThreeRightImage;
    this.upcomingShowsColumnThreeRightProgram = this.email.upcomingShowsColumnThreeRightProgram;
    this.upcomingShowsColumnThreeRightTitle = this.email.upcomingShowsColumnThreeRightTitle;
    this.upcomingShowsColumnThreeRightAirDate = this.email.upcomingShowsColumnThreeRightAirDate;
    this.upcomingShowsColumnThreeRightDescription = this.email.upcomingShowsColumnThreeRightDescription;
    this.utmSubscription();
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
