import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingShowsColumnsThreeComponent } from './upcoming-shows-columns-three.component';

describe('UpcomingShowsColumnsThreeComponent', () => {
  let component: UpcomingShowsColumnsThreeComponent;
  let fixture: ComponentFixture<UpcomingShowsColumnsThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpcomingShowsColumnsThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingShowsColumnsThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
