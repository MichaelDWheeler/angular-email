import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { TextDescriptionComponent } from './text-description.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { FormsModule } from '@angular/forms';

describe('TextDescriptionComponent', () => {
  let component: TextDescriptionComponent;
  let fixture: ComponentFixture<TextDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ 
        TextDescriptionComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });
});
