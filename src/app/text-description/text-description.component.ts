import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-text-description',
  templateUrl: './text-description.component.html',
  styleUrls: ['./text-description.component.css'],
})


export class TextDescriptionComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'greeting';
  message = '<convio:choose xmlns="http://www.convio.com"><convio:when title="First name is not empty" test="nempty"><convio:op><convio:session name="1" param="first_name"></convio:session></convio:op><convio:then><convio:session name="1" param="first_name" title="1:first_name">First Name</convio:session></convio:then></convio:when><convio:otherwise title="First name is empty">Member</convio:otherwise></convio:choose>';
  convioMessage = this.sanitizer.bypassSecurityTrustHtml(this.message);
  textDescription = 'textDescriptionAddInterest';
  textDescriptionLink: string;
  textDescriptionImageAlt: string;
  textDescriptionImageSource: string;
  textDescriptionSalutation: string;
  textDescriptionContent: string;
  textDescriptionAddInterest: string;
  textDescriptionText: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.textDescriptionDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.textDescriptionDisplay;
    this.textDescriptionLink = this.email.textDescriptionLink;
    this.textDescriptionImageAlt = this.email.textDescriptionImageAlt;
    this.textDescriptionImageSource = this.email.textDescriptionImageSource;
    this.textDescriptionSalutation = this.email.textDescriptionSalutation;
    this.textDescriptionContent = this.email.textDescriptionContent;
    this.textDescriptionText = this.email.textDescriptionText;
  }

}
