import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { CreateTemplateService } from '../create-template.service';
import { AddInterestService } from '../add-interest.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-coming-soon-bottom',
  templateUrl: './coming-soon-bottom.component.html',
  styleUrls: ['./coming-soon-bottom.component.css']
})
export class ComingSoonBottomComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = 'two programs';
  addInterest: any;
  bottomLeft = 'comingSoonBottomLeftAddInterest';
  bottomRight = 'comingSoonBottomRightAddInterest';
  comingSoonBottomLeftLink: string;
  comingSoonBottomLeftUTM: string;
  comingSoonBottomLeftAlt: string;
  comingSoonBottomLeftImage: string;
  comingSoonBottomLeftProgram: string;
  comingSoonBottomLeftTitle: string;
  comingSoonBottomLeftAirDate: string;
  comingSoonBottomLeftButton: string;
  comingSoonBottomLeftDescription: string;
  comingSoonBottomRightProgram: string;
  comingSoonBottomRightAlt: string;
  comingSoonBottomRightUTM: string;
  comingSoonBottomRightTitle: string;
  comingSoonBottomRightAirDate: string;
  comingSoonBottomRightButton: string;
  comingSoonBottomRightDescription: string;
  comingSoonBottomRightImage: string;
  comingSoonBottomRightLink: string;
  subscription: Subscription;


  addVariables(): void {
    this.createTemplate.setVariables('comingSoonBottomLeftAlt', this.comingSoonBottomLeftProgram);
    this.createTemplate.setVariables('comingSoonBottomLeftUTM', this.comingSoonBottomLeftProgram);
    this.createTemplate.changeVariables('comingSoonBottomLeftProgram', this.comingSoonBottomLeftProgram);
    this.createTemplate.setVariables('comingSoonBottomRightAlt', this.comingSoonBottomRightProgram);
    this.createTemplate.setVariables('comingSoonBottomRightUTM', this.comingSoonBottomRightProgram);
    this.createTemplate.changeVariables('comingSoonBottomRightProgram', this.comingSoonBottomRightProgram);
    this.comingSoonBottomLeftAlt = this.email.comingSoonBottomLeftAlt;
    this.comingSoonBottomLeftUTM = this.email.comingSoonBottomLeftUTM;
    this.comingSoonBottomRightAlt = this.email.comingSoonBottomRightAlt;
    this.comingSoonBottomRightUTM = this.email.comingSoonBottomRightUTM;
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.comingSoonBottomDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void {
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.comingSoonBottomLeftUTM = this.email.comingSoonBottomLeftUTM;
        this.comingSoonBottomRightUTM = this.email.comingSoonBottomRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public interest: AddInterestService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.addInterest = this.interest.getAddInterests();
    this.showComponent = this.email.comingSoonBottomDisplay;
    this.comingSoonBottomLeftLink = this.email.comingSoonBottomLeftLink;
    this.comingSoonBottomLeftUTM = this.email.comingSoonBottomLeftUTM;
    this.comingSoonBottomLeftAlt = this.email.comingSoonBottomLeftAlt;
    this.comingSoonBottomLeftImage = this.email.comingSoonBottomLeftImage;
    this.comingSoonBottomLeftProgram = this.email.comingSoonBottomLeftProgram;
    this.comingSoonBottomLeftTitle = this.email.comingSoonBottomLeftTitle;
    this.comingSoonBottomLeftAirDate = this.email.comingSoonBottomLeftAirDate;
    this.comingSoonBottomLeftButton = this.email.comingSoonBottomLeftButton;
    this.comingSoonBottomLeftDescription = this.email.comingSoonBottomLeftDescription;
    this.comingSoonBottomRightProgram = this.email.comingSoonBottomRightProgram;
    this.comingSoonBottomRightAlt = this.email.comingSoonBottomRightAlt;
    this.comingSoonBottomRightUTM = this.email.comingSoonBottomRightUTM;
    this.comingSoonBottomRightTitle = this.email.comingSoonBottomRightTitle;
    this.comingSoonBottomRightAirDate = this.email.comingSoonBottomRightAirDate;
    this.comingSoonBottomRightButton = this.email.comingSoonBottomRightButton;
    this.comingSoonBottomRightDescription = this.email.comingSoonBottomRightDescription;
    this.comingSoonBottomRightImage = this.email.comingSoonBottomRightImage;
    this.comingSoonBottomRightLink = this.email.comingSoonBottomRightLink;
    this.utmSubscription();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
