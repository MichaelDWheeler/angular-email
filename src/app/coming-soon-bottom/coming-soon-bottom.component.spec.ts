import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ComingSoonBottomComponent } from './coming-soon-bottom.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestComponent } from '../add-interest/add-interest.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('ComingSoonBottomComponent', () => {
  let component: ComingSoonBottomComponent;
  let fixture: ComponentFixture<ComingSoonBottomComponent>;
  const templateVariables = TEMPLATEVARIABLES;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ComingSoonBottomComponent,
        RestoreButtonComponent,
        RemoveButtonComponent,
        AddInterestComponent
      ],
      imports: [FormsModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingSoonBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "showComponent" to change between true and false', () => {
    component.toggleControls();
    expect(component.showComponent).toBeFalsy();
    component.toggleControls();
    expect(component.showComponent).toBeTruthy();
  });

  it('should populate interest object with 22 interest properties and values', () => {
    component.addInterest = component.interest.getAddInterests();
    expect(component.addInterest.length).toEqual(22);
  });

  it('should return the email object with default properties', () => {
    component.email = component.createTemplate.getVariables();
    expect(component.email).toEqual(templateVariables);
  });
});
