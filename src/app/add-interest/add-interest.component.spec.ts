import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AddInterestComponent } from './add-interest.component';

describe('AddInterestComponent', () => {
  let component: AddInterestComponent;
  let fixture: ComponentFixture<AddInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddInterestComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return addInterest object', () => {
    expect(component.addInterest).toContain({ order: 21, id: 0, category: 'No Interest' });
  });

  it('should not return undefined', () => {
    expect(component.email).toBeDefined();
  });

  it('should return {order: 1, id: 1421, category: \'--Antiquarians\' }', () => {
    expect(component.addInterest[2]).toEqual({ order: 1, id: 1421, category: '--Antiquarians'  });
  });

});

