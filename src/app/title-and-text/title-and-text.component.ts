import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-title-and-text',
  templateUrl: './title-and-text.component.html',
  styleUrls: ['./title-and-text.component.css']
})
export class TitleAndTextComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = "Title & Text";
  titleTextTitle: string;
  titleTextText: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.titleTextDisplay = this.showComponent;
  }
  
  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public sanitizer: DomSanitizer
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.titleTextDisplay;
    this.titleTextTitle = this.email.titleTextTitle;
    this.titleTextText = this.email.titleTextText;
  }

}
