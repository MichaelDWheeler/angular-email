import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamingNowTwoComponent } from './streaming-now-two.component';

describe('StreamingNowTwoComponent', () => {
  let component: StreamingNowTwoComponent;
  let fixture: ComponentFixture<StreamingNowTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreamingNowTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamingNowTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
