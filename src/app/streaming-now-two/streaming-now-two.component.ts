import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-streaming-now-two',
  templateUrl: './streaming-now-two.component.html',
  styleUrls: ['./streaming-now-two.component.css']
})
export class StreamingNowTwoComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = '2nd Streamin Now';
  streamingNowTwoDisplay: boolean;
  streamingNowTwoLeftLink: string;
  streamingNowTwoLeftUTM: string;
  streamingNowTwoLeftAlt: string;
  streamingNowTwoLeftImage: string;
  streamingNowTwoLeftProgram: string;
  streamingNowTwoLeftTitle: string;
  streamingNowTwoLeftDescription: string;
  streamingNowTwoRightLink: string;
  streamingNowTwoRightUTM: string;
  streamingNowTwoRightAlt: string;
  streamingNowTwoRightImage: string;
  streamingNowTwoRightProgram: string;
  streamingNowTwoRightTitle: string;
  streamingNowTwoRightDescription: string;
  subscription: Subscription;

  addVariables(): void {
    this.createTemplate.setVariables('streamingNowTwoLeftAlt', this.streamingNowTwoLeftProgram);
    this.createTemplate.setVariables('streamingNowTwoLeftUTM', this.streamingNowTwoLeftProgram);
    this.streamingNowTwoLeftAlt = this.email.streamingNowTwoLeftAlt;
    this.streamingNowTwoLeftUTM = this.email.streamingNowTwoLeftUTM;
    this.createTemplate.setVariables('streamingNowTwoRightAlt', this.streamingNowTwoRightProgram);
    this.createTemplate.setVariables('streamingNowTwoRightUTM', this.streamingNowTwoRightProgram);
    this.streamingNowTwoRightAlt = this.email.streamingNowTwoRightAlt;
    this.streamingNowTwoRightUTM = this.email.streamingNowTwoRightUTM;
    this.createTemplate.setStorage();
  }

  selectAllText($event: any): void {
    this.select.selectAllText($event);
  }

  toggleControls(): void {
    this.streamingNowTwoDisplay = !this.streamingNowTwoDisplay;
    this.email.streamingNowTwoDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void {
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if (message) {
        this.streamingNowTwoLeftUTM = this.email.streamingNowTwoLeftUTM;
        this.streamingNowTwoRightUTM = this.email.streamingNowTwoRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.streamingNowTwoDisplay;
    this.streamingNowTwoLeftLink = this.email.streamingNowTwoLeftLink;
    this.streamingNowTwoLeftUTM = this.email.streamingNowTwoLeftUTM;
    this.streamingNowTwoLeftAlt = this.email.streamingNowTwoLeftAlt;
    this.streamingNowTwoLeftImage = this.email.streamingNowTwoLeftImage;
    this.streamingNowTwoLeftProgram = this.email.streamingNowTwoLeftProgram;
    this.streamingNowTwoLeftTitle = this.email.streamingNowTwoLeftTitle;
    this.streamingNowTwoLeftDescription = this.email.streamingNowTwoLeftDescription;
    this.streamingNowTwoRightLink = this.email.streamingNowTwoRightLink;
    this.streamingNowTwoRightUTM = this.email.streamingNowTwoRightUTM;
    this.streamingNowTwoRightAlt = this.email.streamingNowTwoRightAlt;
    this.streamingNowTwoRightImage = this.email.streamingNowTwoRightImage;
    this.streamingNowTwoRightProgram = this.email.streamingNowTwoRightProgram;
    this.streamingNowTwoRightTitle = this.email.streamingNowTwoRightTitle;
    this.streamingNowTwoRightDescription = this.email.streamingNowTwoRightDescription;
    this.utmSubscription();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}








