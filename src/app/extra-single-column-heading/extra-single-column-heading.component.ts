import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-extra-single-column-heading',
  templateUrl: './extra-single-column-heading.component.html',
  styleUrls: ['./extra-single-column-heading.component.css']
})
export class ExtraSingleColumnHeadingComponent implements OnInit {
  email: any = {};
  component = 'Extra Section';
  extraSingleColumnHeading: string;
  showComponent: boolean;
  extraSingleColumnAlt: string;
  extraSingleColumnUTM: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.extraSingleColumnHeadingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  addAll(): void {
    this.createTemplate.changeVariables('extraSingleColumnHeading', this.extraSingleColumnHeading);
    this.createTemplate.setVariables('extraSingleColumnAlt', this.extraSingleColumnHeading);
    this.createTemplate.setVariables('extraSingleColumnUTM', this.extraSingleColumnHeading);
    this.extraSingleColumnAlt = this.email.extraSingleColumnAlt;
    this.extraSingleColumnUTM = this.email.extraSingleColumnUTM;
    this.createTemplate.setStorage();
  }

  setVariables(): void {
    if (this.email.extraSingleColumnAlt === '') {
      this.createTemplate.setVariables('extraSingleColumnAlt', this.email.extraSingleColumnHeading);
    }
    if (this.email.extraSingleColumnUTM === '') {
      this.createTemplate.setVariables('extraSingleColumnUTM', this.email.extraSingleColumnHeading);
    }
  }

  constructor(
    public createTemplate: CreateTemplateService, 
    public select: SelectTextService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.extraSingleColumnHeading = this.email.extraSingleColumnHeading;
    this.setVariables();
    this.showComponent = this.email.extraSingleColumnHeadingDisplay;
  }

}
