import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ExtraSingleColumnHeadingComponent } from './extra-single-column-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('ExtraSingleColumnHeadingComponent', () => {
  let component: ExtraSingleColumnHeadingComponent;
  let fixture: ComponentFixture<ExtraSingleColumnHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ExtraSingleColumnHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraSingleColumnHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should become the opposite value of the current value', (() => {
    component.showComponent = true;
    component.toggleControls();
    expect(component.showComponent).toBeFalsy;
    component.toggleControls();
    expect(component.showComponent).toBeTruthy;
  }));

  it('should change the value of extraSingleColumnAlt to the value of extraSingleColumnHeading', (() => {
    component.email.extraSingleColumnHeading = 'this is a test';
    component.addAll();
    expect(component.email.extraSingleColumnAlt).toEqual('this is a test');
  }));

  it('should change the value of extraSingleColumnUTM to "this%20is%20a%20test"', (() => {
    component.email.extraSingleColumnHeading = 'this is a test';
    component.addAll();
    expect(component.email.extraSingleColumnUTM).toEqual('this%20is%20a%20test');
  }));

  it('should change the value of extraSingleColumnAlt to the value of extraSingleColumnHeading', (()=>{
    component.email.extraSingleColumnAlt = '';
    component.setVariables();
    expect(component.email.extraSingleColumnAlt).toEqual(component.email.extraSingleColumnHeading);
    component.email.extraSingleColumnHeading = 'This is a test';
    component.email.extraSingleColumnAlt = '';
    component.setVariables();
    expect(component.email.extraSingleColumnAlt).toEqual('This is a test');
  }));

  it('should change the value of extraSingleColumnUTM to a url parameter with no spaces', (()=>{
    component.email.extraSingleColumnUTM = '';
    component.email.extraSingleColumnHeading = 'This is a parameter test';
    component.setVariables();
    expect(component.email.extraSingleColumnUTM).toEqual('This%20is%20a%20parameter%20test');
  }));
});
