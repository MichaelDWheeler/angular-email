import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { UpcomingShowsColumnsTopComponent } from './upcoming-shows-columns-top.component';
import { FormsModule } from '@angular/forms';

describe('UpcomingShowsColumnsTopComponent', () => {
  let component: UpcomingShowsColumnsTopComponent;
  let fixture: ComponentFixture<UpcomingShowsColumnsTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpcomingShowsColumnsTopComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingShowsColumnsTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
