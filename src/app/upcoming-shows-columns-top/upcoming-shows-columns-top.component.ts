import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-upcoming-shows-columns-top',
  templateUrl: './upcoming-shows-columns-top.component.html',
  styleUrls: ['./upcoming-shows-columns-top.component.css']
})
export class UpcomingShowsColumnsTopComponent implements OnInit, OnDestroy {
  email: any = {};
  upcomingShowsColumnTopLeftAirDate: string;
  upcomingShowsColumnTopLeftAlt: string;
  upcomingShowsColumnTopLeftDescription: string;
  upcomingShowsColumnTopLeftImage: string;
  upcomingShowsColumnTopLeftLink: string;
  upcomingShowsColumnTopLeftProgram: string;
  upcomingShowsColumnTopLeftTitle: string;
  upcomingShowsColumnTopLeftUTM: string;
  upcomingShowsColumnTopRightAirDate: string;
  upcomingShowsColumnTopRightLink: string;
  upcomingShowsColumnTopRightUTM: string;
  upcomingShowsColumnTopRightProgram: string;
  upcomingShowsColumnTopRightTitle: string;
  upcomingShowsColumnTopRightDescription: string;
  upcomingShowsColumnTopRightImage: string;
  upcomingShowsColumnTopRightAlt: string;
  subscription: Subscription;

  addVariables(): void {
    this.createTemplate.setVariables('upcomingShowsColumnTopLeftAlt', this.upcomingShowsColumnTopLeftProgram);
    this.createTemplate.setVariables('upcomingShowsColumnTopLeftUTM', this.upcomingShowsColumnTopLeftProgram);
    this.upcomingShowsColumnTopLeftAlt = this.email.upcomingShowsColumnTopLeftAlt;
    this.upcomingShowsColumnTopLeftUTM = this.email.upcomingShowsColumnTopLeftUTM;
    this.createTemplate.setVariables('upcomingShowsColumnTopRightAlt', this.upcomingShowsColumnTopRightProgram);
    this.createTemplate.setVariables('upcomingShowsColumnTopRightUTM', this.upcomingShowsColumnTopRightProgram);
    this.upcomingShowsColumnTopRightAlt = this.email.upcomingShowsColumnTopRightAlt;
    this.upcomingShowsColumnTopRightUTM = this.email.upcomingShowsColumnTopRightUTM;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void{
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.upcomingShowsColumnTopLeftUTM = this.email.upcomingShowsColumnTopLeftUTM;
        this.upcomingShowsColumnTopRightUTM = this.email.upcomingShowsColumnTopRightUTM;
      }
    });
  }
  
  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.upcomingShowsColumnTopLeftAirDate = this.email.upcomingShowsColumnTopLeftAirDate;
    this.upcomingShowsColumnTopLeftAlt = this.email.upcomingShowsColumnTopLeftAlt;
    this.upcomingShowsColumnTopLeftDescription = this.email.upcomingShowsColumnTopLeftDescription;
    this.upcomingShowsColumnTopLeftImage = this.email.upcomingShowsColumnTopLeftImage;
    this.upcomingShowsColumnTopLeftLink = this.email.upcomingShowsColumnTopLeftLink;
    this.upcomingShowsColumnTopLeftProgram = this.email.upcomingShowsColumnTopLeftProgram;
    this.upcomingShowsColumnTopLeftTitle = this.email.upcomingShowsColumnTopLeftTitle;
    this.upcomingShowsColumnTopLeftUTM = this.email.upcomingShowsColumnTopLeftUTM;
    this.upcomingShowsColumnTopRightLink = this.email.upcomingShowsColumnTopRightLink;
    this.upcomingShowsColumnTopRightUTM = this.email.upcomingShowsColumnTopRightUTM;
    this.upcomingShowsColumnTopRightProgram = this.email.upcomingShowsColumnTopRightProgram;
    this.upcomingShowsColumnTopRightTitle = this.email.upcomingShowsColumnTopRightTitle;
    this.upcomingShowsColumnTopRightDescription = this.email.upcomingShowsColumnTopRightDescription;
    this.upcomingShowsColumnTopRightImage = this.email.upcomingShowsColumnTopRightImage;
    this.upcomingShowsColumnTopRightAlt = this.email.upcomingShowsColumnTopRightAlt;
    this.upcomingShowsColumnTopRightAirDate = this.email.upcomingShowsColumnTopRightAirDate;
    this.utmSubscription();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
