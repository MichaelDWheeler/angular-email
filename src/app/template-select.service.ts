import { Injectable } from '@angular/core';
import { TemplateService } from './template-service';
import { MembershipUtmSource } from './membership-utm-source';

@Injectable({
  providedIn: 'root'
})
export class TemplateSelectService {

  templateOptions: TemplateService[] = [
    { id: 0, name: 'Dedicated', path: '/dedicated' },
    { id: 1, name: 'Membership', path: '/membership' },
    { id: 2, name: 'Passport', path: '/passport' },
    { id: 3, name: 'Program Highlights', path: '/program-highlights' }
  ];

  membershipUTMSource: MembershipUtmSource[] = [
    { id: 0, name: 'Membership SG', source: `Sustainer%20Newsletter%20${this.yearWithMonth()}` },
    { id: 1, name: 'Membership OP', source: `One%20Time%20Newsletter%20${this.yearWithMonth()}` },
    { id: 2, name: 'Producers Club', source: `Producers%20Club%20${this.yearWithMonth()}` }
  ];

  yearWithMonth(): string { 
    const d = new Date();
    const year = d.getFullYear().toString();
    let month = d.getMonth() + 1;
    let monthString = month.toString();
    if(monthString.length === 1){
      monthString = `0${month}`;
    }
    const yearAndMonth = `${year}-${monthString}`;
    return yearAndMonth;
  }
  
  getMembershipOptions(): any{
    return this.membershipUTMSource;
  }

  getOptions(): any {
    return this.templateOptions;
  }


  constructor() { }
}
