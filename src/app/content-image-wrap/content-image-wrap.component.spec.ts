import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ContentImageWrapComponent } from './content-image-wrap.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('ContentImageWrapComponent', () => {
  let component: ContentImageWrapComponent;
  let fixture: ComponentFixture<ContentImageWrapComponent>;
  const templateVariables = Object.assign({}, TEMPLATEVARIABLES);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ContentImageWrapComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentImageWrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.email = templateVariables;
  });

  afterEach(() => {
    fixture.destroy();
    component.email.contentImageWrapSrc = 'http://via.placeholder.com/600x338';
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "showComponent" to change between true and false', () => {
    component.toggleControls();
    expect(component.showComponent).toBeFalsy();
    component.toggleControls();
    expect(component.showComponent).toBeTruthy();
  });

  it('the email object should equal the default properties and values in TEMPLATEVARIABLES', () => {
    component.email = component.createTemplate.getVariables();
    expect(component.email).toEqual(TEMPLATEVARIABLES);
  });

  it('html string should include "http://via.placeholder.com/600x338"', () => {
    component.html = '<table align="right" border="0" cellpadding="0" cellspacing="0" class="full" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="300"><tr><td><img class="full" align="right" src="' + component.email.contentImageWrapSrc + '" width="280"></td></tr></table>';
    expect(component.html).toContain('http://via.placeholder.com/600x338');
  });

  it('should return a string with "www.example.com" in it', () => {
    component.email.contentImageWrapSrc = 'www.example.com';
    component.changeImage();
    expect(component.html).toContain('www.example.com');
  });

});
