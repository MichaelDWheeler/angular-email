import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-content-image-wrap',
  templateUrl: './content-image-wrap.component.html',
  styleUrls: ['./content-image-wrap.component.css'],
})
export class ContentImageWrapComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'image wrap';
  html: string;
  contentImageWrap: string;
  contentImageWrapSrc: string;

  changeImage(): void {
    this.html = '<table align="right" border="0" cellpadding="0" cellspacing="0" class="full" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="300"><tr><td><img class="full" align="right" src="' + this.email.contentImageWrapSrc + '" width="280"></td></tr></table>';
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.contentImageWrapDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.html = '<table align="right" border="0" cellpadding="0" cellspacing="0" class="full" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="300"><tr><td><img class="full" align="right" src="' + this.email.contentImageWrapSrc + '" width="280"></td></tr></table>';
    this.showComponent = this.email.contentImageWrapDisplay;
    this.contentImageWrap = this.email.contentImageWrap;
    this.contentImageWrapSrc = this.email.contentImageWrapSrc;
  }

}
