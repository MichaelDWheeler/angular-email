import { Injectable } from '@angular/core';
import { CreateTemplateService } from './create-template.service';

@Injectable({
  providedIn: 'root'
})
export class CheckEmailFieldsService {
  i: number = 1;
  message = 'There are warnings/errors on this email template:\n';
  emailVariables: any = {} = this._createTemplate.getVariables();
  enjoyLinks: any = [] = this._createTemplate.getLinks('also enjoy');

  checkAdProviders(): void {
    if (this.emailVariables.adProviders.length === 0) {
      this.message += this.i + '. You have no ads in the Providing Support For PBS Section\n';
      this.i++;
    }
  }

  checkAlsoEnjoy(): void {
    if (this.emailVariables.alsoEnjoyHeading === '') {
      this.message += this.i + '. The "You May Also Enjoy" heading is blank\n';
      this.i++;
    }
    if (this.enjoyLinks.length === 0) {
      this.message += this.i + '. There are no links listed in the "Enjoy Links" section\n';
      this.i++;
    }
  }

  checkBottomSingleColumn(): void {
    if (this.emailVariables.bottomSingleColumnAdditionalInfo === '' && this.emailVariables.bottomSingleColumnImage !== 'https://pbs-socal-email.s3.amazonaws.com/newsletters/highlights/2018/10/13/pbs-app-variant-1-min-min.jpg') {
      this.message += this.i + '. WARNING: The "More Info" field is blank in the section right below the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnHeading === '' && this.emailVariables.bottomSingleHeadingDisplay) {
      this.message += this.i + '. You need to add the heading in the section right below the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnImage === '') {
      this.message += this.i + '. You need to add the image source in the section right below the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the section right below the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnLink === '') {
      this.message += this.i + '. You need to add the link in the section right below the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnButton === '' && this.emailVariables.bottomSingleColumnImage !== 'https://pbs-socal-email.s3.amazonaws.com/newsletters/highlights/2018/10/13/pbs-app-variant-1-min-min.jpg') {
      this.message += this.i + '. You need to add the button text in the section right below the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnHeading !== 'PBS SOCAL READS' && this.emailVariables.bottomSingleColumnButton === 'PBS SoCal Reads') {
      this.message += this.i + '. The bottom heading is "' + this.emailVariables.bottomSingleColumnHeading + '" but the button still says "PBS SoCal Reads"\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the bottom section\n';
      this.i++;
    }
    if (this.emailVariables.bottomSingleColumnDescription === '' && this.emailVariables.bottomSingleColumnImage !== 'https://pbs-socal-email.s3.amazonaws.com/newsletters/highlights/2018/10/13/pbs-app-variant-1-min-min.jpg') {
      this.message += this.i + '. You need to add the description in the bottom section\n';
      this.i++;
    }
  }

  checkClosing(): void{
    if (this.emailVariables.closingImageSrc === 'https://via.placeholder.com/600x338') {
      this.message += this.i + '. You need to change the image source in the closing section\n';
      this.i++;
    }
    if (this.emailVariables.closingText === 'blah blah and blah but then blah and I said you saw the blah on the blah.') {
      this.message += this.i + '. You need to change the description in the closing section\n';
      this.i++;
    }
    if (this.emailVariables.closingValediction === '') {
      this.message += this.i + '. WARNING: There is no valediction in the closing section\n';
      this.i++;
    }
    if (this.emailVariables.closingName === '') {
      this.message += this.i + '. WARNING: There is no name in the closing section\n';
      this.i++;
    }
    if (this.emailVariables.closingTitle === '') {
      this.message += this.i + '. WARNING: There is no title in the closing section\n';
      this.i++;
    }
  }

  checkExtraSingleColumn(): void {
    if (this.emailVariables.extraSingleColumnHeading === '') {
      this.message += this.i + '. WARNING: There is no heading in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnTitle === '') {
      this.message += this.i + '. WARNING: There is no title in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnSubTitle === '') {
      this.message += this.i + '. WARNING: There is no subtitle in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnAdditionalInfo === '') {
      this.message += this.i + '. WARNING: There is no text in the extra single column "More Info" section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnImage === '' || this.emailVariables.extraSingleColumnImage === 'https://via.placeholder.com/1200x675') {
      this.message += this.i + '. You need to add the image source in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnAlt === '' && this.emailVariables.extraSingleColumnImage !== '') {
      this.message += this.i + '. You need to add the alt tag in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnLink === '') {
      this.message += this.i + '. You need to add the link in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnButton === '') {
      this.message += this.i + '. WARNING: There is no button in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the extra single column section\n';
      this.i++;
    }
    if (this.emailVariables.extraSingleColumnDescription === '') {
      this.message += this.i + '. WARNING: There is no description in the extra single column section\n';
      this.i++;
    }
  }

  checkGlobalUTMCodes(): void {
    if (this.emailVariables.luminateSubjectLine === '' || this.emailVariables.luminateSubjectLine === 'Luminate Subject Line') {
      this.message += this.i + '. You need to change the Luminate Subject Line\n';
      this.i++;
    }
    if (this.emailVariables.superSubjectLink === '' || this.emailVariables.superSubjectLink === 'Super Subject Line') {
      this.message += this.i + '. You need to change the Super Subject\n';
      this.i++;
    }
    if (this.emailVariables.utmSource === '') {
      this.message += this.i + '. You need to add the UTM Source in the first section\n';
      this.i++;
    }
    if (this.emailVariables.utmCampaign === '') {
      this.message += this.i + '. You need to add the UTM Campaign in the first section\n';
      this.i++;
    }
  }

  checkMonthSection(): void {
    if (this.emailVariables.monthTitle === '') {
      this.message += this.i + '. You need to add the title in the "Monthly Fun" section\n';
      this.i++;
    }
    if (this.emailVariables.monthImage === '' || this.emailVariables.monthImage === 'https://via.placeholder.com/1200x675') {
      this.message += this.i + '. You need to add the image source in the "Monthly Fun" section\n';
      this.i++;
    }
    if (this.emailVariables.monthAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the "Monthly Fun" section\n';
      this.i++;
    }
    if (this.emailVariables.monthLink === '') {
      this.message += this.i + '. You need to add the link in the "Monthly Fun" section\n';
      this.i++;
    }
    if (this.emailVariables.monthButton === '') {
      this.message += this.i + '. WARNING: There is no button in the "Monthly Fun" section\n';
      this.i++;
    }
    if (this.emailVariables.monthUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the "Monthly Fun" section\n';
      this.i++;
    }
    if (this.emailVariables.monthDescription === '') {
      this.message += this.i + '. WARNING: There is no description in the "Monthly Fun" section\n';
      this.i++;
    }
  }

  checkPassportAd(): void {
    if (this.emailVariables.passportAdImage === 'https://via.placeholder.com/600x338' || this.emailVariables.passportAdImage === '') {
      this.message += this.i + '. You need to change the image source in the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.passportAdAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.passportAdLink === '') {
      this.message += this.i + '. You need to add the link in the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.passportAdUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.passportAdButton === '') {
      this.message += this.i + '. You need to add the button text in the Passport section\n';
      this.i++;
    }
    if (this.emailVariables.passportAdDescription === '') {
      this.message += this.i + '. You need to add the description text in the Passport section\n';
      this.i++;
    }
  }

  checkPartnerSpotlight(): void {
    if (this.emailVariables.partnerSpotlightTitle === '') {
      this.message += this.i + '. WARNING: There is no title in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightLocation === '') {
      this.message += this.i + '. WARNING: There is no location in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightDate === '') {
      this.message += this.i + '. WARNING: There is no date in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightButton === '') {
      this.message += this.i + '. WARNING: There is no button in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightDescription === '') {
      this.message += this.i + '. WARNING: There is no description in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightImageSrc === 'https://via.placeholder.com/1200x675') {
      this.message += this.i + '. The image needs to be changed in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightImageAlt === '') {
      this.message += this.i + '. The alt tag needs to be included in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightLink === '') {
      this.message += this.i + '. There is no link for in the second section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightUTM === '') {
      this.message += this.i + '. The UTM needs to be changed in the second section\n';
      this.i++;
    }
  }

  checkPartnerSpotlightTwo(): void {
    if (this.emailVariables.partnerSpotlightTwoTitle === '') {
      this.message += this.i + '. WARNING: There is no title in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoLocation === '') {
      this.message += this.i + '. WARNING: There is no location in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoDate === '') {
      this.message += this.i + '. WARNING: There is no date in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoButton === '') {
      this.message += this.i + '. WARNING: There is no button in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoDescription === '') {
      this.message += this.i + '. WARNING: There is no description in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoImageSrc === 'https://via.placeholder.com/1200x675') {
      this.message += this.i + '. The image needs to be changed in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoImageAlt === '') {
      this.message += this.i + '. The alt tag needs to be included in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoLink === '') {
      this.message += this.i + '. There is no link for in the third section\n';
      this.i++;
    }
    if (this.emailVariables.partnerSpotlightTwoUTM === '') {
      this.message += this.i + '. The UTM needs to be changed in the third section\n';
      this.i++;
    }
  }

  checkSingleColumnSection(): void {
    if (this.emailVariables.singleColumnHeading === '') {
      this.message += this.i + '. WARNING: There is no heading in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnTitle === '') {
      this.message += this.i + '. WARNING: There is no title in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnButton === '') {
      this.message += this.i + '. WARNING: There is no button in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnLocation === '') {
      this.message += this.i + '. WARNING: There is no location in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnDate === '') {
      this.message += this.i + '. WARNING: There is no date in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnDescription === '') {
      this.message += this.i + '. WARNING: There is no description in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnImageSource === 'https://via.placeholder.com/1200x675') {
      this.message += this.i + '. You need to change the image in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnImageAlt === '') {
      this.message += this.i + '. You need to change the alt tag in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnLink === '') {
      this.message += this.i + '. You need to change the link in the single column section\n';
      this.i++;
    }
    if (this.emailVariables.singleColumnContent === '') {
      this.message += this.i + '. You need to change the UTM Content in the single column section\n';
      this.i++;
    }
  }

  checkStreamingNow(): void {
    if (this.emailVariables.streamingNow === '') {
      this.message += this.i + '. The "Streaming Now" heading is blank\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftProgram === '') {
      this.message += this.i + '. You need to add the program name in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftTitle === '') {
      this.message += this.i + '. You need to add the title in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftDescription === '') {
      this.message += this.i + '. You need to add the description in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftImage === 'https://via.placeholder.com/600x338' || this.emailVariables.streamingNowTopLeftImage === '') {
      this.message += this.i + '. You need to change the image source in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftLink === '') {
      this.message += this.i + '. You need to add the link in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the top left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightProgram === '') {
      this.message += this.i + '. You need to add the program name in the top right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightTitle === '') {
      this.message += this.i + '. You need to add the title in the top right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightDescription === '') {
      this.message += this.i + '. You need to add the description in the top right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightImage === 'https://via.placeholder.com/600x338' || this.emailVariables.streamingNowTopRightImage === '') {
      this.message += this.i + '. You need to change the image source in the top right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the top right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightLink === '') {
      this.message += this.i + '. You need to add the link in the top right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTopRightUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the top right streaming now section\n';
      this.i++;
    }
  }

  checkStreamingNowBottomDisplay(): void {
    if (this.emailVariables.streamingNowBottomLeftProgram === '') {
      this.message += this.i + '. You need to add the program name in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomLeftTitle === '') {
      this.message += this.i + '. You need to add the title in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomLeftDescription === '') {
      this.message += this.i + '. You need to add the description in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomLeftImage === 'https://via.placeholder.com/600x338' || this.emailVariables.streamingNowBottomLeftImage === '') {
      this.message += this.i + '. You need to change the image source in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomLeftAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomLeftLink === '') {
      this.message += this.i + '. You need to add the link in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the bottom left streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightProgram === '') {
      this.message += this.i + '. You need to add the program name in the bottom right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightTitle === '') {
      this.message += this.i + '. You need to add the title in the bottom right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightDescription === '') {
      this.message += this.i + '. You need to add the description in the bottom right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightImage === 'https://via.placeholder.com/600x338' || this.emailVariables.streamingNowBottomRightImage === '') {
      this.message += this.i + '. You need to change the image source in the bottom right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the bottom right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightLink === '') {
      this.message += this.i + '. You need to add the link in the bottom right streaming now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowBottomRightUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the bottom right streaming now section\n';
      this.i++;
    }
  }

  checkStreamingNowTwoDisplay(): void {
    if (this.emailVariables.streamingNowTwoLeftProgram === '') {
      this.message += this.i + '. You need to add the program name in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoLeftTitle === '') {
      this.message += this.i + '. You need to add the title in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoLeftDescription === '') {
      this.message += this.i + '. You need to add the description in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoLeftImage === 'https://via.placeholder.com/600x338' || this.emailVariables.streamingNowTwoLeftImage === '') {
      this.message += this.i + '. You need to change the image source in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoLeftAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoLeftLink === '') {
      this.message += this.i + '. You need to add the link in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the left 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightProgram === '') {
      this.message += this.i + '. You need to add the program name in the right 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightTitle === '') {
      this.message += this.i + '. You need to add the title in the right 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightDescription === '') {
      this.message += this.i + '. You need to add the description in the right 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightImage === 'https://via.placeholder.com/600x338' || this.emailVariables.streamingNowTwoRightImage === '') {
      this.message += this.i + '. You need to change the image source in the right 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the right 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightLink === '') {
      this.message += this.i + '. You need to add the link in the right 2ND Streaming Now section\n';
      this.i++;
    }
    if (this.emailVariables.streamingNowTwoRightUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the right 2ND Streaming Now section\n';
      this.i++;
    }
  }

  checkTopSingleColumn(): void {
    if (this.emailVariables.topSingleColumnImageSource === 'https://via.placeholder.com/1200x675' || this.emailVariables.topSingleColumnImageSource === '') {
      this.message += this.i + '. You need to change the main image source\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnImageAlt === '') {
      this.message += this.i + '. You need to add the image alt tag in the main section\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnLink === '') {
      this.message += this.i + '. You need to add the link in the main section\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the main section\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnProgram === '') {
      this.message += this.i + '. You need to add the program name in the main section\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnTitle === '') {
      this.message += this.i + '. You need to add the episode title in the main section\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnAirDate === '') {
      this.message += this.i + '. WARNING: The "Air Dates" is blank in the top section\n';
      this.i++;
    }
    if (this.emailVariables.topSingleColumnDescription === '') {
      this.message += this.i + '. WARNING: There is no description in the main section\n';
      this.i++;
    }
  }

  checkUpComingShowsColumnTop(): void {
    if (this.emailVariables.upcomingShowsColumnTopLeftImage === 'https://via.placeholder.com/600x338' || this.emailVariables.upcomingShowsColumnTopLeftImage === '') {
      this.message += this.i + '. You need to change the image source in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftProgram === '') {
      this.message += this.i + '. You need to add the program name in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftAlt === '') {
      this.message += this.i + '. You need to add the image alt tag in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftLink === '') {
      this.message += this.i + '. You need to add the link in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftTitle === '') {
      this.message += this.i + '. You need to add the title in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftAirDate === '') {
      this.message += this.i + '. WARNING: You are missing the "Air Dates" in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopLeftDescription === '') {
      this.message += this.i + '. You need to add the description in the top left watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightImage === 'https://via.placeholder.com/600x338' || this.emailVariables.upcomingShowsColumnTopRightImage === '') {
      this.message += this.i + '. You need to change the image source in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightProgram === '') {
      this.message += this.i + '. You need to add the program name in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightLink === '') {
      this.message += this.i + '. You need to add the link in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightTitle === '') {
      this.message += this.i + '. You need to add the title in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightAirDate === '') {
      this.message += this.i + '. WARNING: You are missing the air date in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightDescription === '') {
      this.message += this.i + '. You need to add the description in the top right watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTopRightUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the top left watch preview (2nd) section\n';
      this.i++;
    }
  }

  checkUpcomingShowsColumnTwo(): void {
    if (this.emailVariables.upcomingShowsColumnTwoLeftProgram === '') {
      this.message += this.i + '. You need to add the Program in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftTitle === '') {
      this.message += this.i + '. You need to add the Title in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftAirDate === '') {
      this.message += this.i + '. WARNING: The "Air Dates" is empty in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftImage === '' || this.emailVariables.upcomingShowsColumnTwoLeftImage === 'https://via.placeholder.com/600x338') {
      this.message += this.i + '. You need to add the Image in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftAlt === '') {
      this.message += this.i + '. You need to add the Image Alt in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftDescription === '') {
      this.message += this.i + '. You need to add the Description in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftLink === '') {
      this.message += this.i + '. You need to add the Link in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the top left watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightProgram === '') {
      this.message += this.i + '. You need to add the Program in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightTitle === '') {
      this.message += this.i + '. You need to add the Title in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightAirDate === '') {
      this.message += this.i + '. WARNING: The "Air Dates" is empty in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightAlt === '') {
      this.message += this.i + '. You need to add the Alt Tag in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightDescription === '') {
      this.message += this.i + '. You need to add the Description in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightImage === '' || this.emailVariables.upcomingShowsColumnTwoRightImage === 'https://via.placeholder.com/600x338') {
      this.message += this.i + '. You need to add the Image in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightLink === '') {
      this.message += this.i + '. You need to add the Link in the top right watch preview (2nd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnTwoRightUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the top right watch preview (2nd) section\n';
      this.i++;
    }
  }

  checkUpcomingShowsColumnThree(): void {
    if (this.emailVariables.upcomingShowsColumnThreeLeftProgram === '') {
      this.message += this.i + '. You need to add the Program in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftTitle === '') {
      this.message += this.i + '. You need to add the Title in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftAirDate === '') {
      this.message += this.i + '. WARNING: The "Air Dates" is empty in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftDescription === '') {
      this.message += this.i + '. You need to add the Description in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftAlt === '') {
      this.message += this.i + '. You need to add the Image Alt in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftImage === '' || this.emailVariables.upcomingShowsColumnThreeLeftImage === 'https://via.placeholder.com/600x338') {
      this.message += this.i + '. You need to add the Image in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftLink === '') {
      this.message += this.i + '. You need to add the Link in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the top left watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightProgram === '') {
      this.message += this.i + '. You need to add the Program in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightTitle === '') {
      this.message += this.i + '. You need to add the Title in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightAirDate === '') {
      this.message += this.i + '. WARNING: The "Air Dates" is empty in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightAlt === '') {
      this.message += this.i + '. You need to add the Alt Tag in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightDescription === '') {
      this.message += this.i + '. You need to add the Description in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightImage === '' || this.emailVariables.upcomingShowsColumnThreeRightImage === 'https://via.placeholder.com/600x338') {
      this.message += this.i + '. You need to add the Image in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightLink === '') {
      this.message += this.i + '. You need to add the Link in the top right watch preview (3rd) section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnThreeRightUTM === '') {
      this.message += this.i + '. You need to add the UTM Content in the top right watch preview (3rd) section\n';
      this.i++;
    }
  }

  checkUpcomingShowsColumnBottom(): void {
    if (this.emailVariables.upcomingShowsColumnBottomLeftProgram === '') {
      this.message += this.i + '. You need to add the program name in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftTitle === '') {
      this.message += this.i + '. You need to add the title in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftAirDate === '') {
      this.message += this.i + '. WARNING: You are missing the air date in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftDescription === '') {
      this.message += this.i + '. You need to add the description in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftImage === 'https://via.placeholder.com/600x338' || this.emailVariables.upcomingShowsColumnBottomLeftImage === '') {
      this.message += this.i + '. You need to change the image source in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftLink === '') {
      this.message += this.i + '. You need to add the link in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomLeftUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the bottom left 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightProgram === '') {
      this.message += this.i + '. You need to add the program name in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightTitle === '') {
      this.message += this.i + '. You need to add the title in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightAirDate === '') {
      this.message += this.i + '. WARNING: You are missing the air date in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightDescription === '') {
      this.message += this.i + '. You need to add the description in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightImage === 'https://via.placeholder.com/600x338' || this.emailVariables.upcomingShowsColumnBottomRightImage === '') {
      this.message += this.i + '. You need to change the image source in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightAlt === '') {
      this.message += this.i + '. You need to add the alt tag in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightLink === '') {
      this.message += this.i + '. You need to add the link in the bottom right 4th watch preview section\n';
      this.i++;
    }
    if (this.emailVariables.upcomingShowsColumnBottomRightUTM === '') {
      this.message += this.i + '. You need to add the UTM content in the bottom right 4th watch preview section\n';
      this.i++;
    }
  }

  check(): void {
    this.checkGlobalUTMCodes();
    if (this._createTemplate.checkSubDomain() === 'program-highlights') {
      this.checkTopSingleColumn();
      this.checkUpComingShowsColumnTop();
      if (this.emailVariables.upcomingShowsColumnTwoDisplay) {
        this.checkUpcomingShowsColumnTwo();
      }
      if (this.emailVariables.upcomingShowsColumnThreeDisplay) {
        this.checkUpcomingShowsColumnThree();
      }
      if (this.emailVariables.upcomingShowsColumnBottomDisplay) {
        this.checkUpcomingShowsColumnBottom();
      }
      if (this.emailVariables.alsoEnjoyDisplay) {
        this.checkAlsoEnjoy();
      }
      this.checkStreamingNow();
      if (this.emailVariables.streamingNowTwoDisplay) {
        this.checkStreamingNowTwoDisplay();
      }
      if (this.emailVariables.streamingNowBottomDisplay) {
        this.checkStreamingNowBottomDisplay();
      }
      if (this.emailVariables.passportAdDisplay) {
        this.checkPassportAd();
      }
      this.checkBottomSingleColumn();
      if (this.emailVariables.extraSingleColumnHeadingDisplay) {
        this.checkExtraSingleColumn();
      }
      if (this.emailVariables.monthDisplay) {
        this.checkMonthSection();
      }
      if (this.emailVariables.singleColumnDisplay) {
        this.checkSingleColumnSection();
      }
      this.checkAdProviders();
    } else if (this._createTemplate.checkSubDomain() === 'dedicated') {
      this.checkSingleColumnSection();
      if (this.emailVariables.partnerSpotlightDisplay) {
        this.checkPartnerSpotlight();
      }
      if (this.emailVariables.partnerSpotlightTwoDisplay) {
        this.checkPartnerSpotlightTwo();
      }
      if(this.emailVariables.closingDisplay){
        this.checkClosing();
      }
    }
    if (this.message !== 'There are warnings/errors on this email template:\n') {
      alert(this.message);
      this.message = 'There are warnings/errors on this email template:\n';
      this.i = 1;
    }
  }
  
  constructor(private _createTemplate: CreateTemplateService) { }
}
