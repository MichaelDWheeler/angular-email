import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ExtraSingleColumnComponent } from './extra-single-column.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('ExtraSingleColumnComponent', () => {
  let component: ExtraSingleColumnComponent;
  let fixture: ComponentFixture<ExtraSingleColumnComponent>;
  const templateVariables = TEMPLATEVARIABLES;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ExtraSingleColumnComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraSingleColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set email object to equal default templateVariables', () => {
    expect(component.email).toEqual(templateVariables);
  });

  it('should set the variables with custom title', () => {
    component.email.extraSingleColumnTitle = "Custom Title";
    component.addVariables();
    expect(component.email.extraSingleColumnAlt).toEqual("Custom Title");
    expect(component.email.extraSingleColumnUTM).toEqual("Custom%20Title");
   });

});
