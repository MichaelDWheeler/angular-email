import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-extra-single-column',
  templateUrl: './extra-single-column.component.html',
  styleUrls: ['./extra-single-column.component.css']
})
export class ExtraSingleColumnComponent implements OnInit {
  @Input() extraSingleColumnUTM: string;
  @Input() extraSingleColumnAlt: string;
  email: any = {};
  showComponent: boolean;
  extraSingleColumnLink: string;
  extraSingleColumnImage: string;
  extraSingleColumnTitle: string;
  extraSingleColumnSubTitle: string;
  extraSingleColumnAdditionalInfo: string;
  extraSingleColumnContent: string;
  extraSingleColumnDescription: string;
  extraSingleColumnButton: string;

  addVariables(): void {
    this.createTemplate.setVariables('extraSingleColumnAlt', this.extraSingleColumnTitle);
    this.createTemplate.setVariables('extraSingleColumnUTM', this.extraSingleColumnTitle);
    this.createTemplate.changeVariables('extraSingleColumnTitle',this.extraSingleColumnTitle);
    this.extraSingleColumnUTM = this.email.extraSingleColumnUTM;
    this.extraSingleColumnAlt = this.email.extraSingleColumnAlt;
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public sanitizer: DomSanitizer,
    public font: FontService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.extraSingleColumnHeadingDisplay;
    this.extraSingleColumnLink = this.email.extraSingleColumnLink;
    this.extraSingleColumnImage = this.email.extraSingleColumnImage;
    this.extraSingleColumnTitle = this.email.extraSingleColumnTitle;
    this.extraSingleColumnSubTitle = this.email.extraSingleColumnSubTitle;
    this.extraSingleColumnAdditionalInfo = this.email.extraSingleColumnAdditionalInfo;
    this.extraSingleColumnContent = this.email.extraSingleColumnContent;
    this.extraSingleColumnDescription = this.email.extraSingleColumnDescription;
    this.extraSingleColumnButton = this.email.extraSingleColumnButton;
    this.extraSingleColumnUTM = this.email.extraSingleColumnUTM;
    this.extraSingleColumnAlt = this.email.extraSingleColumnAlt;
  }

}
