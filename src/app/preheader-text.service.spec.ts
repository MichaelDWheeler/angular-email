import { TestBed, inject } from '@angular/core/testing';

import { PreheaderTextService } from './preheader-text.service';

describe('PreheaderTextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PreheaderTextService]
    });
  });

  it('should be created', inject([PreheaderTextService], (service: PreheaderTextService) => {
    expect(service).toBeTruthy();
  }));
});
