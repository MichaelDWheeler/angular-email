import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-divider',
  templateUrl: './divider.component.html',
  styleUrls: ['./divider.component.css']
})
export class DividerComponent implements OnInit {
  @Input() showComponent: boolean;
  component = 'divider';
  email: any;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
  }

  constructor(public createTemplate: CreateTemplateService) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
  }

}
