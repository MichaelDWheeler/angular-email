import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DividerComponent } from './divider.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('DividerComponent', () => {
  let component: DividerComponent;
  let fixture: ComponentFixture<DividerComponent>;
  const templateVariables = TEMPLATEVARIABLES;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DividerComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DividerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "showComponent" to change between true and false', () => {
    component.toggleControls();
    expect(component.showComponent).toBeFalsy;
    component.toggleControls();
    expect(component.showComponent).toBeTruthy;
  });

  it('the email object should equal the default properties and values in TEMPLATEVARIABLES', ()=>{
    component.email = component.createTemplate.getVariables();
    expect(component.email).toEqual(templateVariables);
  });

});
