import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-providing-support-ads',
  templateUrl: './providing-support-ads.component.html',
  styleUrls: ['./providing-support-ads.component.css']
})
export class ProvidingSupportAdsComponent implements OnInit {
  email: any = {};
  supporters: any = [];
  image = '';
  link = '';
  alt = 'Click to view - our valued supporter';

  addSupporter(image: string, link: string, alt: string): void {
    if (this.image === '' || this.link === '') {
      alert('You must include an image and a link for the ad');
      return;
    } else {
      this.supporters.push({ 'image': image, 'link': link, 'alt': alt });
      this.image = '';
      this.link = '';
      this.alt = 'Click to view our valued supporter';
      this.createTemplate.setVariables('adProviders', this.supporters);
    }
  }

  removeAd(ad: any): void {
    const position = this.supporters.indexOf(ad);
    this.supporters.splice(position, 1);
    this.createTemplate.setVariables('adProviders', this.supporters);
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.supporters = this.createTemplate.getSupporters();
  }
}
