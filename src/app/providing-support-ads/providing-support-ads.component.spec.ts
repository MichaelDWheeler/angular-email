import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidingSupportAdsComponent } from './providing-support-ads.component';
import { FormsModule } from '@angular/forms';

describe('ProvidingSupportAdsComponent', () => {
  let component: ProvidingSupportAdsComponent;
  let fixture: ComponentFixture<ProvidingSupportAdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProvidingSupportAdsComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidingSupportAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have called an alert with the message "You must include an image and a link for the ad"', () => { 
    spyOn(window, 'alert');
    component.image = '';
    component.link = '';
    component.alt = 'test';
    component.addSupporter(component.image, component.link, component.alt);
    expect(window.alert).toHaveBeenCalledWith('You must include an image and a link for the ad');
  });
  
  it('should modify the supporters object with certain key/value pairs', ()=>{
    component.image = "testImage";
    component.link = "testLink";
    component.alt = "Click to view our valued supporter";
    component.addSupporter(component.image, component.link, component.alt);
    expect(component.supporters).toBe[ Object({ image: 'testImage', link: 'testLink', alt: 'Click to view our valued supporter' }) ];
  });

  it('should remove the object from supporters array', ()=>{
    component.supporters = [ Object({ image: 'testImage', link: 'testLink', alt: 'Click to view our valued supporter' }) ];
    let obj = [ Object({ image: 'testImage', link: 'testLink', alt: 'Click to view our valued supporter' }) ];
    component.removeAd(obj);
    expect(component.supporters).toEqual([  ]);
  });
});
