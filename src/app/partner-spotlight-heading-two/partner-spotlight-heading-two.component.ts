import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-partner-spotlight-heading-two',
  templateUrl: './partner-spotlight-heading-two.component.html',
  styleUrls: ['./partner-spotlight-heading-two.component.css'],
})
export class partnerSpotlightTwoHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'heading';

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.partnerSpotlightTwoHeadingDisplay = this.showComponent;
  }
  
  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.partnerSpotlightTwoHeadingDisplay;
  }
}
