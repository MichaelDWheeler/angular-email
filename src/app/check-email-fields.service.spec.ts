import { TestBed, inject } from '@angular/core/testing';

import { CheckEmailFieldsService } from './check-email-fields.service';

describe('CheckEmailFieldsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckEmailFieldsService]
    });
  });

  it('should be created', inject([CheckEmailFieldsService], (service: CheckEmailFieldsService) => {
    expect(service).toBeTruthy();
  }));
});
