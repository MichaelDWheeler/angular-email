import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassportTemplateComponent } from './passport-template.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { GlobalUtmComponent } from '../global-utm/global-utm.component';
import { EmailHeadingComponent } from '../email-heading/email-heading.component';
import { TextDescriptionComponent } from '../text-description/text-description.component';
import { DividerComponent } from '../divider/divider.component';
import { UpcomingShowsHeadingComponent } from '../upcoming-shows-heading/upcoming-shows-heading.component';
import { ComingSoonTopComponent } from '../coming-soon-top/coming-soon-top.component';
import { ComingSoonBottomComponent } from '../coming-soon-bottom/coming-soon-bottom.component';
import { BannerAdComponent } from '../banner-ad/banner-ad.component';
import { TitleAndTextComponent } from '../title-and-text/title-and-text.component';
import { ComingSoonTwoComponent } from '../coming-soon-two/coming-soon-two.component';
import { SingleColumnHeadingComponent } from '../single-column-heading/single-column-heading.component';
import { SingleColumnContentComponent } from '../single-column-content/single-column-content.component';
import { ComingSoonThreeComponent } from '../coming-soon-three/coming-soon-three.component';
import { ComingSoonFourComponent } from '../coming-soon-four/coming-soon-four.component';
import { ContentImageWrapComponent } from '../content-image-wrap/content-image-wrap.component';
import { ContentImageWrapHeadingComponent } from '../content-image-wrap-heading/content-image-wrap-heading.component';
import { LittleMoreHeadingComponent } from '../little-more-heading/little-more-heading.component';
import { LittleMoreComponent } from '../little-more/little-more.component';
import { ClosingComponent } from '../closing/closing.component';
import { FooterComponent } from '../footer/footer.component';
import { CodeOutComponent } from '../code-out/code-out.component';
import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PassportTemplateComponent', () => {
  let component: PassportTemplateComponent;
  let fixture: ComponentFixture<PassportTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PassportTemplateComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        GlobalUtmComponent,
        EmailHeadingComponent,
        TextDescriptionComponent,
        DividerComponent,
        UpcomingShowsHeadingComponent,
        ComingSoonTopComponent,
        ComingSoonBottomComponent,
        BannerAdComponent,
        TitleAndTextComponent,
        ComingSoonTwoComponent,
        SingleColumnHeadingComponent,
        ComingSoonThreeComponent,
        ComingSoonFourComponent,
        ContentImageWrapHeadingComponent,
        ContentImageWrapComponent,
        LittleMoreHeadingComponent,
        LittleMoreComponent,
        ClosingComponent,
        FooterComponent,
        CodeOutComponent
      ],
      imports: [FormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassportTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
