import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-passport-template',
  templateUrl: './passport-template.component.html',
  styleUrls: ['./passport-template.component.css']
})
export class PassportTemplateComponent implements OnInit {
  email: any = {};
  showComponent = true;
  
  constructor(public createTemplate: CreateTemplateService) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
  }

}
