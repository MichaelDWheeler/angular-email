import { TestBed, inject } from '@angular/core/testing';

import { PagenameService } from './pagename.service';

describe('PagenameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PagenameService]
    });
  });

  it('should be created', inject([PagenameService], (service: PagenameService) => {
    expect(service).toBeTruthy();
  }));
});
