import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComingSoonThreeComponent } from './coming-soon-three.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestComponent } from '../add-interest/add-interest.component';
import { FormsModule } from '@angular/forms';

describe('ComingSoonThreeComponent', () => {
  let component: ComingSoonThreeComponent;
  let fixture: ComponentFixture<ComingSoonThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ComingSoonThreeComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingSoonThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
