import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { CreateTemplateService } from '../create-template.service';
import { AddInterestService } from '../add-interest.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-coming-soon-three',
  templateUrl: './coming-soon-three.component.html',
  styleUrls: ['./coming-soon-three.component.css']
})
export class ComingSoonThreeComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = 'two programs';
  addInterest: any = [];
  topLeft = 'comingSoonThreeLeftAddInterest';
  topRight = 'comingSoonThreeRightAddInterest';
  comingSoonThreeLeftProgram: string;
  comingSoonThreeLeftAlt: string;
  comingSoonThreeLeftUTM: string;
  comingSoonThreeLeftTitle: string;
  comingSoonThreeLeftAirDate: string;
  comingSoonThreeLeftButton: string;
  comingSoonThreeLeftDescription: string;
  comingSoonThreeLeftImage: string;
  comingSoonThreeLeftLink: string;
  comingSoonThreeRightProgram: string;
  comingSoonThreeRightAlt: string;
  comingSoonThreeRightUTM: string;
  comingSoonThreeRightTitle: string;
  comingSoonThreeRightAirDate: string;
  comingSoonThreeRightButton: string;
  comingSoonThreeRightDescription: string;
  comingSoonThreeRightImage: string;
  comingSoonThreeRightLink: string;
  subscription: Subscription;


  addVariables(): void {
    this.createTemplate.setVariables('comingSoonThreeLeftAlt', this.comingSoonThreeLeftProgram);
    this.createTemplate.setVariables('comingSoonThreeLeftUTM', this.comingSoonThreeLeftProgram);
    this.createTemplate.changeVariables('comingSoonThreeLeftProgram', this.comingSoonThreeLeftProgram);
    this.comingSoonThreeLeftAlt = this.email.comingSoonThreeLeftAlt;
    this.comingSoonThreeLeftUTM = this.email.comingSoonThreeLeftUTM;
    this.createTemplate.setVariables('comingSoonThreeRightAlt', this.comingSoonThreeRightProgram);
    this.createTemplate.setVariables('comingSoonThreeRightUTM', this.comingSoonThreeRightProgram);
    this.createTemplate.changeVariables('comingSoonThreeRightProgram', this.comingSoonThreeRightProgram);
    this.comingSoonThreeRightAlt = this.email.comingSoonThreeRightAlt;
    this.comingSoonThreeRightUTM = this.email.comingSoonThreeRightUTM;
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.comingSoonThreeDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void {
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if (message) {
        this.comingSoonThreeLeftUTM = this.email.comingSoonThreeLeftUTM;
        this.comingSoonThreeRightUTM = this.email.comingSoonThreeRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public interest: AddInterestService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.comingSoonThreeDisplay;
    this.comingSoonThreeLeftProgram = this.email.comingSoonThreeLeftProgram;
    this.comingSoonThreeLeftAlt = this.email.comingSoonThreeLeftAlt;
    this.comingSoonThreeLeftUTM = this.email.comingSoonThreeLeftUTM;
    this.comingSoonThreeLeftTitle = this.email.comingSoonThreeLeftTitle;
    this.comingSoonThreeLeftAirDate = this.email.comingSoonThreeLeftAirDate;
    this.comingSoonThreeLeftButton = this.email.comingSoonThreeLeftButton;
    this.comingSoonThreeLeftDescription = this.email.comingSoonThreeLeftDescription;
    this.comingSoonThreeLeftImage = this.email.comingSoonThreeLeftImage;
    this.comingSoonThreeLeftLink = this.email.comingSoonThreeLeftLink;
    this.comingSoonThreeRightAlt = this.email.comingSoonThreeRightAlt;
    this.comingSoonThreeRightUTM = this.email.comingSoonThreeRightUTM;
    this.comingSoonThreeRightTitle = this.email.comingSoonThreeRightTitle;
    this.comingSoonThreeRightAirDate = this.email.comingSoonThreeRightAirDate;
    this.comingSoonThreeRightButton = this.email.comingSoonThreeRightButton;
    this.comingSoonThreeRightDescription = this.email.comingSoonThreeRightDescription;
    this.comingSoonThreeRightImage = this.email.comingSoonThreeRightImage;
    this.comingSoonThreeRightLink = this.email.comingSoonThreeRightLink;
    this.comingSoonThreeRightProgram = this.email.comingSoonThreeRightProgram;
    this.utmSubscription();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
