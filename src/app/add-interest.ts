export interface AddInterest {
  order: number;
  id: number;
  category: string;
}
