import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { EmailHeadingComponent } from './email-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('EmailHeadingComponent', () => {
  let component: EmailHeadingComponent;
  let fixture: ComponentFixture<EmailHeadingComponent>;
  const templateVariables = TEMPLATEVARIABLES;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EmailHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [ FormsModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "showComponent" to change between true and false', () => {
    component.toggleControls();
    expect(component.showComponent).toBeFalsy;
    component.toggleControls();
    expect(component.showComponent).toBeTruthy;
  });

  it('the email object should equal the default properties and values in TEMPLATEVARIABLES', ()=>{
    component.email = component.createTemplate.getVariables();
    expect(component.email).toEqual(templateVariables);
  });

});
