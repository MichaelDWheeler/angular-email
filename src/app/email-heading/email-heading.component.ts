import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
    selector: 'app-email-heading',
    templateUrl: './email-heading.component.html',
    styleUrls: ['./email-heading.component.css'],
})
export class EmailHeadingComponent implements OnInit {
    email: any = {};
    showComponent: boolean;
    component = 'heading';
    emailHeading: string;

    toggleControls(): void {
        this.showComponent = !this.showComponent;
        this.email.emailHeadingDisplay = this.showComponent;
        this.createTemplate.setStorage();
    }

    constructor(
        public select: SelectTextService,
        public createTemplate: CreateTemplateService
    ) { }

    ngOnInit() {
        this.email = this.createTemplate.getVariables();
        this.showComponent = this.email.emailHeadingDisplay;
        this.emailHeading = this.email.emailHeading;
    }

}
