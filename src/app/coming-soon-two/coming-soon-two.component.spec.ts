import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComingSoonTwoComponent } from './coming-soon-two.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestComponent } from '../add-interest/add-interest.component';
import { FormsModule } from '@angular/forms';

describe('ComingSoonTwoComponent', () => {
  let component: ComingSoonTwoComponent;
  let fixture: ComponentFixture<ComingSoonTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ComingSoonTwoComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingSoonTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
