import { Component, OnInit} from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { CreateTemplateService } from '../create-template.service';
import { AddInterestService } from '../add-interest.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-coming-soon-two',
  templateUrl: './coming-soon-two.component.html',
  styleUrls: ['./coming-soon-two.component.css']
})
export class ComingSoonTwoComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'Two Programs';
  addInterest: any = [];
  topLeft = 'comingSoonTwoLeftAddInterest';
  topRight = 'comingSoonTwoRightAddInterest';
  comingSoonTwoLeftProgram: string;
  comingSoonTwoLeftTitle: string;
  comingSoonTwoLeftAirDate: string;
  comingSoonTwoLeftButton: string;
  comingSoonTwoLeftDescription: string;
  comingSoonTwoLeftImage: string;
  comingSoonTwoLeftAlt: string;
  comingSoonTwoLeftLink: string;
  comingSoonTwoLeftUTM: string;
  comingSoonTwoRightProgram: string;
  comingSoonTwoRightAlt: string;
  comingSoonTwoRightUTM: string;
  comingSoonTwoRightTitle: string;
  comingSoonTwoRightAirDate: string;
  comingSoonTwoRightButton: string;
  comingSoonTwoRightDescription: string;
  comingSoonTwoRightImage: string;
  comingSoonTwoRightLink: string;

  addVariables(): void {
    this.createTemplate.setVariables('comingSoonTwoLeftAlt', this.comingSoonTwoLeftProgram);
    this.createTemplate.setVariables('comingSoonTwoLeftUTM', this.comingSoonTwoLeftProgram);
    this.createTemplate.changeVariables('comingSoonTwoLeftProgram',this.comingSoonTwoLeftProgram);
    this.createTemplate.setVariables('comingSoonTwoRightAlt', this.comingSoonTwoRightProgram);
    this.createTemplate.setVariables('comingSoonTwoRightUTM', this.comingSoonTwoRightProgram);
    this.createTemplate.changeVariables('comingSoonTwoRightProgram',this.comingSoonTwoRightProgram);
    this.comingSoonTwoLeftAlt = this.email.comingSoonTwoLeftAlt;
    this.comingSoonTwoLeftUTM = this.email.comingSoonTwoLeftUTM;
    this.comingSoonTwoRightAlt = this.email.comingSoonTwoRightAlt;
    this.comingSoonTwoRightUTM = this.email.comingSoonTwoRightUTM;
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.comingSoonTwoDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public interest: AddInterestService,
    public sanitizer: DomSanitizer
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.comingSoonTwoDisplay;
    this.comingSoonTwoLeftProgram = this.email.comingSoonTwoLeftProgram;
    this.comingSoonTwoLeftTitle = this.email.comingSoonTwoLeftTitle;
    this.comingSoonTwoLeftAirDate = this.email.comingSoonTwoLeftAirDate;
    this.comingSoonTwoLeftButton = this.email.comingSoonTwoLeftButton;
    this.comingSoonTwoLeftDescription = this.email.comingSoonTwoLeftDescription;
    this.comingSoonTwoLeftImage = this.email.comingSoonTwoLeftImage;
    this.comingSoonTwoLeftAlt = this.email.comingSoonTwoLeftAlt;
    this.comingSoonTwoLeftLink = this.email.comingSoonTwoLeftLink;
    this.comingSoonTwoLeftUTM = this.email.comingSoonTwoLeftUTM;
    this.comingSoonTwoRightAlt = this.email.comingSoonTwoRightAlt;
    this.comingSoonTwoRightUTM = this.email.comingSoonTwoRightUTM;
    this.comingSoonTwoRightTitle = this.email.comingSoonTwoRightTitle;
    this.comingSoonTwoRightAirDate = this.email.comingSoonTwoRightAirDate;
    this.comingSoonTwoRightButton = this.email.comingSoonTwoRightButton;
    this.comingSoonTwoRightDescription = this.email.comingSoonTwoRightDescription;
    this.comingSoonTwoRightImage = this.email.comingSoonTwoRightImage;
    this.comingSoonTwoRightLink = this.email.comingSoonTwoRightLink;
    this.comingSoonTwoRightProgram = this.email.comingSoonTwoRightProgram;
  }

}
