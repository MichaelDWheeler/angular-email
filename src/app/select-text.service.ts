import { Injectable } from '@angular/core';
import { CheckEmailFieldsService } from './check-email-fields.service';

@Injectable({
  providedIn: 'root'
})
export class SelectTextService {
  public selectAllText($event: any): void {
    $event.target.select();
  }

  constructor(private _check: CheckEmailFieldsService) { }
}
