import { TestBed, inject } from '@angular/core/testing';

import { AddSupportersService } from './add-supporters.service';

describe('AddSupportersService', () => {
  let service: AddSupportersService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddSupportersService]
    });
    service = new AddSupportersService;
  });

  it('should be created', inject([AddSupportersService], (service: AddSupportersService) => {
    expect(service).toBeTruthy();
  }));

  it('should return', ()=>{
    service.supporters = [{
      image: "testImage",
      link: "testLink",
      alt: "testAlt"
    }];
    const array: any = [] = service.getSupporters();
    expect(array).toEqual([ Object({ image: 'testImage', link: 'testLink', alt: 'testAlt' }) ]);
  });
});
