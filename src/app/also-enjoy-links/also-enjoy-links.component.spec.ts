import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AlsoEnjoyLinksComponent } from './also-enjoy-links.component';

describe('AlsoEnjoyLinksComponent', () => {
  let component: AlsoEnjoyLinksComponent;
  let fixture: ComponentFixture<AlsoEnjoyLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlsoEnjoyLinksComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlsoEnjoyLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.dataenjoylinks = [];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a new link', () => {
    component.programName = 'This is a test';
    component.programUTM = component.removeWhiteSpace(component.programName);
    component.programLink = 'test-link.html';
    component.programTitle = 'Test Title';
    component.airsDate = 'Sept 19, 2018';
    component.addNewLink();
    expect(component.dataenjoylinks).toEqual([{ show: 'This is a test', link: 'test-link.html', title: 'Test Title', date: 'Sept 19, 2018', UTM: 'This%20is%20a%20test' }]);
  });

  it('should return an empty object', () => {
    component.dataenjoylinks = [{ show: 'This is a test', link: 'test-link.html', title: 'Test Title', date: 'Sept 19, 2018', UTM: 'This%20is%20a%20test' }];
    component.removeLink(0);
    expect(component.dataenjoylinks.length).toEqual(0);
  });

});
