import { Component, OnInit, Input } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-also-enjoy-links',
  templateUrl: './also-enjoy-links.component.html',
  styleUrls: ['./also-enjoy-links.component.css']
})
export class AlsoEnjoyLinksComponent implements OnInit {
  @Input() showComponent: boolean;
  component = 'also enjoy';
  programName: string;
  programTitle: string;
  programLink: string;
  programUTM: string;
  airsDate: string;
  dataenjoylinks: any;
  email: any = {};

  removeWhiteSpace(value: string): string {
    const str = value.replace(/ /g, '%20');
    return str;
  }

  addNewLink(): void {
    this.programUTM = this.removeWhiteSpace(this.programName);
    this.dataenjoylinks.push({
      show: this.programName,
      link: this.programLink,
      title: this.programTitle,
      date: this.airsDate,
      UTM: this.programUTM
    });
    this.programName = '';
    this.programTitle = '';
    this.programLink = '';
    this.airsDate = '';
    this.createTemplate.setVariables('dataEnjoyLinks', this.dataenjoylinks);
    this.createTemplate.setStorage();
  }

  removeLink(link): void {
    const position = this.dataenjoylinks.indexOf(link);
    this.dataenjoylinks.splice(position, 1);
    this.createTemplate.setVariables('dataEnjoyLinks', this.dataenjoylinks);
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.dataenjoylinks = this.createTemplate.getLinks(this.component);
  }

}
