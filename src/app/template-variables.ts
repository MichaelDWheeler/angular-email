import { EnjoyLinks } from './enjoy-links';
import { Supporters } from './supporters';

export interface TemplateVariables {
    adProviders: Supporters[];
    alsoEnjoyDisplay: boolean;
    alsoEnjoyHeading: string;
    bannerAdDisplay: boolean;
    bannerAdImageSrc: string;
    bannerAdLink: string;
    bottomSingleHeadingDisplay: boolean;
    bottomSingleColumnAdditionalInfo: string;
    bottomSingleColumnAlt: string;
    bottomSingleColumnButton: string;
    bottomSingleColumnDescription: string;
    bottomSingleColumnHeading: string;
    bottomSingleColumnImage: string;
    bottomSingleColumnLink: string;
    bottomSingleColumnSubTitle: string;
    bottomSingleColumnTitle: string;
    bottomSingleColumnUTM: string;
    closingDisplay: boolean;
    closingImageSrc: string;
    closingName: string;
    closingText: string;
    closingTitle: string;
    closingValediction: string;
    comingSoonBottomDisplay: boolean;
    comingSoonBottomLeftAddInterest: number;
    comingSoonBottomLeftAddInterestConverted: string;
    comingSoonBottomLeftAirDate: string;
    comingSoonBottomLeftAlt: string;
    comingSoonBottomLeftButton: string;
    comingSoonBottomLeftDescription: string;
    comingSoonBottomLeftImage: string;
    comingSoonBottomLeftLink: string;
    comingSoonBottomLeftProgram: string;
    comingSoonBottomLeftTitle: string;
    comingSoonBottomLeftUTM: string;
    comingSoonBottomRightAddInterest: number;
    comingSoonBottomRightAddInterestConverted: string;
    comingSoonBottomRightAirDate: string;
    comingSoonBottomRightAlt: string;
    comingSoonBottomRightButton: string;
    comingSoonBottomRightDescription: string;
    comingSoonBottomRightImage: string;
    comingSoonBottomRightLink: string;
    comingSoonBottomRightProgram: string;
    comingSoonBottomRightTitle: string;
    comingSoonBottomRightUTM: string;
    comingSoonTopDisplay: boolean;
    comingSoonTopLeftAddInterest: number;
    comingSoonTopLeftAddInterestConverted: string;
    comingSoonTopLeftAirDate: string;
    comingSoonTopLeftAlt: string;
    comingSoonTopLeftButton: string;
    comingSoonTopLeftDescription: string;
    comingSoonTopLeftImage: string;
    comingSoonTopLeftLink: string;
    comingSoonTopLeftProgram: string;
    comingSoonTopLeftTitle: string;
    comingSoonTopLeftUTM: string;
    comingSoonTopRightAddInterest: number;
    comingSoonTopRightAddInterestConverted: string;
    comingSoonTopRightAirDate: string;
    comingSoonTopRightAlt: string;
    comingSoonTopRightButton: string;
    comingSoonTopRightDescription: string;
    comingSoonTopRightImage: string;
    comingSoonTopRightLink: string;
    comingSoonTopRightProgram: string;
    comingSoonTopRightTitle: string;
    comingSoonTopRightUTM: string;
    comingSoonTwoDisplay: boolean;
    comingSoonTwoLeftAddInterest: number;
    comingSoonTwoLeftAddInterestConverted: string;
    comingSoonTwoLeftAirDate: string;
    comingSoonTwoLeftAlt: string;
    comingSoonTwoLeftButton: string;
    comingSoonTwoLeftDescription: string;
    comingSoonTwoLeftImage: string;
    comingSoonTwoLeftLink: string;
    comingSoonTwoLeftProgram: string;
    comingSoonTwoLeftTitle: string;
    comingSoonTwoLeftUTM: string;
    comingSoonTwoRightAddInterest: number;
    comingSoonTwoRightAddInterestConverted: string;
    comingSoonTwoRightAirDate: string;
    comingSoonTwoRightAlt: string;
    comingSoonTwoRightButton: string;
    comingSoonTwoRightDescription: string;
    comingSoonTwoRightImage: string;
    comingSoonTwoRightLink: string;
    comingSoonTwoRightProgram: string;
    comingSoonTwoRightTitle: string;
    comingSoonTwoRightUTM: string;
    comingSoonThreeDisplay: boolean;
    comingSoonThreeLeftAddInterest: number;
    comingSoonThreeLeftAddInterestConverted: string;
    comingSoonThreeLeftAirDate: string;
    comingSoonThreeLeftAlt: string;
    comingSoonThreeLeftButton: string;
    comingSoonThreeLeftDescription: string;
    comingSoonThreeLeftImage: string;
    comingSoonThreeLeftLink: string;
    comingSoonThreeLeftProgram: string;
    comingSoonThreeLeftTitle: string;
    comingSoonThreeLeftUTM: string;
    comingSoonThreeRightAddInterest: number;
    comingSoonThreeRightAddInterestConverted: string;
    comingSoonThreeRightAirDate: string;
    comingSoonThreeRightAlt: string;
    comingSoonThreeRightButton: string;
    comingSoonThreeRightDescription: string;
    comingSoonThreeRightImage: string;
    comingSoonThreeRightLink: string;
    comingSoonThreeRightProgram: string;
    comingSoonThreeRightTitle: string;
    comingSoonThreeRightUTM: string;
    comingSoonFourDisplay: boolean;
    comingSoonFourLeftAddInterest: number;
    comingSoonFourLeftAddInterestConverted: string;
    comingSoonFourLeftAirDate: string;
    comingSoonFourLeftAlt: string;
    comingSoonFourLeftButton: string;
    comingSoonFourLeftDescription: string;
    comingSoonFourLeftImage: string;
    comingSoonFourLeftLink: string;
    comingSoonFourLeftProgram: string;
    comingSoonFourLeftTitle: string;
    comingSoonFourLeftUTM: string;
    comingSoonFourRightAddInterest: number;
    comingSoonFourRightAddInterestConverted: string;
    comingSoonFourRightAirDate: string;
    comingSoonFourRightAlt: string;
    comingSoonFourRightButton: string;
    comingSoonFourRightDescription: string;
    comingSoonFourRightImage: string;
    comingSoonFourRightLink: string;
    comingSoonFourRightProgram: string;
    comingSoonFourRightTitle: string;
    comingSoonFourRightUTM: string;
    contentImageWrap: string;
    contentImageWrapDisplay: boolean;
    contentImageWrapHeading: string;
    contentImageWrapHeadingDisplay: boolean;
    contentImageWrapDescription: string;
    contentImageWrapSrc: string;
    dataEnjoyLinks: EnjoyLinks[];
    emailHeadingDisplay: boolean;
    emailHeading: string;
    emailTemplate: string;
    episodeFontSize: number;
    extraSingleColumnAdditionalInfo: string;
    extraSingleColumnAlt: string;
    extraSingleColumnButton: string;
    extraSingleColumnContent: string;
    extraSingleColumnDescription: string;
    extraSingleColumnHeadingDisplay: boolean;
    extraSingleColumnHeading: string;
    extraSingleColumnImage: string;
    extraSingleColumnLink: string;
    extraSingleColumnTitle: string;
    extraSingleColumnSubTitle: string;
    extraSingleColumnUTM: string;
    littleMoreAddInterest: number;
    littleMoreAddInterestConverted: string;
    littleMoreHeading: string;
    littleMoreHeadingDisplay: boolean;
    littleMoreDescription: string;
    littleMoreDisplay: boolean;
    littleMoreLinks: EnjoyLinks[];
    littleMoreTitle: string;
    littleMoreTitleRight: string;
    littleMoreUTM: string;
    localStorage: boolean;
    luminateSubjectLine: string;
    membershipSelected: Number;
    membershipUTMSource: string;
    monthAlt: string;
    monthButton: string;
    monthDescription: string;
    monthDisplay: boolean;
    monthImage: string;
    monthLink: string;
    monthTitle: string;
    monthUTM: string;
    pagename: string;
    pagenameConverted: string;
    partnerSpotlightAddInterest: number;
    partnerSpotlightAddInterestConverted: string;
    partnerSpotlightButton: string;
    partnerSpotlightDate: string;
    partnerSpotlightDescription: string;
    partnerSpotlightDisplay: boolean;
    partnerSpotlightHeading: string;
    partnerSpotlightHeadingDisplay: boolean;
    partnerSpotlightImageAlt: string;
    partnerSpotlightImageSrc: string;
    partnerSpotlightLink: string;
    partnerSpotlightLocation: string;
    partnerSpotlightTitle: string;
    partnerSpotlightUTM: string;
    partnerSpotlightTwoAddInterest: number;
    partnerSpotlightTwoAddInterestConverted: string;
    partnerSpotlightTwoButton: string;
    partnerSpotlightTwoDate: string;
    partnerSpotlightTwoDescription: string;
    partnerSpotlightTwoDisplay: boolean;
    partnerSpotlightTwoHeading: string;
    partnerSpotlightTwoHeadingDisplay: boolean;
    partnerSpotlightTwoImageAlt: string;
    partnerSpotlightTwoImageSrc: string;
    partnerSpotlightTwoLink: string;
    partnerSpotlightTwoLocation: string;
    partnerSpotlightTwoTitle: string;
    partnerSpotlightTwoUTM: string;
    passportAdAlt: string;
    passportAdButton: string;
    passportAdDescription: string;
    passportAdDisplay: boolean;
    passportAdImage: string;
    passportAdLink: string;
    passportAdUTM: string;
    passportAdHeaderAlt: string;
    passportAdHeaderImage: string;
    passportAdHeaderLink: string;
    passportAdHeaderUTM: string;
    passportLearnMoreAddInterest: number;
    passportLearnMoreAddInterestConverted: string;
    passportLearnMoreButton: string;
    passportLearnMoreDescription: string;
    passportLearnMoreDisplay: boolean;
    passportLearnMoreHeadingDisplay: boolean;
    passportLearnMoreImage: string;
    passportLearnMoreLink: string;
    passportLearnMoreTitle: string;
    programFontSize: number;
    providingSupport: string;
    salutation: string;
    showControls: boolean;
    singleColumnDisplay: boolean;
    singleColumnAddInterest: number;
    singleColumnAddInterestConverted: string;
    singleColumnButton: string;
    singleColumnContent: string;
    singleColumnDate: string;
    singleColumnDescription: string;
    singleColumnHeading: string;
    singleColumnImageAlt: string;
    singleColumnImageSource: string;
    singleColumnLink: string;
    singleColumnLocation: string;
    singleColumnTitle: string;
    singleImageAddInterest: number;
    singleImageAddInterestConverted: string;
    singleImageDisplay: boolean;
    singleImageSource: string;
    singleImageLink: string;
    streamingNow: string;
    streamingNowBottomDisplay: boolean;
    streamingNowBottomLeftAlt: string;
    streamingNowBottomLeftDescription: string;
    streamingNowBottomLeftImage: string;
    streamingNowBottomLeftLink: string;
    streamingNowBottomLeftProgram: string;
    streamingNowBottomLeftTitle: string;
    streamingNowBottomLeftUTM: string;
    streamingNowBottomRightAlt: string;
    streamingNowBottomRightDescription: string;
    streamingNowBottomRightImage: string;
    streamingNowBottomRightLink: string;
    streamingNowBottomRightProgram: string;
    streamingNowBottomRightTitle: string;
    streamingNowBottomRightUTM: string;
    streamingNowTopLeftAlt: string;
    streamingNowTopLeftDescription: string;
    streamingNowTopLeftImage: string;
    streamingNowTopLeftLink: string;
    streamingNowTopLeftProgram: string;
    streamingNowTopLeftTitle: string;
    streamingNowTopLeftUTM: string;
    streamingNowTopRightAlt: string;
    streamingNowTopRightDescription: string;
    streamingNowTopRightImage: string;
    streamingNowTopRightLink: string;
    streamingNowTopRightProgram: string;
    streamingNowTopRightTitle: string;
    streamingNowTopRightUTM: string;
    streamingNowTwoDisplay: boolean;
    streamingNowTwoLeftAlt: string;
    streamingNowTwoLeftDescription: string;
    streamingNowTwoLeftImage: string;
    streamingNowTwoLeftLink: string;
    streamingNowTwoLeftProgram: string;
    streamingNowTwoLeftTitle: string;
    streamingNowTwoLeftUTM: string;
    streamingNowTwoRightAlt: string;
    streamingNowTwoRightDescription: string;
    streamingNowTwoRightImage: string;
    streamingNowTwoRightLink: string;
    streamingNowTwoRightProgram: string;
    streamingNowTwoRightTitle: string;
    streamingNowTwoRightUTM: string;
    superSubjectLink: string;
    templateSelected: number;
    textDescription: string;
    textDescriptionText: string;
    textDescriptionAddInterest: number;
    textDescriptionAddInterestConverted: string;
    textDescriptionContent: string;
    textDescriptionDisplay: boolean;
    textDescriptionImageAlt: string;
    textDescriptionImageSource: string;
    textDescriptionLink: string;
    textDescriptionSalutation: string;
    titleTextDisplay: boolean;
    titleTextText: string;
    titleTextTitle: string;
    topSingleColumnAirDate: string;
    topSingleColumnUTM: string;
    topSingleColumnDescription: string;
    topSingleColumnTitle: string;
    topSingleColumnImageAlt: string;
    topSingleColumnImageSource: string;
    topSingleColumnLink: string;
    topSingleColumnProgram: string;
    upcomingShowsColumnBottomDisplay: Boolean;
    upcomingShowsColumnBottomLeftAirDate: string;
    upcomingShowsColumnBottomLeftAlt: string;
    upcomingShowsColumnBottomLeftDescription: string;
    upcomingShowsColumnBottomLeftImage: string;
    upcomingShowsColumnBottomLeftLink: string;
    upcomingShowsColumnBottomLeftProgram: string;
    upcomingShowsColumnBottomLeftTitle: string;
    upcomingShowsColumnBottomLeftUTM: string;
    upcomingShowsColumnBottomRightAirDate: string;
    upcomingShowsColumnBottomRightAlt: string;
    upcomingShowsColumnBottomRightDescription: string;
    upcomingShowsColumnBottomRightImage: string;
    upcomingShowsColumnBottomRightLink: string;
    upcomingShowsColumnBottomRightProgram: string;
    upcomingShowsColumnBottomRightTitle: string;
    upcomingShowsColumnBottomRightUTM: string;
    upcomingShowsColumnTopLeftAirDate: string;
    upcomingShowsColumnTopLeftAlt: string;
    upcomingShowsColumnTopLeftDescription: string;
    upcomingShowsColumnTopLeftImage: string;
    upcomingShowsColumnTopLeftLink: string;
    upcomingShowsColumnTopLeftProgram: string;
    upcomingShowsColumnTopLeftTitle: string;
    upcomingShowsColumnTopLeftUTM: string;
    upcomingShowsColumnTopRightAirDate: string;
    upcomingShowsColumnTopRightAlt: string;
    upcomingShowsColumnTopRightDescription: string;
    upcomingShowsColumnTopRightImage: string;
    upcomingShowsColumnTopRightLink: string;
    upcomingShowsColumnTopRightProgram: string;
    upcomingShowsColumnTopRightTitle: string;
    upcomingShowsColumnTopRightUTM: string;
    upcomingShowsColumnTwoDisplay: boolean;
    upcomingShowsColumnTwoLeftAirDate: string;
    upcomingShowsColumnTwoLeftAlt: string;
    upcomingShowsColumnTwoLeftDescription: string;
    upcomingShowsColumnTwoLeftImage: string;
    upcomingShowsColumnTwoLeftLink: string;
    upcomingShowsColumnTwoLeftProgram: string;
    upcomingShowsColumnTwoLeftTitle: string;
    upcomingShowsColumnTwoLeftUTM: string;
    upcomingShowsColumnTwoRightAirDate: string;
    upcomingShowsColumnTwoRightAlt: string;
    upcomingShowsColumnTwoRightDescription: string;
    upcomingShowsColumnTwoRightImage: string;
    upcomingShowsColumnTwoRightLink: string;
    upcomingShowsColumnTwoRightProgram: string;
    upcomingShowsColumnTwoRightTitle: string;
    upcomingShowsColumnTwoRightUTM: string;
    upcomingShowsColumnThreeDisplay: boolean;
    upcomingShowsColumnThreeLeftAirDate: string;
    upcomingShowsColumnThreeLeftAlt: string;
    upcomingShowsColumnThreeLeftDescription: string;
    upcomingShowsColumnThreeLeftImage: string;
    upcomingShowsColumnThreeLeftLink: string;
    upcomingShowsColumnThreeLeftProgram: string;
    upcomingShowsColumnThreeLeftTitle: string;
    upcomingShowsColumnThreeLeftUTM: string;
    upcomingShowsColumnThreeRightAirDate: string;
    upcomingShowsColumnThreeRightAlt: string;
    upcomingShowsColumnThreeRightDescription: string;
    upcomingShowsColumnThreeRightImage: string;
    upcomingShowsColumnThreeRightLink: string;
    upcomingShowsColumnThreeRightProgram: string;
    upcomingShowsColumnThreeRightTitle: string;
    upcomingShowsColumnThreeRightUTM: string;
    upcomingShowsHeading: string;
    upcomingShowsHeadingDisplay: boolean;
    utmSource: string;
    utmMedium: string;
    utmCampaign: string;
}
