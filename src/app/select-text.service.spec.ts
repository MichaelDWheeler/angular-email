import { TestBed, inject } from '@angular/core/testing';

import { SelectTextService } from './select-text.service';

describe('SelectTextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectTextService]
    });
  });

  it('should be created', inject([SelectTextService], (service: SelectTextService) => {
    expect(service).toBeTruthy();
  }));
});
