import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamingNowBottomComponent } from './streaming-now-bottom.component';
import { FormsModule } from '@angular/forms';

describe('StreamingNowBottomComponent', () => {
  let component: StreamingNowBottomComponent;
  let fixture: ComponentFixture<StreamingNowBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreamingNowBottomComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamingNowBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
