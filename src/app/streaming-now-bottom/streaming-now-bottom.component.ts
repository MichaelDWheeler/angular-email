import { Component, OnInit, OnDestroy } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-streaming-now-bottom',
  templateUrl: './streaming-now-bottom.component.html',
  styleUrls: ['./streaming-now-bottom.component.css']
})
export class StreamingNowBottomComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = 'Bottom Streamin Now';
  streamingNowBottomDisplay: boolean;
  streamingNowBottomLeftLink: string;
  streamingNowBottomLeftUTM: string;
  streamingNowBottomLeftAlt: string;
  streamingNowBottomLeftImage: string;
  streamingNowBottomLeftProgram: string;
  streamingNowBottomLeftTitle: string;
  streamingNowBottomLeftDescription: string;
  streamingNowBottomRightLink: string;
  streamingNowBottomRightUTM: string;
  streamingNowBottomRightAlt: string;
  streamingNowBottomRightImage: string;
  streamingNowBottomRightProgram: string;
  streamingNowBottomRightTitle: string;
  streamingNowBottomRightDescription: string;
  subscription: Subscription;

  addVariables(): void {
    this.createTemplate.setVariables('streamingNowBottomLeftAlt', this.streamingNowBottomLeftProgram);
    this.createTemplate.setVariables('streamingNowBottomLeftUTM', this.streamingNowBottomLeftProgram);
    this.streamingNowBottomLeftAlt = this.email.streamingNowBottomLeftAlt;
    this.streamingNowBottomLeftUTM = this.email.streamingNowBottomLeftUTM;
    this.createTemplate.setVariables('streamingNowBottomRightAlt', this.streamingNowBottomRightProgram);
    this.createTemplate.setVariables('streamingNowBottomRightUTM', this.streamingNowBottomRightProgram);
    this.streamingNowBottomRightAlt = this.email.streamingNowBottomRightAlt;
    this.streamingNowBottomRightUTM = this.email.streamingNowBottomRightUTM;
    this.createTemplate.setStorage();
  }

  selectAllText($event: any): void {
    this.select.selectAllText($event);
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.streamingNowBottomDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void{
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.streamingNowBottomLeftUTM = this.email.streamingNowBottomLeftUTM;
        this.streamingNowBottomRightUTM = this.email.streamingNowBottomRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.streamingNowBottomDisplay;
    this.streamingNowBottomLeftLink = this.email.streamingNowBottomLeftLink;
    this.streamingNowBottomLeftUTM = this.email.streamingNowBottomLeftUTM;
    this.streamingNowBottomLeftAlt = this.email.streamingNowBottomLeftAlt;
    this.streamingNowBottomLeftImage = this.email.streamingNowBottomLeftImage;
    this.streamingNowBottomLeftProgram = this.email.streamingNowBottomLeftProgram;
    this.streamingNowBottomLeftTitle = this.email.streamingNowBottomLeftTitle;
    this.streamingNowBottomLeftDescription = this.email.streamingNowBottomLeftDescription;
    this.streamingNowBottomRightLink = this.email.streamingNowBottomRightLink;
    this.streamingNowBottomRightUTM = this.email.streamingNowBottomRightUTM;
    this.streamingNowBottomRightAlt = this.email.streamingNowBottomRightAlt;
    this.streamingNowBottomRightImage = this.email.streamingNowBottomRightImage;
    this.streamingNowBottomRightProgram = this.email.streamingNowBottomRightProgram;
    this.streamingNowBottomRightTitle = this.email.streamingNowBottomRightTitle;
    this.streamingNowBottomRightDescription = this.email.streamingNowBottomRightDescription;
    this.utmSubscription();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
