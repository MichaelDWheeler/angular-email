import { Injectable } from '@angular/core';
import { TEMPLATEVARIABLES } from './data-create-template';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreateTemplateService {
  templateVariables = TEMPLATEVARIABLES;
  templateDefault: any = {};
  d: any;
  email: any = {};
  heading: string;
  key: any;
  month = {
    'Jan': 'January',
    'Feb': 'February',
    'Mar': 'March',
    'Apr': 'April',
    'May': 'May',
    'Jun': 'June',
    'Jul': 'July',
    'Aug': 'August',
    'Sep': 'September',
    'Oct': 'October',
    'Nov': 'November',
    'Dec': 'December'
  };
  value: string;
  utmSource: any;

  private _subject = new Subject<any>();

  triggerUTMChange(value: boolean) {
    this._subject.next({ change: value})
  }

  getMessage(): Observable<any> {
    return this._subject.asObservable();
  }

  allStorage(): any {
    let archive: any = {};
    const keys = Object.keys(localStorage);
    let i: number = keys.length;

    while (i--) {
      if (keys[i] === 'dataEnjoyLinks' || keys[i] === 'adProviders' || keys[i] === 'littleMoreLinks') {
        archive[keys[i]] = JSON.parse(localStorage.getItem(keys[i]));
      } else {
        archive[keys[i]] = localStorage.getItem(keys[i]);
      }
    }
    archive = this.restoreDataType(archive);
    return archive;
  }

  changeParagraphTags($event: any): string {
    let str = $event.target.value;
    str = str.replace(/<p>/g, '<br style="display: block;content:\'\';margin-top: 14px;">');
    str = str.replace(/<\/p>/g, '<br><br style="display: block;content:\'\';margin-bottom: 14px;">');
    str = str.replace(/<br><br>/g, '<br><br style="display: block;content:\'\';margin-top: 14px;">');
    return str;
  }

  changeUTM(key: string): string {
    return this.templateVariables[key];
  }

  changeVariables(key: string, value: string): any {
    value = value.trim();
    this.setVariables(key, value);
    this.checkUTMsForDuplicates();
    this.setStorage();
  }

  checkUTMsForDuplicates(): void {
    const utmCollection: any[] = [
      'bottomSingleColumnUTM',
      'comingSoonBottomLeftUTM',
      'comingSoonBottomRightUTM',
      'comingSoonTopLeftUTM',
      'comingSoonTopRightUTM',
      'comingSoonTwoLeftUTM',
      'comingSoonTwoRightUTM',
      'comingSoonThreeLeftUTM',
      'comingSoonThreeRightUTM',
      'comingSoonFourLeftUTM',
      'comingSoonFourRightUTM',
      'extraSingleColumnUTM',
      'littleMoreUTM',
      'monthUTM',
      'partnerSpotlightUTM',
      'partnerSpotlightTwoUTM',
      'passportAdUTM',
      'passportAdHeaderUTM',
      'streamingNowBottomLeftUTM',
      'streamingNowBottomRightUTM',
      'streamingNowTopLeftUTM',
      'streamingNowTopRightUTM',
      'streamingNowTwoLeftUTM',
      'streamingNowTwoRightUTM',
      'topSingleColumnUTM',
      'upcomingShowsColumnBottomLeftUTM',
      'upcomingShowsColumnBottomRightUTM',
      'upcomingShowsColumnTopLeftUTM',
      'upcomingShowsColumnTopRightUTM',
      'upcomingShowsColumnTwoLeftUTM',
      'upcomingShowsColumnTwoRightUTM',
      'upcomingShowsColumnThreeLeftUTM',
      'upcomingShowsColumnThreeRightUTM',
    ];

    utmCollection.forEach((element) => {
      utmCollection.forEach((comparedElement) => {
        if (element !== comparedElement) { //if element is not the same element being compared
          if (this.templateVariables[element] === this.templateVariables[comparedElement] && this.templateVariables[element] !== '' && this.templateVariables[comparedElement] !== '') {
            const streaming = this.templateVariables[comparedElement].includes('streaming-') ? 'streaming-' : '';
            const elementPrefix = element.split('UTM')[0];
            const elementProgram = elementPrefix + 'Program';
            const elementTitle = elementPrefix + 'Title';
            const firstWord = this.templateVariables[elementProgram] + '%20' + this.templateVariables[elementTitle];
            this.templateVariables[element] = this.removeSpaces(firstWord);
            const comparedElementPrefix = comparedElement.split('UTM')[0];
            const comparedElementProgram = comparedElementPrefix + 'Program';
            const comparedElementTitle = comparedElementPrefix + 'Title';
            const secondWord = streaming + this.templateVariables[comparedElementProgram] + '%20' + this.templateVariables[comparedElementTitle];
            this.templateVariables[comparedElement] = this.removeSpaces(secondWord);
          }
        }
      });
    });
  }

  checkSubDomain(): string {
    const location: any = window.location.href;
    const array = location.split('/');
    const index = array.length - 1;
    const subdomain = array[index];
    return subdomain;
  }

  checkTemplateSelected(): number {
    const subdomain = this.checkSubDomain();

    if (subdomain === 'program-highlights') {
      if (this.templateVariables.utmCampaign === '' || this.templateVariables.utmCampaign === 'membership' || this.templateVariables.utmCampaign === 'passport' || this.templateVariables.utmCampaign === 'dedicated') {
        this.templateVariables.utmCampaign = 'program%20highlights';
        this.templateVariables.templateSelected = 3;
      }
      return 3;
    } else if (subdomain === 'membership') {
      if (this.templateVariables.utmCampaign === '' || this.templateVariables.utmCampaign === 'program%20highlights' || this.templateVariables.utmCampaign === 'passport' || this.templateVariables.utmCampaign === 'dedicated') {
        this.templateVariables.utmCampaign = 'membership';
        this.templateVariables.templateSelected = 1;
      }
      return 1;
    } else if (subdomain === 'passport') {
      if (this.templateVariables.utmCampaign === '' || this.templateVariables.utmCampaign === 'program%20highlights' || this.templateVariables.utmCampaign === 'membership' || this.templateVariables.utmCampaign === 'dedicated') {
        this.templateVariables.utmCampaign = 'passport';
        this.templateVariables.templateSelected = 2;
      }
      return 2;
    } else if (subdomain === 'dedicated') {
      if (this.templateVariables.utmCampaign === '' || this.templateVariables.utmCampaign === 'program%20highlights' || this.templateVariables.utmCampaign === 'membership' || this.templateVariables.utmCampaign === 'passport') {
        this.templateVariables.utmCampaign = 'dedicated';
        this.templateVariables.templateSelected = 0;
        this.templateVariables.partnerSpotlightImageSrc = 'https://via.placeholder.com/1200x675';
        this.templateVariables.partnerSpotlightTwoImageSrc = 'https://via.placeholder.com/1200x675';
      }
      return 0;
    } else if (this.templateVariables.emailTemplate === 'Program Highlights' || this.templateVariables.templateSelected === 3) {
      this.templateVariables.templateSelected = 3;
      return 3;
    } else if (this.templateVariables.emailTemplate === 'Membership' || this.templateVariables.templateSelected === 1) {
      this.templateVariables.templateSelected = 1;
      return 1;
    } else if (this.templateVariables.emailTemplate === 'Passport' || this.templateVariables.templateSelected === 2) {
      this.templateVariables.templateSelected = 2;
      return 2;
    } else if (this.templateVariables.emailTemplate === 'Dedicated' || this.templateVariables.templateSelected === 0) {
      this.templateVariables.templateSelected = 0;
      return 0;
    } else {
      this.templateVariables.templateSelected = null;
      return null;
    }
  }

  checkVariableFormat(key, value) {
    this.key = key;
    this.value = value;

    const keyArray = [
      'bottomSingleColumnUTM',
      'comingSoonBottomLeftUTM',
      'comingSoonBottomRightUTM',
      'comingSoonTopLeftUTM',
      'comingSoonTopRightUTM',
      'comingSoonTwoLeftUTM',
      'comingSoonTwoRightUTM',
      'comingSoonThreeLeftUTM',
      'comingSoonThreeRightUTM',
      'comingSoonFourLeftUTM',
      'comingSoonFourRightUTM',
      'extraSingleColumnUTM',
      'littleMoreUTM',
      'monthUTM',
      'partnerSpotlightUTM',
      'partnerSpotlightTwoUTM',
      'passportAdUTM',
      'passportAdHeaderUTM',
      'streamingNowBottomLeftUTM',
      'streamingNowBottomRightUTM',
      'streamingNowTopLeftUTM',
      'streamingNowTopRightUTM',
      'streamingNowTwoLeftUTM',
      'streamingNowTwoRightUTM',
      'topSingleColumnUTM',
      'upcomingShowsColumnBottomLeftUTM',
      'upcomingShowsColumnBottomRightUTM',
      'upcomingShowsColumnTopLeftUTM',
      'upcomingShowsColumnTopRightUTM',
      'upcomingShowsColumnTwoLeftUTM',
      'upcomingShowsColumnTwoRightUTM',
      'upcomingShowsColumnThreeLeftUTM',
      'upcomingShowsColumnThreeRightUTM',
      'utmCampaign',
      'singleColumnContent'
    ];

    for (let i = 0, length = keyArray.length; i < length; i++) {
      let str;
      if (key === keyArray[i]) {
        str = value.replace(/[^\w\s]/gi, '');
        str = str.replace(/ /g, '%20');
        this.templateVariables[key] = str;
      }
    }
  }

  clearLocalStorageWarning() {
    const data = Object.keys(localStorage);
    if (data.length > 0) {
      const warningMessage = confirm('This will clear your storage and you will start with a new template. Do you want to proceed?');
      return warningMessage;
    } else {
      return true;
    }

  }

  clearStorage(): any {
    localStorage.clear();
    location.reload();
  }

  formatDate(date): string {
    const d = new Date(date).toLocaleDateString();
    const dateArray = d.split('/');
    const dateString = dateArray[2] + '-' + dateArray[0] + '-' + dateArray[1];
    return dateString;
  }

  formatUTMSource(value) {
    let str = value.toString().replace(/[^\w\s]/gi, '');
    str = str.replace(/ /g, '%20');
    return str;
  }

  getDate(): void {
    if (this.templateVariables.utmSource === '') {
      const d = this.nextSaturday(),
        dd = d.getDate();
      let ddString = String(dd);
      ddString = this.makeTwoDigit(ddString);
      const mm = d.getMonth() + 1;
      let mmString = String(mm);
      mmString = this.makeTwoDigit(mmString);
      const yyyy = d.getFullYear();
      const dateString = `${yyyy}-${mmString}-${ddString}`;
      this.utmSource = dateString;
      this.setVariables('utmSource', this.utmSource);
    } else {
      return;
    }
  }

  getFutureDate(): string {
    if (this.templateVariables.monthTitle === '') {
      this.d = this.nextSaturday();
      this.d = this.d.toString().split(' ')[1];
      this.heading = this.month[this.d] + ' fun around SoCal';
      this.setVariables('monthTitle', this.heading);
      this.setVariables('monthAlt', this.month[this.d] + ' FUN');
      this.setVariables('monthUTM', this.heading);
      this.setVariables('littleMoreTitle', 'Most Streamed in ' + this.month[this.d]);
      return this.heading;
    } else {
      return this.templateVariables.monthTitle;
    }
  }

  getLinks(component): any {
    if (component === 'also enjoy') {
      return this.templateVariables.dataEnjoyLinks;
    }
    if (component === 'little more') {
      return this.templateVariables.littleMoreLinks;
    }
  }

  getStorage() {
    const data = Object.keys(localStorage);
    let storedVariables: any = {};
    this.templateVariables.monthTitle = this.getFutureDate();
    this.templateDefault = Object.assign({}, this.templateVariables);
    if (data.length !== 0) {
      storedVariables = this.allStorage();
      this.templateVariables = this.mapLocalStorage(storedVariables, this.templateVariables);
      this.templateVariables.localStorage = true;
    }
    return this.templateVariables;
  }

  getSupporters() {
    return this.templateVariables.adProviders;
  }

  getVariables() {
    return this.getStorage();
  }

  mapLocalStorage(storedVariables: any, obj: any): any {
    for (const item in obj) {
      if (obj.hasOwnProperty(item)) {
        if (obj[item] !== storedVariables[item] && item !== 'showControls') {
          obj[item] = storedVariables[item];
        }
      }
    }
    return obj;
  }

  makeTwoDigit(numString): string {
    if (numString.length !== 2) {
      return `0${numString}`;
    } else {
      return numString;
    }
  }

  nextSaturday() {
    const d: Date = new Date();
    const currentDay = d.getDay();
    const daysUntilSaturday = 6 - currentDay;
    d.setDate(d.getDate() + daysUntilSaturday);
    return d;
  }

  removeSpaces(value): string {
    if(value.endsWith('%20')){
      value = value.replace(new RegExp('%20' + '$'), '');
    }
    const str = value.replace(/ /g, '%20');
    return str;
  }

  restoreDataType(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const isNum = /^\d+$/.test(obj[key]);
        if (isNum) {
          obj[key] = parseInt(obj[key], 10);
        } else if (obj[key] === 'true') {
          obj[key] = true;
        } else if (obj[key] === 'false') {
          obj[key] = false;
        } else if (obj[key] === 'undefined') {
          obj[key] = undefined;
        }
      }
    }
    return obj;
  }

  setStorage() {
    for (const key in this.templateVariables) {
      if (this.templateVariables.hasOwnProperty(key)) {
        if (typeof (this.templateVariables[key]) === 'object') {
          localStorage.setItem(key, JSON.stringify(this.templateVariables[key]));
        } else {
          localStorage.setItem(key, this.templateVariables[key]);
        }
      }
    }
  }

  setVariables(key, value) {
    if (typeof (value) === 'string') {
      value = value.trim();
    }
    this.templateVariables[key] = value;
    this.checkVariableFormat(key, value);
  }

  constructor() { }
}
