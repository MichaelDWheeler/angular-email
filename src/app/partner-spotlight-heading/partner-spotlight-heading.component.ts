import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-partner-spotlight-heading',
  templateUrl: './partner-spotlight-heading.component.html',
  styleUrls: ['./partner-spotlight-heading.component.css'],
})
export class PartnerSpotlightHeadingComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'heading';
  partnerSpotlightHeading: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.partnerSpotlightHeadingDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public select: SelectTextService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.partnerSpotlightHeadingDisplay;
    this.partnerSpotlightHeading = this.email.partnerSpotlightHeading;
  }

}
