import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { PartnerSpotlightHeadingComponent } from './partner-spotlight-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('PartnerSpotlightHeadingComponent', () => {
  let component: PartnerSpotlightHeadingComponent;
  let fixture: ComponentFixture<PartnerSpotlightHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PartnerSpotlightHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [ FormsModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerSpotlightHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });
});
