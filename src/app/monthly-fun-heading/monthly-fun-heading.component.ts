import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-monthly-fun-heading',
  templateUrl: './monthly-fun-heading.component.html',
  styleUrls: ['./monthly-fun-heading.component.css']
})
export class MonthlyFunHeadingComponent implements OnInit {
  email: any;
  component = "Monthly Fun";
  showComponent: boolean;
  monthAlt: string;
  monthTitle: string;
  monthUTM: string;

  addVariables():void {
    this.createTemplate.setVariables('monthAlt', this.monthTitle);
    this.createTemplate.setVariables('monthUTM', this.monthTitle);
    this.createTemplate.changeVariables('monthTitle', this.monthTitle);
    this.monthAlt = this.email.monthAlt;
    this.monthTitle = this.email.monthTitle;
    this.monthUTM = this.email.monthUTM;
  }

  reset(): void {
    this.email.monthTitle = '';
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.monthDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.monthDisplay;
    this.monthAlt = this.email.monthAlt;
    this.monthTitle = this.email.monthTitle;
    this.monthUTM = this.email.monthUTM;
  }

}
