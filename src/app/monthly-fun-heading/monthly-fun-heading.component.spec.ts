import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { MonthlyFunHeadingComponent } from './monthly-fun-heading.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('MonthlyFunHeadingComponent', () => {
  let component: MonthlyFunHeadingComponent;
  let fixture: ComponentFixture<MonthlyFunHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MonthlyFunHeadingComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyFunHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set monthAlt and monthUTM values to "Test Month Title" and "Test%20Month%20Title" respectively', () => { 
    component.email.monthTitle = 'Test Month Title';
    component.addVariables();
    expect(component.email.monthAlt).toBe('Test Month Title');
    expect(component.email.monthUTM).toBe('Test%20Month%20Title');
  });

  it('should change monthTitle value to an empty string', ()=>{
    component.reset();
    expect(component.email.monthTitle).toBe('');
  });
});
