import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingShowsColumnsBottomComponent } from './upcoming-shows-columns-bottom.component';
import { FormsModule } from '@angular/forms';

describe('UpcomingShowsColumnsBottomComponent', () => {
  let component: UpcomingShowsColumnsBottomComponent;
  let fixture: ComponentFixture<UpcomingShowsColumnsBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpcomingShowsColumnsBottomComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingShowsColumnsBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
