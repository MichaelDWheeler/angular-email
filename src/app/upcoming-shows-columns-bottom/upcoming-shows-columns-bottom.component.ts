import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CreateTemplateService } from '../create-template.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-upcoming-shows-columns-bottom',
  templateUrl: './upcoming-shows-columns-bottom.component.html',
  styleUrls: ['./upcoming-shows-columns-bottom.component.css']
})
export class UpcomingShowsColumnsBottomComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = '4th Watch Now';
  upcomingShowsColumnBottomLeftLink: string;
  upcomingShowsColumnBottomLeftUTM: string;
  upcomingShowsColumnBottomLeftAlt: string;
  upcomingShowsColumnBottomLeftImage: string;
  upcomingShowsColumnBottomLeftProgram: string;
  upcomingShowsColumnBottomLeftTitle: string;
  upcomingShowsColumnBottomLeftAirDate: string;
  upcomingShowsColumnBottomLeftDescription: string;
  upcomingShowsColumnBottomRightLink: string;
  upcomingShowsColumnBottomRightUTM: string;
  upcomingShowsColumnBottomRightAlt: string;
  upcomingShowsColumnBottomRightImage: string;
  upcomingShowsColumnBottomRightProgram: string;
  upcomingShowsColumnBottomRightTitle: string;
  upcomingShowsColumnBottomRightAirDate: string;
  upcomingShowsColumnBottomRightDescription: string;
  subscription: Subscription;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.upcomingShowsColumnBottomDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  addVariables(): void {
    this.createTemplate.setVariables('upcomingShowsColumnBottomLeftAlt', this.upcomingShowsColumnBottomLeftProgram);
    this.createTemplate.setVariables('upcomingShowsColumnBottomLeftUTM', this.upcomingShowsColumnBottomLeftProgram);
    this.upcomingShowsColumnBottomLeftAlt = this.email.upcomingShowsColumnBottomLeftAlt;
    this.upcomingShowsColumnBottomLeftUTM = this.email.upcomingShowsColumnBottomLeftUTM;
    this.createTemplate.setVariables('upcomingShowsColumnBottomRightAlt', this.upcomingShowsColumnBottomRightProgram);
    this.createTemplate.setVariables('upcomingShowsColumnBottomRightUTM', this.upcomingShowsColumnBottomRightProgram);
    this.upcomingShowsColumnBottomRightAlt = this.email.upcomingShowsColumnBottomRightAlt;
    this.upcomingShowsColumnBottomRightUTM = this.email.upcomingShowsColumnBottomRightUTM;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void {
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if (message) {
        this.upcomingShowsColumnBottomLeftUTM = this.email.upcomingShowsColumnBottomLeftUTM;
        this.upcomingShowsColumnBottomRightUTM = this.email.upcomingShowsColumnBottomRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.upcomingShowsColumnBottomDisplay;
    this.upcomingShowsColumnBottomLeftLink = this.email.upcomingShowsColumnBottomLeftLink;
    this.upcomingShowsColumnBottomLeftUTM = this.email.upcomingShowsColumnBottomLeftUTM;
    this.upcomingShowsColumnBottomLeftAlt = this.email.upcomingShowsColumnBottomLeftAlt;
    this.upcomingShowsColumnBottomLeftImage = this.email.upcomingShowsColumnBottomLeftImage;
    this.upcomingShowsColumnBottomLeftProgram = this.email.upcomingShowsColumnBottomLeftProgram;
    this.upcomingShowsColumnBottomLeftTitle = this.email.upcomingShowsColumnBottomLeftTitle;
    this.upcomingShowsColumnBottomLeftAirDate = this.email.upcomingShowsColumnBottomLeftAirDate;
    this.upcomingShowsColumnBottomLeftDescription = this.email.upcomingShowsColumnBottomLeftDescription;
    this.upcomingShowsColumnBottomRightLink = this.email.upcomingShowsColumnBottomRightLink;
    this.upcomingShowsColumnBottomRightUTM = this.email.upcomingShowsColumnBottomRightUTM;
    this.upcomingShowsColumnBottomRightAlt = this.email.upcomingShowsColumnBottomRightAlt;
    this.upcomingShowsColumnBottomRightImage = this.email.upcomingShowsColumnBottomRightImage;
    this.upcomingShowsColumnBottomRightProgram = this.email.upcomingShowsColumnBottomRightProgram;
    this.upcomingShowsColumnBottomRightTitle = this.email.upcomingShowsColumnBottomRightTitle;
    this.upcomingShowsColumnBottomRightAirDate = this.email.upcomingShowsColumnBottomRightAirDate;
    this.upcomingShowsColumnBottomRightDescription = this.email.upcomingShowsColumnBottomRightDescription;
    this.utmSubscription();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

}
