import { TestBed, inject } from '@angular/core/testing';

import { TemplateSelectService } from './template-select.service';

describe('TemplateSelectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TemplateSelectService]
    });
  });

  it('should be created', inject([TemplateSelectService], (service: TemplateSelectService) => {
    expect(service).toBeTruthy();
  }));
});
