import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AddInterestFullWidthComponent } from './add-interest-full-width.component';

describe('AddInterestFullWidthComponent', () => {
  let component: AddInterestFullWidthComponent;
  let fixture: ComponentFixture<AddInterestFullWidthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInterestFullWidthComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInterestFullWidthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (async((done) => {
    expect(component).toBeTruthy();
  })));

  it('should return { order: 1, id: 1421, category: \'--Antiquarians\'}', () =>{
    expect(component.addInterest[2]).toEqual({ order: 1, id: 1421, category: '--Antiquarians' });
  });
});
