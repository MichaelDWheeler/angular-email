import { Component, OnInit, Input } from '@angular/core';
import { AddInterestService } from '../add-interest.service';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-add-interest-full-width',
  templateUrl: './add-interest-full-width.component.html',
  styleUrls: ['./add-interest-full-width.component.css']
})
export class AddInterestFullWidthComponent implements OnInit {
  @Input() addComponent: string;
  email: any = {};
  addInterest: any;
  interestId: string;

  constructor(
    public interest: AddInterestService,
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.addInterest = this.interest.getAddInterests();
    this.email = this.createTemplate.getVariables();
  }

}
