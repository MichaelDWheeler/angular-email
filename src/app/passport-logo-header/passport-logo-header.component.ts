import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-passport-logo-header',
  templateUrl: './passport-logo-header.component.html',
  styleUrls: ['./passport-logo-header.component.css']
})
export class PassportLogoHeaderComponent implements OnInit {
  email: any = {};
  component = 'Banner Ad';
  showComponent: boolean;
  passportAdHeaderImage: string;
  passportAdHeaderAlt: string;
  passportAdHeaderLink: string;
  passportAdHeaderUTM: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.passportAdDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.passportAdDisplay;
    this.passportAdHeaderImage = this.email.passportAdHeaderImage;
    this.passportAdHeaderAlt = this.email.passportAdHeaderAlt;
    this.passportAdHeaderLink = this.email.passportAdHeaderLink;
    this.passportAdHeaderUTM = this.email.passportAdHeaderUTM;
  }

}
