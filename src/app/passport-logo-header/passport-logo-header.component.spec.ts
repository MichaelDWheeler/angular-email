import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassportLogoHeaderComponent } from './passport-logo-header.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('PassportLogoHeaderComponent', () => {
  let component: PassportLogoHeaderComponent;
  let fixture: ComponentFixture<PassportLogoHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PassportLogoHeaderComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassportLogoHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
