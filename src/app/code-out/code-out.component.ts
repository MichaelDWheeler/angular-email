import { Component, OnInit } from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { CheckEmailFieldsService } from '../check-email-fields.service';
import { CreateTemplateService } from '../create-template.service';


@Component({
  selector: 'app-code-out',
  templateUrl: './code-out.component.html',
  styleUrls: ['./code-out.component.css']
})
export class CodeOutComponent implements OnInit {
  code = '';
  bodyCode: string;
  emailCode: string;
  templateSelected: Number;

  format(node: any, level: number) {
    const indentBefore = new Array(level++ + 1).join('  '),
      indentAfter = new Array(level - 1).join('  ');
    let textNode: Text;
    for (let i = 0; i < node.children.length; i++) {
      textNode = document.createTextNode('\n' + indentBefore);
      if (node.innerHTML.includes('skip-this-node') && level === 17) {
        node.insertBefore(textNode, node.children[i]);
      } else {
        node.insertBefore(textNode, node.children[i]);
        this.format(node.children[i], level);
      }
      if (node.lastElementChild === node.children[i]) {
        textNode = document.createTextNode('\n' + indentAfter);
        node.appendChild(textNode);
      }
    }
    return node;
  }

  process(): string {
    const div = document.createElement('div');
    div.innerHTML = this.code.trim();
    div.innerHTML = div.innerHTML.replace(/<!---->/g,'');
    div.innerHTML = div.innerHTML.replace(/<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full relative height-49" width="100%"><tbody><tr><td align="center" valign="top" width="100%"><table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full relative" width="100%"><\/table><\/td><\/tr><\/tbody><\/table>/g, '');
    div.innerHTML = div.innerHTML.replace(/<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="full relative height-49" width="100%"><tbody><tr><td align="center" valign="top" width="100%"><table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full relative" width="100%"><\/table><\/td><\/tr><\/tbody><\/table>/g, '');
    div.innerHTML = div.innerHTML.replace(/<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="full" width="100%"><tbody><\/tbody><\/table>/g, '');
    div.innerHTML = div.innerHTML.replace(/<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="full relative" width="100%"><tbody><\/tbody><\/table>/g, '');
    return this.format(div, 1).innerHTML;
  }
  
  getEmailHTML(): string {
    this.code += document.body.outerHTML;
    this.code = this.code.replace(/_nghost-(\w*)=""/g, '');
    this.code = this.code.replace(/ng-version="\d.\d.\d"/g, '');
    this.code = this.code.replace(/_ngcontent-(\w*)=""/g, '');
    this.code = this.code.replace(/<!--bindings={/g, '');
    this.code = this.code.replace(/}-->/g, '');
    this.code = this.code.replace(/ng-reflect-model="(.*?)"/g, '');
    this.code = this.code.replace(/"ng-reflect-ng-if": "(.*?)"/g, '');
    this.code = this.code.replace(/"ng-reflect-ng-if": "true"/g, '');
    this.code = this.code.replace(/"ng-reflect-ng-if": "false"/g, '');
    this.code = this.code.replace(/<router-outlet.*><\/router-outlet>/, '');
    this.code = this.code.replace(/"ng-reflect-ng-for-of": /g, '');
    this.code = this.code.replace(/"\[object Object]"/g, '');
    this.code = this.code.replace(/"\[object Object],\[object Object"/g, '');
    this.code = this.code.replace(/<app-(.*?)>/g, '');
    this.code = this.code.replace(/\r?\n|\r/g, '');
    this.code = this.code.replace(/> *</g, '><');
    this.code = this.code.replace(/<code>/, '');
    this.code = this.code.replace(/<table  align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full" id="code-out" width="1200">(.*)<\/script>/, '');
    this.code = this.code.replace(/  /g, ' ');
    this.code = this.code.replace(/ *>/g, '>');
    this.code = this.code.replace(/<button *.*?>.*?<\/button>/g, '');
    this.code = this.code.replace(/<tr class="destroy".*?<\/tr>/g, '');
    this.code = this.code.replace(/ ""/g, '');
    this.code = this.code.replace(/<div class="controls">.<\/div>/g, '');
    this.code = this.code.replace(/<br><br>/g, '<br><br style="display: block;content:\'\';margin-top: 15px;">');
    this.code = this.code.replace('<img alt="PBS SoCal Logo" border="0" src="http://dj3b4v4vfrnun.cloudfront.net/wp-content/themes/pbs-socal/libs/images/pbssocal-animated-logo.gif" width="100">', '<!--[if gte mso 7]><img width="100" src="https://www.pbssocal.org/wp-content/uploads/2018/07/pbs-socal-logo.jpg" alt="PBS SoCal Logo" border="0"><![endif]--><img alt="PBS SoCal Logo" border="0" src="http://dj3b4v4vfrnun.cloudfront.net/wp-content/themes/pbs-socal/libs/images/pbssocal-animated-logo.gif" width="100">');
    this.code = this.code.replace(/<code>.*<\/script>/, '');
    this.code = this.code.replace(/display: block;cont"/g, '');
    this.code = this.code.replace(/<br class="destroy">/g, '');
    this.code = this.process();
    this.code += '</body>';
    this.bodyCode = '<body style="padding:0;">' + this.code + '\n</html>';
    this.code = this.bodyCode;
    return this.code;
  }

  constructor(
    public selectText: SelectTextService,
    public checkFields: CheckEmailFieldsService,
    public createTemplate: CreateTemplateService,

  ) { }

  ngOnInit() {
    this.code = this.getEmailHTML();
  }

}
