import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { CodeOutComponent } from './code-out.component';
import { CreateTemplateService } from '../create-template.service';

describe('CodeOutComponent', () => {
  let component: CodeOutComponent;
  let fixture: ComponentFixture<CodeOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodeOutComponent],
      providers: [CreateTemplateService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return nested html code', () => {
    const div = document.createElement('div');
    div.innerHTML = '<div id="1"><div id="2"></div></div>';
    expect(component.format(div, 1).innerHTML).toContain(`<div id="2"></div>`);
  });

  it('should remove empty tables', () => {
    component.code = '<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full relative height-49" width="100%"><tbody><tr><td align="center" valign="top" width="100%"><table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full relative" width="100%"></table></td></tr></tbody></table><table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="full relative height-49" width="100%"><tbody><tr><td align="center" valign="top" width="100%"><table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="full relative" width="100%"></table></td></tr></tbody></table><table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="full" width="100%"><tbody></tbody></table><table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="full relative" width="100%"><tbody></tbody></table>';
    expect(component.process()).toEqual('');
  });

  it('should return code', () => {
    component.templateSelected = 0;
    component.checkTemplate();
    expect(component.code).toContain('<body>');
  });

  it('should return html code with ending html tag', () => {
    expect(component.getEmailHTML()).toContain('</html>');
  });

  it('should equal document html', () => {
    expect(component.code).toContain('\n</html>');
  });

  it('should return "program-highlights" and template selected should return as 0', inject([CreateTemplateService], (createTemplate: CreateTemplateService) => {
    const templateVariables = createTemplate.getVariables();
    let mynamespace;
    mynamespace = mynamespace || {};
    mynamespace.util = (
      function () {
        function getWindowLocationHRef() {
          return window.location.href;
        }
      return {
        getWindowLocationHRef: getWindowLocationHRef
      };
    })();

    mynamespace.util.getWindowLocationHRef = function () {
      return 'http://localhost:4200/program-highlights'
    };

    mynamespace.util.checkSubdomain = function (): string {
      const location: any = mynamespace.util.getWindowLocationHRef();
      const array = location.split('/');
      const index = array.length - 1;
      const subdomain = array[index];
      return subdomain;
    };

    mynamespace.util.checkTemplateSelected = (): number => {
      const subdomain = mynamespace.util.checkSubdomain();
      if (subdomain === 'program-highlights') {
        if (templateVariables.utmCampaign === '' || templateVariables.utmCampaign === 'membership%20messages') {
          templateVariables.utmCampaign = 'program%20highlights';
        }
        return 0;
      } else if (subdomain === 'membership-messages') {
        if (templateVariables.utmCampaign === '' || templateVariables.utmCampaign === 'program%20highlights') {
          templateVariables.utmCampaign = 'membership%20messages';
        }
        return 1;
      } else if (templateVariables.emailTemplate === '' || templateVariables.emailTemplate === undefined) {
        return;
      } else if (templateVariables.emailTemplate === 'Program Highlights') {
        return 0;
      } else if (templateVariables.emailTemplate === 'Membership Messages') {
        templateVariables.templateSelected = 1;
        return 1;
      } else {
        return 0;
      }
    };

    expect(mynamespace.util.checkSubdomain()).toEqual('program-highlights');
    expect(mynamespace.util.checkTemplateSelected()).toEqual(0);
  }));
});
