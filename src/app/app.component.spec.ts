import { TestBed, async } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ProgramHighlightTemplateComponent } from './program-highlight-template/program-highlight-template.component';
import { MembershipMessagesComponent } from './membership-messages/membership-messages.component';
import { TopSingleColumnComponent } from './top-single-column/top-single-column.component';
import { HeaderComponent } from './header/header.component';
import { UpcomingShowsColumnsTopComponent } from './upcoming-shows-columns-top/upcoming-shows-columns-top.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { WhatsOnComponent } from './whats-on/whats-on.component';
import { GlobalUtmComponent } from './global-utm/global-utm.component';
import { UpcomingShowsColumnsBottomComponent } from './upcoming-shows-columns-bottom/upcoming-shows-columns-bottom.component';
import { AlsoEnjoyComponent } from './also-enjoy/also-enjoy.component';
import { AlsoEnjoyLinksComponent } from './also-enjoy-links/also-enjoy-links.component';
import { StreamingNowHeaderComponent } from './streaming-now-header/streaming-now-header.component';
import { StreamingNowTopComponent } from './streaming-now-top/streaming-now-top.component';
import { StreamingNowBottomComponent } from './streaming-now-bottom/streaming-now-bottom.component';
import { PassportLogoHeaderComponent } from './passport-logo-header/passport-logo-header.component';
import { PassportAdComponent } from './passport-ad/passport-ad.component';
import { MonthlyFunHeadingComponent } from './monthly-fun-heading/monthly-fun-heading.component';
import { MonthlyFunSectionComponent } from './monthly-fun-section/monthly-fun-section.component';
import { BottomSingleColumnHeadingComponent } from './bottom-single-column-heading/bottom-single-column-heading.component';
import { BottomSingleColumnSectionComponent } from './bottom-single-column-section/bottom-single-column-section.component';
import { ProvidingSupportHeadingComponent } from './providing-support-heading/providing-support-heading.component';
import { ProvidingSupportAdsComponent } from './providing-support-ads/providing-support-ads.component';
import { EmailOutputComponent } from './email-output/email-output.component';
import { CodeOutComponent } from './code-out/code-out.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { ExtraSingleColumnComponent } from './extra-single-column/extra-single-column.component';
import { ExtraSingleColumnHeadingComponent } from './extra-single-column-heading/extra-single-column-heading.component';
import { EmailHeadingComponent } from './email-heading/email-heading.component';
import { TextDescriptionComponent } from './text-description/text-description.component';
import { UpcomingShowsHeadingComponent } from './upcoming-shows-heading/upcoming-shows-heading.component';
import { SingleColumnHeadingComponent } from './single-column-heading/single-column-heading.component';
import { SingleColumnContentComponent } from './single-column-content/single-column-content.component';
import { ComingSoonTopComponent } from './coming-soon-top/coming-soon-top.component';
import { ComingSoonBottomComponent } from './coming-soon-bottom/coming-soon-bottom.component';
import { PassportLearnMoreComponent } from './passport-learn-more/passport-learn-more.component';
import { SingleImageSectionComponent } from './single-image-section/single-image-section.component';
import { PartnerSpotlightComponent } from './partner-spotlight/partner-spotlight.component';
import { PassportLearnMoreHeadingComponent } from './passport-learn-more-heading/passport-learn-more-heading.component';
import { PartnerSpotlightHeadingComponent } from './partner-spotlight-heading/partner-spotlight-heading.component';
import { partnerSpotlightTwoHeadingComponent } from './partner-spotlight-heading-two/partner-spotlight-heading-two.component';
import { PartnerSpotlightTwoComponent } from './partner-spotlight-two/partner-spotlight-two.component';
import { RemoveButtonComponent } from './remove-button/remove-button.component';
import { RestoreButtonComponent } from './restore-button/restore-button.component';
import { DividerComponent } from './divider/divider.component';
import { ContentImageWrapComponent } from './content-image-wrap/content-image-wrap.component';
import { ContentImageWrapHeadingComponent } from './content-image-wrap-heading/content-image-wrap-heading.component';
import { LittleMoreHeadingComponent } from './little-more-heading/little-more-heading.component';
import { LittleMoreComponent } from './little-more/little-more.component';
import { AddInterestComponent } from './add-interest/add-interest.component';
import { AddInterestFullWidthComponent } from './add-interest-full-width/add-interest-full-width.component';
import { APP_BASE_HREF } from '@angular/common';

const routes: Routes = [
  { path: 'program-highlights', component: ProgramHighlightTemplateComponent },
  { path: 'membership-messages', component: MembershipMessagesComponent }
];

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        AppComponent,
        TopSingleColumnComponent,
        HeaderComponent,
        UpcomingShowsColumnsTopComponent,
        ContentComponent,
        FooterComponent,
        WhatsOnComponent,
        GlobalUtmComponent,
        UpcomingShowsColumnsBottomComponent,
        AlsoEnjoyComponent,
        AlsoEnjoyLinksComponent,
        StreamingNowHeaderComponent,
        StreamingNowTopComponent,
        StreamingNowBottomComponent,
        PassportLogoHeaderComponent,
        PassportAdComponent,
        MonthlyFunHeadingComponent,
        MonthlyFunSectionComponent,
        BottomSingleColumnHeadingComponent,
        BottomSingleColumnSectionComponent,
        ProvidingSupportHeadingComponent,
        ProvidingSupportAdsComponent,
        EmailOutputComponent,
        CodeOutComponent,
        routingComponents,
        ExtraSingleColumnComponent,
        ExtraSingleColumnHeadingComponent,
        EmailHeadingComponent,
        TextDescriptionComponent,
        UpcomingShowsHeadingComponent,
        SingleColumnHeadingComponent,
        SingleColumnContentComponent,
        ComingSoonTopComponent,
        ComingSoonBottomComponent,
        PassportLearnMoreComponent,
        SingleImageSectionComponent,
        PartnerSpotlightComponent,
        PassportLearnMoreHeadingComponent,
        PartnerSpotlightHeadingComponent,
        partnerSpotlightTwoHeadingComponent,
        PartnerSpotlightTwoComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        DividerComponent,
        ContentImageWrapComponent,
        ContentImageWrapHeadingComponent,
        LittleMoreHeadingComponent,
        LittleMoreComponent,
        AddInterestComponent,
        AddInterestFullWidthComponent,
      ],
      imports: [
        RouterModule,
        FormsModule,
        RouterModule.forRoot(routes)
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/localhost:9876/' }]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should return true when comparing objects', () => {
    const a = { a: 1, b: 2 };
    const b = { a: 1, b: 2 };
    const _isEquivalent = function (objA, objB) {
      const aProps = Object.getOwnPropertyNames(objA);
      const bProps = Object.getOwnPropertyNames(objB);

      if (aProps.length !== bProps.length) {
        return false;
      }

      for (const key in a) {
        if (a.hasOwnProperty(key)) {
          if (a[key] !== b[key] && key !== 'showControls') {
            return false;
          }
        }
      }
      return true;
    }
    let result = _isEquivalent(a, b);
    expect(result).toBeTruthy();
  });
});
