import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { SingleColumnContentComponent } from './single-column-content.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestFullWidthComponent } from '../add-interest-full-width/add-interest-full-width.component';

describe('SingleColumnContentComponent', () => {
  let component: SingleColumnContentComponent;
  let fixture: ComponentFixture<SingleColumnContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        SingleColumnContentComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestFullWidthComponent
      ],
      imports: [ FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleColumnContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });

  it('should change the values of email.singleColumnImageAlt and email.singleColumnContent', ()=>{
    component.email.singleColumnTitle = 'This is a test';
    component.addVariables();
    expect(component.email.singleColumnImageAlt).toBe('This is a test');
    expect(component.email.singleColumnContent).toBe('This%20is%20a%20test');
  });
});
