import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-single-column-content',
  templateUrl: './single-column-content.component.html',
  styleUrls: ['./single-column-content.component.css']
})
export class SingleColumnContentComponent implements OnInit {
  @Input() singleColumnImageAlt: string;
  @Input() singleColumnContent: string;
  email: any = {};
  showComponent: boolean;
  component = 'Single Column';
  singleColumn = 'singleColumnAddInterest';
  singleColumnLink: string;
  singleColumnImageSource: string;
  singleColumnTitle: string;
  singleColumnLocation: string;
  singleColumnDate: string;
  singleColumnDescription: string;
  singleColumnButton: string;

  addVariables(): void {
    this.createTemplate.setVariables('singleColumnImageAlt', this.singleColumnTitle);
    this.createTemplate.setVariables('singleColumnContent', this.singleColumnTitle);
    this.createTemplate.changeVariables('singleColumnTitle', this.singleColumnTitle);
    this.singleColumnImageAlt = this.email.singleColumnImageAlt;
    this.singleColumnContent = this.email.singleColumnContent;
  }
  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.singleColumnDisplay;
    this.singleColumnImageAlt = this.email.singleColumnImageAlt;
    this.singleColumnContent = this.email.singleColumnContent;
    this.singleColumnLink = this.email.singleColumnLink;
    this.singleColumnImageSource = this.email.singleColumnImageSource;
    this.singleColumnTitle = this.email.singleColumnTitle;
    this.singleColumnLocation = this.email.singleColumnLocation;
    this.singleColumnDate = this.email.singleColumnDate;
    this.singleColumnDescription = this.email.singleColumnDescription;
    this.singleColumnButton = this.email.singleColumnButton;
  }

}
