import { Injectable } from '@angular/core';
import { CreateTemplateService } from './create-template.service';

@Injectable({
  providedIn: 'root'
})
export class FontService {
email = this.createTemplate.getVariables();

  programIncrease(): void {
    this.email.programFontSize ++;
  }

  programDecrease(): void {
    this.email.programFontSize --;
  }

  episodeIncrease(): void {
    this.email.episodeFontSize ++;
  }

  episodeDecrease(): void {
    this.email.episodeFontSize --;
  }

  constructor(public createTemplate: CreateTemplateService) { }
}
