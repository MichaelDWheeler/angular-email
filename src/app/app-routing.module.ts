import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ProgramHighlightTemplateComponent } from './program-highlight-template/program-highlight-template.component';
import { MembershipMessagesComponent } from './membership-messages/membership-messages.component';
import { GlobalUtmComponent } from './global-utm/global-utm.component';
import { PassportTemplateComponent } from './passport-template/passport-template.component';
import { DedicatedTemplateComponent } from './dedicated-template/dedicated-template.component';


const routes: Routes = [
  { path: '', component: GlobalUtmComponent },
  { path: 'dedicated', component: DedicatedTemplateComponent},
  { path: 'membership', component: MembershipMessagesComponent },
  { path: 'passport', component: PassportTemplateComponent },
  { path: 'program-highlights', component: ProgramHighlightTemplateComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
export const routingComponents = [
  GlobalUtmComponent, 
  ProgramHighlightTemplateComponent, 
  MembershipMessagesComponent,
  PassportTemplateComponent
];
