import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';

@Component({
  selector: 'app-dedicated-template',
  templateUrl: './dedicated-template.component.html',
  styleUrls: ['./dedicated-template.component.css']
})
export class DedicatedTemplateComponent implements OnInit {
  email: any = {};
  showComponent = true;

  constructor(
    public createTemplate: CreateTemplateService
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
  }
}
