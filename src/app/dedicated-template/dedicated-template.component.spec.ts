import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DedicatedTemplateComponent } from './dedicated-template.component';
import { GlobalUtmComponent } from '../global-utm/global-utm.component';
import { SingleColumnContentComponent } from '../single-column-content/single-column-content.component';
import { DividerComponent } from '../divider/divider.component';
import { PartnerSpotlightComponent } from '../partner-spotlight/partner-spotlight.component';
import { PartnerSpotlightTwoComponent } from '../partner-spotlight-two/partner-spotlight-two.component';
import { FooterComponent } from '../footer/footer.component';
import { CodeOutComponent } from '../code-out/code-out.component';
import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DedicatedTemplateComponent', () => {
  let component: DedicatedTemplateComponent;
  let fixture: ComponentFixture<DedicatedTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas:[NO_ERRORS_SCHEMA],
      declarations: [ 
        DedicatedTemplateComponent,
        GlobalUtmComponent,
        SingleColumnContentComponent,
        DividerComponent,
        PartnerSpotlightComponent,
        PartnerSpotlightTwoComponent,
        FooterComponent,
        CodeOutComponent,
       ],
       imports:[
         FormsModule
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DedicatedTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
