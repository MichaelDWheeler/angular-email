import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-partner-spotlight-two',
  templateUrl: './partner-spotlight-two.component.html',
  styleUrls: ['./partner-spotlight-two.component.css']
})
export class PartnerSpotlightTwoComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'partner content';
  partnerSpotlight = 'partnerSpotlightTwoAddInterest';
  templateSelected: number;
  partnerSpotlightTwoTitle: string;
  partnerSpotlightTwoLocation: string;
  partnerSpotlightTwoDate: string;
  partnerSpotlightTwoButton: string;
  partnerSpotlightTwoDescription: string;
  partnerSpotlightTwoImageSrc: string;
  partnerSpotlightTwoImageAlt: string;
  partnerSpotlightTwoLink: string;
  partnerSpotlightTwoUTM: string;

  checkTemplate(): void{
    this.templateSelected = this.createTemplate.checkTemplateSelected();
    if(this.templateSelected === 0){
      this.email.partnerSpotlightTwoImageSrc = "https://pbs-socal-email.s3.amazonaws.com/newsletters/highlights/2018/10/13/pbs-app-variant-1-min-min.jpg";
      this.email.partnerSpotlightTwoButton = '';
      this.email.partnerSpotlightTwoLink = 'https://www.pbssocal.org/app/';
      this.email.partnerSpotlightTwoUTM = "get%20the%20app";
      this.email.partnerSpotlightTwoImageAlt = "Get the free PBS app";
    }
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.partnerSpotlightTwoDisplay = this.showComponent;
  }

  addVariables(): void {
    this.createTemplate.setVariables('partnerSpotlightTwoImageAlt', this.partnerSpotlightTwoTitle);
    this.createTemplate.setVariables('partnerSpotlightTwoUTM', this.partnerSpotlightTwoTitle);
    this.createTemplate.changeVariables('partnerSpotlightTwoTitle',this.partnerSpotlightTwoTitle);
    this.partnerSpotlightTwoImageAlt = this.email.partnerSpotlightTwoImageAlt;
    this.partnerSpotlightTwoUTM = this.email.partnerSpotlightTwoUTM;
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.partnerSpotlightTwoDisplay;
    this.checkTemplate();
    this.partnerSpotlightTwoTitle = this.email.partnerSpotlightTwoTitle;
    this.partnerSpotlightTwoLocation = this.email.partnerSpotlightTwoLocation;
    this.partnerSpotlightTwoDate = this.email.partnerSpotlightTwoDate;
    this.partnerSpotlightTwoButton = this.email.partnerSpotlightTwoButton;
    this.partnerSpotlightTwoDescription = this.email.partnerSpotlightTwoDescription;
    this.partnerSpotlightTwoImageSrc = this.email.partnerSpotlightTwoImageSrc;
    this.partnerSpotlightTwoImageAlt = this.email.partnerSpotlightTwoImageAlt;
    this.partnerSpotlightTwoLink = this.email.partnerSpotlightTwoLink;
    this.partnerSpotlightTwoUTM = this.email.partnerSpotlightTwoUTM;
  }

}
