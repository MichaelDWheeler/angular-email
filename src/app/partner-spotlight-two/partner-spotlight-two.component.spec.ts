import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { PartnerSpotlightTwoComponent } from './partner-spotlight-two.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestFullWidthComponent } from '../add-interest-full-width/add-interest-full-width.component';

describe('PartnerSpotlightTwoComponent', () => {
  let component: PartnerSpotlightTwoComponent;
  let fixture: ComponentFixture<PartnerSpotlightTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PartnerSpotlightTwoComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestFullWidthComponent
      ],
      imports: [ FormsModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerSpotlightTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });

  it('should set variables to "THIS IS A TEST" and "THIS%20IS%20A%20TEST"', ()=>{
    component.email.partnerSpotlightTwoTitle = 'THIS IS A TEST';
    component.addVariables();
    expect(component.email.partnerSpotlightTwoImageAlt).toBe('THIS IS A TEST');
    expect(component.email.partnerSpotlightTwoUTM).toBe('THIS%20IS%20A%20TEST');
  });
});
