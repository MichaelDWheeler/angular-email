import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-banner-ad',
  templateUrl: './banner-ad.component.html',
  styleUrls: ['./banner-ad.component.css']
})
export class BannerAdComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'Banner Ad';
  bannerAdImageSrc: string;
  bannerAdLink: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.bannerAdDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }
  
  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.bannerAdDisplay;
    this.bannerAdImageSrc = this.email.bannerAdImageSrc;
    this.bannerAdLink = this.email.bannerAdLink;
  }

}
