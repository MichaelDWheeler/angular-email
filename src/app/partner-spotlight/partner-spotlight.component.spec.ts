import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { PartnerSpotlightComponent } from './partner-spotlight.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestFullWidthComponent } from '../add-interest-full-width/add-interest-full-width.component';

describe('PartnerSpotlightComponent', () => {
  let component: PartnerSpotlightComponent;
  let fixture: ComponentFixture<PartnerSpotlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PartnerSpotlightComponent,
        RemoveButtonComponent,
        RestoreButtonComponent,
        AddInterestFullWidthComponent
      ],
      imports: [ FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerSpotlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause showComponent to equal false and then true', ()=>{
    component.toggleControls();
    expect(component.showComponent).toBe(false);
    component.toggleControls();
    expect(component.showComponent).toBe(true);
  });

  it('should set variables to "THIS IS A TEST" and "THIS%20IS%20A%20TEST"', ()=>{
    component.email.partnerSpotlightTitle = 'THIS IS A TEST';
    component.addVariables();
    expect(component.email.partnerSpotlightImageAlt).toBe('THIS IS A TEST');
    expect(component.email.partnerSpotlightUTM).toBe('THIS%20IS%20A%20TEST');
  });
});
