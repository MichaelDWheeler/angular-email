import { Component, OnInit } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-partner-spotlight',
  templateUrl: './partner-spotlight.component.html',
  styleUrls: ['./partner-spotlight.component.css']
})
export class PartnerSpotlightComponent implements OnInit {
  email: any = {};
  showComponent: boolean;
  component = 'partner content';
  partnerSpotlight = 'partnerSpotlightAddInterest';
  partnerSpotlightTitle: string;
  partnerSpotlightImageAlt: string;
  partnerSpotlightUTM: string;
  partnerSpotlightLocation: string;
  partnerSpotlightDate: string;
  partnerSpotlightButton: string;
  partnerSpotlightDescription: string;
  partnerSpotlightImageSrc: string;
  partnerSpotlightLink: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.partnerSpotlightDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  addVariables(): void {
    this.createTemplate.setVariables('partnerSpotlightImageAlt', this.partnerSpotlightTitle);
    this.createTemplate.setVariables('partnerSpotlightUTM', this.partnerSpotlightTitle);
    this.createTemplate.changeVariables('partnerSpotlightTitle', this.partnerSpotlightTitle);
    this.partnerSpotlightImageAlt = this.email.partnerSpotlightImageAlt;
    this.partnerSpotlightUTM = this.email.partnerSpotlightUTM;
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.partnerSpotlightDisplay;
    this.partnerSpotlightTitle = this.email.partnerSpotlightTitle;
    this.partnerSpotlightLocation = this.email.partnerSpotlightLocation;
    this.partnerSpotlightDate = this.email.partnerSpotlightDate;
    this.partnerSpotlightButton = this.email.partnerSpotlightButton;
    this.partnerSpotlightDescription = this.email.partnerSpotlightDescription;
    this.partnerSpotlightImageSrc = this.email.partnerSpotlightImageSrc;
    this.partnerSpotlightImageAlt = this.email.partnerSpotlightImageAlt;
    this.partnerSpotlightLink = this.email.partnerSpotlightLink;
    this.partnerSpotlightUTM = this.email.partnerSpotlightUTM;
  }

}
