import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AlsoEnjoyComponent } from './also-enjoy.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';

describe('AlsoEnjoyComponent', () => {
  let component: AlsoEnjoyComponent;
  let fixture: ComponentFixture<AlsoEnjoyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AlsoEnjoyComponent,
        RemoveButtonComponent,
        RestoreButtonComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlsoEnjoyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return \'You May Also Enjoy\' as the heading', () => {
    expect(component.email.alsoEnjoyHeading).toEqual('You May Also Enjoy');
  });
});
