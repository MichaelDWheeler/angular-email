import { Component, OnInit, Input } from '@angular/core';
import { CreateTemplateService } from '../create-template.service';
import { SelectTextService } from '../select-text.service';

@Component({
  selector: 'app-also-enjoy',
  templateUrl: './also-enjoy.component.html',
  styleUrls: ['./also-enjoy.component.css']
})
export class AlsoEnjoyComponent implements OnInit {
  @Input() show: boolean;
  component = 'Also Enjoy Section';
  email: any = {};
  showComponent: boolean;
  alsoEnjoyHeading: string;

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.alsoEnjoyDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService
    ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.showComponent = this.email.alsoEnjoyDisplay;
    this.alsoEnjoyHeading = this.email.alsoEnjoyHeading;
  }

}
