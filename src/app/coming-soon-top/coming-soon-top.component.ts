import { Component, OnInit, OnDestroy} from '@angular/core';
import { SelectTextService } from '../select-text.service';
import { FontService } from '../font.service';
import { CreateTemplateService } from '../create-template.service';
import { AddInterestService } from '../add-interest.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-coming-soon-top',
  templateUrl: './coming-soon-top.component.html',
  styleUrls: ['./coming-soon-top.component.css']
})
export class ComingSoonTopComponent implements OnInit, OnDestroy {
  email: any = {};
  showComponent: boolean;
  component = 'two programs';
  addInterest: any = [];
  topLeft = 'comingSoonTopLeftAddInterest';
  topRight = 'comingSoonTopRightAddInterest';
  comingSoonTopLeftLink: string;
  comingSoonTopLeftProgram: string;
  comingSoonTopLeftTitle: string;
  comingSoonTopLeftAirDate: string;
  comingSoonTopLeftButton: string;
  comingSoonTopLeftDescription: string;
  comingSoonTopLeftImage: string;
  comingSoonTopLeftAlt: string;
  comingSoonTopLeftUTM: string;
  comingSoonTopRightProgram: string;
  comingSoonTopRightTitle: string;
  comingSoonTopRightAirDate: string;
  comingSoonTopRightButton: string;
  comingSoonTopRightDescription: string;
  comingSoonTopRightImage: string;
  comingSoonTopRightAlt: string;
  comingSoonTopRightLink: string;
  comingSoonTopRightUTM: string;
  subscription: Subscription;

  addVariables(): void {
    this.createTemplate.setVariables('comingSoonTopLeftAlt', this.comingSoonTopLeftProgram);
    this.createTemplate.setVariables('comingSoonTopLeftUTM', this.comingSoonTopLeftProgram); 
    this.createTemplate.changeVariables('comingSoonTopLeftProgram',this.comingSoonTopLeftProgram);
    this.createTemplate.setVariables('comingSoonTopRightAlt', this.comingSoonTopRightProgram);
    this.createTemplate.setVariables('comingSoonTopRightUTM', this.comingSoonTopRightProgram);
    this.createTemplate.changeVariables('comingSoonTopRightProgram',this.comingSoonTopRightProgram);
    this.comingSoonTopLeftAlt = this.email.comingSoonTopLeftAlt;
    this.comingSoonTopLeftUTM = this.email.comingSoonTopLeftUTM;
    this.comingSoonTopRightAlt = this.email.comingSoonTopRightAlt;
    this.comingSoonTopRightUTM = this.email.comingSoonTopRightUTM;
  }

  toggleControls(): void {
    this.showComponent = !this.showComponent;
    this.email.comingSoonTopDisplay = this.showComponent;
    this.createTemplate.setStorage();
  }

  utmSubscription(): void{
    this.subscription = this.createTemplate.getMessage().subscribe(message => {
      if(message){
        this.comingSoonTopLeftUTM = this.email.comingSoonTopLeftUTM;
        this.comingSoonTopRightUTM = this.email.comingSoonTopRightUTM;
      }
    });
  }

  constructor(
    public createTemplate: CreateTemplateService,
    public select: SelectTextService,
    public font: FontService,
    public interest: AddInterestService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.email = this.createTemplate.getVariables();
    this.addInterest = this.interest.getAddInterests();
    this.showComponent = this.email.comingSoonTopDisplay;
    this.comingSoonTopLeftLink = this.email.comingSoonTopLeftLink;
    this.comingSoonTopLeftProgram = this.email.comingSoonTopLeftProgram;
    this.comingSoonTopLeftTitle = this.email.comingSoonTopLeftTitle;
    this.comingSoonTopLeftAirDate = this.email.comingSoonTopLeftAirDate;
    this.comingSoonTopLeftButton = this.email.comingSoonTopLeftButton;
    this.comingSoonTopLeftDescription = this.email.comingSoonTopLeftDescription;
    this.comingSoonTopLeftImage = this.email.comingSoonTopLeftImage;
    this.comingSoonTopLeftAlt = this.email.comingSoonTopLeftAlt;
    this.comingSoonTopLeftUTM = this.email.comingSoonTopLeftUTM;
    this.comingSoonTopRightProgram = this.email.comingSoonTopRightProgram;
    this.comingSoonTopRightTitle = this.email.comingSoonTopRightTitle;
    this.comingSoonTopRightAirDate = this.email.comingSoonTopRightAirDate;
    this.comingSoonTopRightButton = this.email.comingSoonTopRightButton;
    this.comingSoonTopRightDescription = this.email.comingSoonTopRightDescription;
    this.comingSoonTopRightImage = this.email.comingSoonTopRightImage;
    this.comingSoonTopRightAlt = this.email.comingSoonTopRightAlt;
    this.comingSoonTopRightLink = this.email.comingSoonTopRightLink;
    this.comingSoonTopRightUTM = this.email.comingSoonTopRightUTM;
    this.utmSubscription();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
