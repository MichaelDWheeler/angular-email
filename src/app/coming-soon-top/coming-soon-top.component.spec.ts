import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { ComingSoonTopComponent } from './coming-soon-top.component';
import { RemoveButtonComponent } from '../remove-button/remove-button.component';
import { RestoreButtonComponent } from '../restore-button/restore-button.component';
import { AddInterestComponent } from '../add-interest/add-interest.component';
import { TEMPLATEVARIABLES } from '../data-create-template';

describe('ComingSoonTopComponent', () => {
  let component: ComingSoonTopComponent;
  let fixture: ComponentFixture<ComingSoonTopComponent>;
  const templateVariables = TEMPLATEVARIABLES;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ComingSoonTopComponent,
        RestoreButtonComponent,
        RemoveButtonComponent,
        AddInterestComponent
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComingSoonTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "showComponent" to change between true and false', () => {
    component.toggleControls();
    expect(component.showComponent).toBeFalsy();
    component.toggleControls();
    expect(component.showComponent).toBeTruthy();
  });

  it('should populate interest object with 22 interest properties and values', () => {
    component.addInterest = component.interest.getAddInterests();
    expect(component.addInterest.length).toEqual(22);
  });

  it('should return the email object with default properties', () => {
    component.email = component.createTemplate.getVariables();
    expect(component.email).toEqual(templateVariables);
  });

});
