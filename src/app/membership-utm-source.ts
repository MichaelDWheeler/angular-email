export interface MembershipUtmSource {
  id: number;
  name: string;
  source: string;
}
