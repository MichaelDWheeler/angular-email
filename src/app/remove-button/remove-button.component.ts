import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-remove-button',
  templateUrl: './remove-button.component.html',
  styleUrls: ['./remove-button.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveButtonComponent implements OnInit {
  @Input() component: string;
  @Input() showComponent: boolean;
  color: string;


  checkComponent(): void{
    if(this.component === 'divider'){
      this.color = 'divider';
    } else {
      this.color = 'remove-btn';
    }
  }
  constructor() { }

  ngOnInit() {
    this.checkComponent();
  }

}
