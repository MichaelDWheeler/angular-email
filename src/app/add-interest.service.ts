import { Injectable } from '@angular/core';
import { AddInterest } from './add-interest';
import { CreateTemplateService } from './create-template.service';

@Injectable({
  providedIn: 'root'
})
export class AddInterestService {
  email: any = {} = this.createTemplate.getVariables();

  addInterestOptions: AddInterest[] = [
    { order: 21, id: 0, category: 'No Interest' },
    { order: 0, id: 1000, category: 'Program Preferences' },
    { order: 1, id: 1421, category: '--Antiquarians' },
    { order: 2, id: 1347, category: '--Biographers' },
    { order: 3, id: 1541, category: '--Bookworms' },
    { order: 4, id: 1403, category: '--Comedians' },
    { order: 5, id: 1346, category: '--Detectives' },
    { order: 6, id: 1402, category: '--Documentarians' },
    { order: 7, id: 1343, category: '--Dramatists' },
    { order: 8, id: 1381, category: '--Film Fans' },
    { order: 9, id: 1562, category: '--Financial Wizards' },
    { order: 10, id: 1350, category: '--Foodies' },
    { order: 11, id: 1561, category: '--Health and Fitness Gurus' },
    { order: 12, id: 1345, category: '--Historians' },
    { order: 13, id: 1466, category: '--Home Builders' },
    { order: 14, id: 1322, category: '--Music Lovers' },
    { order: 15, id: 1401, category: '--Naturalists' },
    { order: 16, id: 1342, category: '--News Junkies' },
    { order: 17, id: 1351, category: '--Parents, Grandparents, Kid-Havers' },
    { order: 18, id: 1344, category: '--Science Nerds' },
    { order: 19, id: 1361, category: '--Sirens of Stage' },
    { order: 20, id: 1348, category: '--Travelers & Tourists' },
  ];

  getAddInterests(): any {
    return this.addInterestOptions;
  }

  setAddInterests($event: any, component: string): void {
    this.email[component] = $event;
    if (parseInt($event, 10) === 0) {
      this.email[component + 'Converted'] = '';
    } else {
      this.email[component + 'Converted'] = 'AddInterest=' + $event + '&';
    }
  }

  constructor(public createTemplate: CreateTemplateService) { }
}
